import os
import cv2
import shutil


file_list = os.listdir('./')
png_list = []

for f in file_list:
	ex = f.split('.')
	if(len(ex)>1 and ex[1] == 'png'):
		png_list.append(f)



for png in png_list:
	print(png)
	img = cv2.imread('./'+png,cv2.IMREAD_UNCHANGED)


	h,w,c = img.shape

	BG = [255,128,128,255]
	U = [159,224,128,255]
	R = [159,127,224,255]
	L = [159,128,32,255]
	D = [159,32,127,255]

	UR = [165,199,183,255]
	DR = [164,61,190,255]
	UL = [158,193,56,255]
	DL = [166,58,72,255]


	img2 = [[0 for i in range(w)] for i in range(h)]

	for y in range(h):
		for x in range(w):
			if img[y,x][3] == 0 :
				img[y,x] = BG
				img2[y][x] = 'bg'
				

				if (y > 0 and (img2[y-1][x] != 'bg' and str(img2[y-1][x]).find('U') == -1 and str(img2[y-1][x]).find('D') == -1)):
					if img2[y-1][x] == 'L':
						img2[y-1][x] == 'DL'
						img[y-1,x] = DL
					else:
						img2[y-1][x] == 'D'
						img[y-1,x] = D

				if (x > 0 and (img2[y][x-1] != 'bg' and str(img2[y][x-1]).find('L') == -1 and str(img2[y][x-1]).find('R') == -1)):
					
					if img2[y][x-1] == 'U':
						img2[y][x-1] = 'UR'
						img[y,x-1] = UR
					elif img2[y][x-1] == 'D':
						img2[y][x-1] = 'DR'
						img[y,x-1] = UR
					else:
						img2[y][x-1] = 'R'
						img[y,x-1] = R

			else:
				if y == 0 or img2[y-1][x] == 'bg':
					img[y,x] = U
					img2[y][x] = 'U'

				if x == 0 or img2[y][x-1] == 'bg' :
					if(img2[y][x] == 'U'):
						img[y,x] = UL
						img2[y][x] = 'UL'
					else:
						img[y,x] = L
						img2[y][x] = 'L'

				if img2[y][x] == 0 :
					img[y,x] = BG
					img2[y][x] = 'f'
					if y == h-1 :
						if(img2[y][x] == 'L'):
							img2[y][x] = 'DL'
							img[y,x] = DL
						else :
							img[y,x] = D
							img2[y][x] = 'D'
					if x == w-1 :
						if img2[y][x] == 'U':
							img2[y][x] = 'UR'
							img[y,x] = UR
						else:
							img2[y][x] = 'R'
							img[y,x] = R


		
	if(not os.path.isdir('./Done')):
		os.mkdir('./Done')
	

	name = png.split('.')
	cv2.imwrite('./Done/'+name[0]+'_noramlmap.png',img)

	shutil.move('./'+png,'./Done/'+png)
	
	
