using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFollow : MonoBehaviour
{

    public Transform target;
    public Vector2 pivot;
    Vector2 currentPivot;
    Vector2 velocity = new Vector2(2,2);
    [SerializeField]
    float followTime = 1;

    [SerializeField]
    SpriteRenderer spriteRenderer;

    SpriteRenderer targetSprite;
    // Start is called before the first frame update
    void Start()
    {
        currentPivot = pivot;
        targetSprite = target.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetSprite.flipX)
        {
            currentPivot.x = -pivot.x;
        }
        else
        {
            currentPivot.x = pivot.x;
        }

        transform.position = new Vector2(Mathf.SmoothDamp(transform.position.x, target.position.x + currentPivot.x, ref velocity.x, followTime), 
                                            Mathf.SmoothDamp(transform.position.y, target.position.y + currentPivot.y, ref velocity.y, followTime));

        if(transform.position.x < target.position.x)
        {
            spriteRenderer.flipX = false;
        }
        else
        {
            spriteRenderer.flipX = true;
        }
    }
}
