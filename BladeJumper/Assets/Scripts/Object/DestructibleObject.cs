using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(BoxCollider2D))]
public class DestructibleObject : MonoBehaviour,IDamagedObject
{

    [SerializeField]
    int hp = 2;

    Bounds bounds;


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }

    public bool Damaged(int damage, Vector2 dir = default, Vector2 power = default, GameObject actor = default)
    {
        hp -= damage;

        if(hp <= 0)
        {
            Destroy(gameObject);
        }
        return true;
    }

    public int GetHP()
    {
        return hp;
    }
}
