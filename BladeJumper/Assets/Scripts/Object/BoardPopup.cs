using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Popup targetObject When Player Approach this BoxColliderArea

[RequireComponent(typeof(BoxCollider2D))]
public class BoardPopup : MonoBehaviour
{
    [SerializeField]
    GameObject popupObject;

    Animator animator;
    BoxCollider2D boxCollider;
    // Start is called before the first frame update
    private void Awake()
    {
        animator = popupObject.GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();
    }


    // Update is called once per frame
    void Update()
    {
        if (PlayerApproach())
        {
            if(animator != null)
            {
                animator.Rebind();
                animator.Update(0f);
            }
            popupObject.SetActive(true);
        }
        else
        {
            popupObject.SetActive(false);
        }
    }

    //Player Approach Check
    bool PlayerApproach()
    {
        Vector2 playerPos = BladeJumper.bladeJumper.GetComponent<BoxCollider2D>().bounds.center;
        if(playerPos.y > boxCollider.bounds.min.y && playerPos.y < boxCollider.bounds.max.y &&
            playerPos.x > boxCollider.bounds.min.x && playerPos.x < boxCollider.bounds.max.x)
        {
            return true;
        }
        return false;
    }
}
