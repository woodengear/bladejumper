using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITrailUse
{
    
    public bool EndTrail();
    public Vector2 Position();


}

public class TrailEffectControl : MonoBehaviour
{
    [ColorUsage(true, true)]
    public Color baseColor;
    GameObject owner;
    ITrailUse target;
    float lastTime; // 유지 시간
    float lastMove; // 해당 시간동안 마지막 진행 방향으로 이동
    Material material;
    TrailRenderer trail;
    // Start is called before the first frame update
    private void Awake()
    {
        trail = GetComponent<TrailRenderer>();
        material = trail.material;
        material.SetColor("_EffectColor", baseColor);
        trail.startColor = baseColor;
        trail.endColor = baseColor;


    }

    internal void SetColor(Color color)
    {
        material.SetColor("_EffectColor", color);
        trail.startColor = color;
        trail.endColor = color;
    }

    internal void StartTrail(GameObject owner,ITrailUse trailUse,float lastTime, float lastMove)
    {
        this.owner = owner;
        this.target = trailUse;
        this.lastTime = lastTime;
        this.lastMove = lastMove;
        StartCoroutine(Trail());
    }

    IEnumerator Trail()
    {
        while (owner != null &&target != null &&!target.EndTrail())
        {
            transform.position = owner.transform.position;
            yield return null;
        }
        yield return new WaitForSeconds(lastMove);
    }
    internal void EndTrail()
    {
        
    }
}
