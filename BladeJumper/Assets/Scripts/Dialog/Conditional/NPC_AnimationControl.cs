using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dialog.Conditional;

namespace Dialog.Conditional
{

    [RequireComponent(typeof(Animator))]
    public class NPC_AnimationControl : MonoBehaviour
    {
        /*
         * 조건에 따라 애니메이션 변경
         * 
         * 위에 있을수록 우선순위가 높다.
         * 
         * 변수 설명
         *      ConditionSet conditionSet
         *           애니메이션 변경을 하기위한 조건
         *      string animationStateName
         *           조건이 만족할 경우 변경할 AnimationStatneNmae
         *           해당 게임오브젝트의 Animator에 있는 animation 이름 기준
         *           
         * 
         *      ConditionalDialogControl[] updateDialogs
         *          해당 배열에 있는 Dialog 종료시 조건 업데이트 
         */
        [System.Serializable]
        struct ConditionAnimation
        {
            [SerializeField]
            internal ConditionSet conditionSet;
            [SerializeField]
            internal string animationStateName;

            internal bool GetConditions()
            {
                return conditionSet.GetCondition();
            }
        }

        [SerializeField]
        ConditionAnimation[] conditionalAnimations;


        [SerializeField]
        ConditionalDialogControl[] updateDialogs;
        // Start is called before the first frame update
        Animator animator;

        internal void AnimationUpdate()
        {
            if(conditionalAnimations.Length > 0)
            {
                for(int i = 0; i < conditionalAnimations.Length; i++)
                {
                    if (conditionalAnimations[i].GetConditions())
                    {
                        animator.Play(conditionalAnimations[i].animationStateName);
                        break;
                    }
                }
            }
        }
        
        void Start()
        {
            if(updateDialogs.Length > 0)
            {
                for(int i = 0; i < updateDialogs.Length; i++)
                {
                    updateDialogs[i].updateEvent += AnimationUpdate;
                }
            }
            animator = GetComponent<Animator>();
            AnimationUpdate();
        }

        private void OnBecameVisible()
        {
            AnimationUpdate();
        }
    }
}
