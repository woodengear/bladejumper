using System;
using UnityEngine;

namespace Dialog.Conditional
{
    [Serializable]
    internal enum DialogCondition { bestHeight, candyCount, dialogCount }
    [Serializable]
    internal class Condition
    {
        /* 클래스 설명
         * 조건 Dialog의 조건 구현 클래스
         * 
         * 조건 비교 연산
         *      Over    초과
         *      More    이상
         *      Same    동일
         *      Below   이하
         *      Under   미만
         */
        /* 변수설명
         * 
         * string dialogCountName
         *      몇번 대화 했는지 불러올 대화의 이름.
         *      해당 이름을 기준으로 조건의 dialogCount값을 불러옴
         * 
         * int value
         *      지정된 dialogCondition과 비교
         */

        [Serializable]
        enum ConditionOperator { Over, More, Same, Below, Under }

        [SerializeField]
        string dialogCountName;

        [SerializeField]
        internal DialogCondition condition;

        [SerializeField]
        ConditionOperator _operator;

        [SerializeField]
        internal int value;


        bool CalculateCondition(int targetValue)
        {
            switch (_operator)
            {
                case ConditionOperator.Over:
                    if (value < targetValue)
                        return true;
                    break;
                case ConditionOperator.More:
                    if (value <= targetValue)
                        return true;
                    break;
                case ConditionOperator.Same:
                    if (value == targetValue)
                        return true;
                    break;
                case ConditionOperator.Below:
                    if (value >= targetValue)
                        return true;
                    break;
                case ConditionOperator.Under:
                    if (value > targetValue)
                        return true;
                    break;
            }
            return false;
        }

        internal bool GetCondition(int dialogCount = -1)
        {

            switch (condition)
            {
                case DialogCondition.bestHeight:
                    return CalculateCondition(SaveManager.instance.userData.bestHeight);

                case DialogCondition.candyCount:
                    return CalculateCondition(KKRCandy.candyCount);

                case DialogCondition.dialogCount:
                    if (dialogCountName != "")
                    {
                        dialogCount = SaveManager.instance.GetUsedCount(dialogCountName.GetHashCode());
                    }
#if UNITY_EDITOR
                    if (dialogCount < 0)
                    {
                        Debug.LogError("dialogCountName 설정 바람");
                        return false;
                    }
#endif
                    return CalculateCondition(dialogCount);
            }
            return false;
        }
    }
}