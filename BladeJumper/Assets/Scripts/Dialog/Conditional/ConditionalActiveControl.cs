using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dialog.Conditional;

namespace Dialog.Conditional
{
    public class ConditionalActiveControl : MonoBehaviour
    {
        /*
        * 조건에 따라 GameObject.active 변경
        * 
        * 조건내의 target의 active를 변경
        *   같은 target의 여러 조건들이 동시에 만족하면 만족하는 조건중 가장 밑에있는 active로 됨
        * 
        * 변수 설명
        *       ConditionSet conditionSet
        *           active 변경을 하기위한 조건
        *       GameObject target
        *           active를 변경할 gameObject
        *       bool active
        *           조건이 만족할 경우 target의 active를 변경할 값
         *      ConditionalDialogControl[] updateDialogs
         *          해당 배열에 있는 Dialog 종료시 조건 업데이트 
        */
        [System.Serializable]
        struct ActiveControl
        {
            [SerializeField]
            string name;
            [SerializeField]
            ConditionSet conditionSet;
            [SerializeField]
            internal GameObject target;
            [SerializeField]
            internal bool active;
            internal bool GetConditions()
            {
                return conditionSet.GetCondition();
            }
        }


        [SerializeField]
        ActiveControl[] activeControls;

        [SerializeField]
        ConditionalDialogControl[] updateDialog;
        // Start is called before the first frame update
        void Start()
        {
            if(updateDialog.Length > 0)
            {
                for(int i = 0; i < updateDialog.Length; i++)
                {
                    updateDialog[i].updateEvent += UpdateActive;
                }
            }
        }
        void UpdateActive()
        {
            if (activeControls.Length > 0)
            {
                for (int i = 0; i < activeControls.Length; i++)
                {
                    if (activeControls[i].GetConditions())
                    {
                        if(activeControls[i].target.activeSelf != activeControls[i].active)
                             activeControls[i].target.SetActive(activeControls[i].active);
                    }
                }
            }
        }
        private void OnBecameVisible()
        {
            UpdateActive();
        }
        private void OnBecameInvisible()
        {
            UpdateActive();
        }
    }
}