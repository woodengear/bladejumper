using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Dialog;


namespace Dialog.Conditional
{
    

    internal delegate void ConditionUpdate();

    [Serializable]
    public class ConditionalDialog
    {
        /* 클래스 설명
         * 조건 포함된 Dialog 클래스
         * 조건 종류
         *      최대 도달 높이
         *      캔디 갯수
         *      대화 횟수
         */
        /* 변수 설명
         * bool mumble
         *      플레이어 캐릭터 이동 가능.
         *      플레이어의 조작과 상관없이 대화
         *      
         * bool countable
         *      조건에 해당하는 대화 갯수에 포함되는지 결정
         *      
         * Condition[] conditions
         *      조건 배열
         *      해당 조건중 하나라도 false라면 조건 만족하지 않음 = false 리턴
         *      배열의 길이가 0이면 항상 true 리턴
         *      
         * DialogInfo[] dialogs
         *      대화 배열
         *      
         */
        
        [SerializeField]
        string name;
        [SerializeField]
        internal string dialogCountName;
        [SerializeField]
        internal bool mumble = false;
        [SerializeField]
        internal bool countable = true;



        [SerializeField]
        ConditionSet conditionSet;
        
        [SerializeField]
        internal DialogInfo[] dialogs;

        

        /* 함수 설명
         * bool GetCondition(int dialogCount)
         *      conditions 의 모든 조건을 검사하여
         *      모든 조건이 만족하면 true
         *      하나라도 만족하지 않으면 false 리턴
         *      
         * 
         */

        

        internal bool GetCondition(int dialogCount)
        {
            if (conditionSet == null) return true;
            
            return conditionSet.GetCondition(dialogCount);
        }
    }

    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class ConditionalDialogControl : MonoBehaviour
    {
        /*클래스 설명
         * ConditionalDialog 들을 컨트롤
         * 
         */

        /* 변수 설명
         * ConditionalDialog[] dialogs 
         *      조건이 포함된 dialog 집합
         *      index가 0에 가까울 수록 우선도가 높다.
         *      
         * bool isPlay
         *      Dialog가 실행중인지 확인하는 변수
         *      조건에 맞는 Dialog 가 실행 될때 true
         *      Dialog 가 종료되면 false
         *      
         * int playCount
         *      플레이어가 해당 오브젝트를 통하여 몇번 대화 했는지 확인
         *      해당 데이터는 저장된다.
         * 
         * AnimationControl animCon
         *      플레이어 캐릭터의 현재 애니메이션 확인용
         *      플레이어의 animation이 walk, idle일때만 대화 진행
         *      dialog가 mumble일경우에는 상관 없다.
         *      
         * DialogEndEvent hdr
         *      현재 dialog 삭제용 핸들러
         * ConditionUpdate updateEvent
         *      대화 종료시 실행
         */
        [SerializeField]
        ConditionalDialog[] dialogs;

        bool visible = false;
        bool isPlay = false;
        int playCount = 0;
        BoxCollider2D col;
        AnimationControl animCon;
        DialogEndEvent hdr;
        /// <summary>
        /// Dialog 종료시 실행
        /// </summary>
        internal ConditionUpdate updateEvent;

        private void OnDrawGizmos()
        {
            Bounds bounds = GetComponent<BoxCollider2D>().bounds;

            Gizmos.color = new Color(0, 0, 1, .5f);
            Gizmos.DrawCube(bounds.center, bounds.size);
        }
        // Start is called before the first frame update
        void Start()
        {
            playCount = SaveManager.instance.GetUsedCount(gameObject.name.GetHashCode());
            col = GetComponent<BoxCollider2D>();
            animCon = BladeJumper.bladeJumper.GetComponent<AnimationControl>();
        }

        // Update is called once per frame
        void Update()
        {
            if (visible)
            {

                if (isPlay == false && DialogManager.instance.IsDialog() == false && col.bounds.Contains(BladeJumper.bladeJumper.GetComponent<BoxCollider2D>().bounds.center))
                {

                    if (ConditionalStart((animCon.currentAnim == AnimationControl.AnimState.Idle || animCon.currentAnim == AnimationControl.AnimState.Walk)))
                    {
                        isPlay = true;

                    }

                }
            }
        }


        /* StartDialog(DialogInfo[] dialogs,bool mumble)
         * Dialog 진행 루틴
         * parameter :
         *      DialogInfo[] dialogs
         *          대화 정보가 포함된 배열
         *      bool mumble
         *          대화인지, 혼잣말인지 확인
         *          false 라면 플레이어 캐릭터의 동작을 제한하고 애니메이션을 초기화.
         *          
         * 
         * 
         */
        IEnumerator StartDialog(DialogInfo[] dialogs,bool mumble)
        {
            isPlay = true;
            if (!mumble)
            {
                DialogManager.instance.SetCurrentDialog(true);
                Animator pAnim = BladeJumper.bladeJumper.GetComponent<Animator>();
                pAnim.Rebind();
                pAnim.Update(0f);
            }
            
            for (int i = 0; i < dialogs.Length; i++)
            {
                Animator animator = dialogs[i].talker.GetComponent<Animator>();
                if (animator != null && dialogs[i].animationState != null)
                {
                    animator.Play(dialogs[i].animationState);
                }

                yield return StartCoroutine(DialogManager.instance.CreateDialog(dialogs[i].dialog, dialogs[i].talker, ref hdr, DIALOG_DIR.UP,!mumble));

                if (animator != null && dialogs[i].animationState != null)
                {
                    animator.Rebind();
                    animator.Update(0f);
                }
                //yield return new WaitForSeconds(interval);

                if(mumble == true)
                {
                    yield return new WaitForSeconds(1f);
                    
                }
                else
                {
                    yield return new WaitUntil(() => UserInput.WasPressed(UserInput.UserAction.Space));
                }

                hdr();
                hdr = null;
            }
            if (!mumble)
                DialogManager.instance.SetCurrentDialog(false);
            
            yield return new WaitUntil(() => !(col.bounds.Contains(BladeJumper.bladeJumper.GetComponent<BoxCollider2D>().bounds.center)));
            isPlay = false;
            if(updateEvent != null)
            {
                updateEvent();
            }
        }

        /* bool ConditionalStart(bool talkable)
         *      bool talkable
         *          현재 플레이어의 캐릭터가 대화 가능 상태인지 확인하는 변수
         *          false라면 혼잣말만 가능하다
         * 해당 오브젝트에 있는 ConditionalDialog[] dialogs 배열을 검사하여
         * 가장 먼저 조건이 참인 dialog로 대화를 시작한다.
         */
        bool ConditionalStart(bool talkable)
        {
            for(int i = 0; i < dialogs.Length; i++)
            {
                if (dialogs[i].GetCondition(playCount))
                {
                    
                    if (talkable == false&& dialogs[i].mumble == false)
                    {
                        continue;
                    }
                    if( dialogs[i].mumble == false)
                    {
                        if(PlayerInputControl.jump > 0)
                        {
                            PlayerInputControl.jump = 0;
                            
                        }
                        else
                        {
                            DialogManager.talkBoxTarget = dialogs[i].dialogs[0].talker.gameObject;
                            break;
                        }
                    }
                    if (dialogs[i].countable)
                    {
                        if(dialogs[i].dialogCountName == "")
                        {
                            playCount = SaveManager.instance.GetUsedCount(this.gameObject.name.GetHashCode());
                            playCount += 1;
                            SaveManager.instance.SetUsedCount(this.gameObject.name.GetHashCode(), playCount);
                        }
                        else
                        {
                            SaveManager.instance.SetUsedCount(this.gameObject.name.GetHashCode(), SaveManager.instance.GetUsedCount(this.gameObject.name.GetHashCode() + 1));
                        }
                       
                    }
                    StartCoroutine(StartDialog(dialogs[i].dialogs, dialogs[i].mumble));
                    return true;
                }
            }
            return false;
        }

        private void OnBecameVisible()
        {
            visible = true;
        }
        private void OnBecameInvisible()
        {
            visible = false;
        }

        
    }
}