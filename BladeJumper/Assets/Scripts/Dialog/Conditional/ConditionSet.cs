using System;
using UnityEngine;

namespace Dialog.Conditional
{
    [Serializable]
    internal class ConditionSet
    {
        /*
         * Condition 의 집합
         * 
         * GetCondition
         *      conditions의 모든 원소가 만족 할때에만 true를 return
         */
        [SerializeField]
        Condition[] conditions;
        internal bool GetCondition(int dialogCount = -1)
        {
            if (conditions == null) return true;
            for (int i = 0; i < conditions.Length; i++)
            {
                if (conditions[i].GetCondition(dialogCount) == false)
                {
                    return false;
                }
            }
            return true;
        }
    }
}