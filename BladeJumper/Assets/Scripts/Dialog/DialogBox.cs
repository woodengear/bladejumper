using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Dialog
{
    public class DialogBox : MonoBehaviour
    {
        /* 기능 설명
         * 텍스트가 표시되는 대화 상자
         * 타이핑 하는 것처럼 한글자씩 추가된다.
         * 
         * 움직이는 대상을 추적할수있음
         * 스킵기능
         * 
         */

        /* 변수 설명
         * Text text
         *      text가 표시될 TextUI
         * float interval
         *      타이핑 간격
         * int autoEnterCount
         *      자동으로 줄바꿈을 할 글자수
         * bool skip
         *      스킵버튼을 눌렀는지 확인하는 변수
         * bool skipable
         *      스킵할수 있는 텍스트인지 확인
         */

        [SerializeField]
        Text text;

        internal float interval = .1f;

        [SerializeField]
        int autoEnterCount = 10;

        [SerializeField]
        AudioClip typingSound;
        AudioSource sound;

        GameObject follow;
        DIALOG_DIR dir = DIALOG_DIR.UP;

        bool skip = false;
        bool skipable = true;

        internal void SetText(string str,ref DialogEndEvent hdr, bool skipable = true)
        {
            hdr += new DialogEndEvent(DialogEnd);
            this.skipable = skipable;
            //StartCoroutine(TextView(str));
        }
        

        internal void SetFollow(GameObject target, DIALOG_DIR dir)
        {
            follow = target;
            this.dir = dir;
        }

        void DialogEnd()
        {
            if(this != null)
            {
                Destroy(this.gameObject);
            }

        }

        void Follow()
        {
            Bounds bounds = follow.GetComponent<BoxCollider2D>().bounds;
            if(Vector2.Distance(transform.position, bounds.max + Vector3.up * .7f)>.05f)
                transform.position = bounds.max + Vector3.up * .7f;
        }
        // Start is called before the first frame update
        void Start()
        {
            sound = GetComponent<AudioSource>();

        }

        private void OnBecameInvisible()
        {
            Destroy(this.gameObject);
        }

        

        // Update is called once per frame
        void Update()
        {
            if (follow != null)
            {
                Follow();
            }
            if (skipable && DialogManager.instance.IsDialog() && text.text.Length > 1 && UserInput.WasPressed(UserInput.UserAction.Space))
            {
                skip = true;
            }
        }
        /* IEnumerator TextView(string str)
         * 순서
         *      1. 초기화
         *          skip 되어있지 않음
         *          audioSource 가져옴
         *          Ui의 텍스트 초기화
         *      2. 텍스트 입력
         *          
         *          추가될때마다 typingSound 재생
         *          현재 줄의 길이가 autoEntercount와 같다면
         *              엔터 추가
         *              이번에 추가될 str의 문자가 ' '이면 다음 주기로 넘어감
         *          str을 하나씩 textUI 에 추가
         *          
         *          스킵버튼이 눌린 상태라면(skip == true)
         *              텍스트UI의 텍스트 초기화
         *              줄바꿈 적용하며 전체 str 표시
         */
        internal IEnumerator TextView(string str)
        {
            sound = GetComponent<AudioSource>();
            text.text = "";
            skip = false;
            for (int i = 0; i < str.Length; i++)
            {
                sound.PlayOneShot(typingSound,SoundSetting.uiVolume);
                if(i%autoEnterCount == 0&&i!= 0)
                {
                    text.text += '\n';
                    if(str[i]==' ')
                    {
                        continue;
                    }
                }
                text.text += str[i];
                if (skip)
                {
                    text.text = "";
                    for(int j = 0; j < str.Length; j++)
                    {
                        
                        if (j % autoEnterCount == 0 && j != 0)
                        {
                            text.text += '\n';
                            if (str[j] == ' ')
                            {
                                continue;
                            }
                        }
                        text.text += str[j];
                    }
                    skip = false;
                    break;
                }
                yield return new WaitForSeconds(interval);
            }
            skip = false;
            //endImage.SetActive(true);
        }
    }
}