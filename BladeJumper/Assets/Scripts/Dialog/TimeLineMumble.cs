using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dialog;

namespace Dialog
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class TimeLineMumble : MonoBehaviour
    {
        [SerializeField]
        string text;
        [SerializeField]
        GameObject target;
        [SerializeField]
        float removeTime = 2;
        DialogEndEvent dehdr;
        public void PlayMumble()
        {

        }
        private void OnBecameVisible()
        {
            StartCoroutine(MumbleRutine());
        }

        private void OnBecameInvisible()
        {
            StopAllCoroutines();
            if (dehdr != null)
            {
                dehdr();
                dehdr = null;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        IEnumerator MumbleRutine()
        {

            yield return StartCoroutine(DialogManager.instance.CreateDialog(text, target, ref dehdr));

            yield return new WaitForSeconds(removeTime);
            if (dehdr != null)
            {
                dehdr();
                dehdr = null;
            }


        }
    }
}
