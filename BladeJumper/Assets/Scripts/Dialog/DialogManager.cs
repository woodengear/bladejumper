using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace Dialog
{
    public enum DIALOG_DIR { UP, DOWN, RIGHT, LEFT }
    public delegate void DialogEndEvent();

    [RequireComponent(typeof(Canvas))]
    public class DialogManager : MonoBehaviour
    {
        /* 기능 설명
         * 모든 대화 컨트롤
         * 대화상자를 만든다.
         * 
         * 대화상자 중복시 처리(미구현)
         */

        /* 변수
         * dialogCanvas
         * bool currentDialog
         *      현재 대화가 진행중인지 확인
         */
        public static DialogManager instance;

        Canvas dialogCanvas;
        [SerializeField]
        GameObject dialogPrefab;

        [SerializeField]
        float textTypingInterval = .1f;

        [SerializeField]
        GameObject talkBox;
        static internal GameObject talkBoxTarget;
        bool currentDialog = false;

        DialogEndEvent timelineHdr;

        Dictionary<GameObject, DialogEndEvent> hdrCheck = new Dictionary<GameObject, DialogEndEvent>();

        private void Update()
        {
            if(talkBoxTarget != null)
            {
                talkBox.SetActive(true);
                Bounds bounds = talkBoxTarget.GetComponent<BoxCollider2D>().bounds;
                talkBox.transform.position = bounds.max + Vector3.up * .7f;
                talkBoxTarget = null;
            }
            else
            {
                talkBox.SetActive(false);
            }
            
        }

        internal bool IsDialog()
        {
            return currentDialog;
        }
        internal void SetCurrentDialog(bool value)
        {
            if (value)
            {
                Animator anim = BladeJumper.bladeJumper.GetComponent<Animator>();
                anim.Rebind();
                anim.Update(0);
            }
            
            currentDialog = value;
            PlayerInputControl.manager.SetEnable(GetType().ToString(), !value);
        }

        internal IEnumerator CreateDialog(string str, GameObject target,ref DialogEndEvent hdr, DIALOG_DIR dir = DIALOG_DIR.UP, bool skipable = true)
        {
            
            DialogBox dialogBox = Instantiate<GameObject>(dialogPrefab, dialogCanvas.transform).GetComponent<DialogBox>();
            Bounds bounds = target.GetComponent<BoxCollider2D>().bounds;
            dialogBox.transform.position = bounds.max + Vector3.up * 1;
            dialogBox.SetText(str,ref hdr,skipable);
            dialogBox.SetFollow(target,dir);
            dialogBox.interval = textTypingInterval;
            return dialogBox.TextView(str);
        }

        // Start is called before the first frame update
        private void Awake()
        {
            instance = this;
            dialogCanvas = GetComponent<Canvas>();
        }


        /* TimeLineDialog
         * 타임라인에서 대화 표시를 위한 함수
         * 
         */
        internal void TimelineDialog(Playable playable,GameObject target, TimeLineDialogPlayableBehaviour behaviour, string str, bool waitInput)
        {
            StartCoroutine(TimelineDialogRoutine(playable, target, behaviour,str,waitInput));
        }

        IEnumerator TimelineDialogRoutine(Playable playable, GameObject target, TimeLineDialogPlayableBehaviour behaviour, string str, bool waitInput)
        {
            
            WaitUntil dialogUntil = new WaitUntil(() => (waitInput&& UserInput.WasPressed(UnityEngine.InputSystem.Key.Space))|| (!currentDialog || behaviour.end));
            DialogEndEvent deeHdr = null;

            yield return CreateDialog(str, target, ref deeHdr, DIALOG_DIR.UP,waitInput);
            
            if (waitInput)
            {
                yield return null;
                yield return new WaitUntil(() => !currentDialog || UserInput.WasPressed(UnityEngine.InputSystem.Key.Space));
                //playable.GetGraph().Play();
                playable.GetGraph().GetRootPlayable(0).SetTime(behaviour.clip.clipEnd);
                //playable.Play();
            }
            else
            {
                yield return new WaitUntil(() => (!currentDialog||behaviour.end));
            }

            if (deeHdr != null)
            {

                deeHdr();
            }
            behaviour.dialogEnd = true;
            
        }
    }
}