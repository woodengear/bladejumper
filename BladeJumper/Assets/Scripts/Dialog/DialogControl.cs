using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public struct DialogInfo
{
    public GameObject talker;
    public string animationState;
    [TextArea]
    public string dialog;
}

namespace Dialog {
    [RequireComponent(typeof(BoxCollider2D))]
    public class DialogControl : MonoBehaviour
    {

        /*
        * 조건
        *   캔디 갯수
        *   최대도달 높이
        *   마주친 횟수
        */


        [SerializeField]
        DialogInfo[] dialogs;

        [SerializeField]
        float interval;

        [SerializeField]
        bool follow = false;

        [SerializeField]
        bool disposable = true;

        [SerializeField]
        PlayableDirector nextPlayableDirector;

        DialogEndEvent hdr;
        bool visible = true;



        internal bool isPlay = false;

        private void OnDrawGizmos()
        {
            Bounds bounds = GetComponent<BoxCollider2D>().bounds;

            Gizmos.color = new Color(0, 0, 1, .5f);
            Gizmos.DrawCube(bounds.center, bounds.size);
        }

        // Start is called before the first frame update
        void Start()
        {
            isPlay = SaveManager.instance.GetDisposableUsed(this.gameObject.name.GetHashCode());
        }

        // Update is called once per frame
        void Update()
        {
            if (visible&&isPlay ==false)
            {
                Bounds bounds = GetComponent<BoxCollider2D>().bounds;
                RaycastHit2D hit = Physics2D.BoxCast(bounds.center, bounds.size, 0, Vector2.zero, 0,1 << 3);
                if (hit)
                {
                    AnimationControl.AnimState anim = hit.collider.GetComponent<AnimationControl>().currentAnim;
                    if (!(anim == AnimationControl.AnimState.Idle || anim == AnimationControl.AnimState.Walk))
                    {
                        return;
                    }
                    StartCoroutine(StartDialog());
                    isPlay = true;
                }
            }
        }
        private void OnBecameVisible()
        {
            visible = true;
        }
        private void OnBecameInvisible()
        {
            visible = false;
        }
       

        IEnumerator StartDialog()
        {
            DialogManager.instance.SetCurrentDialog(true);
            Animator pAnim = BladeJumper.bladeJumper.GetComponent<Animator>();
            pAnim.Rebind();
            pAnim.Update(0f);
            for (int i = 0; i < dialogs.Length; i++)
            {
                
                Animator animator = dialogs[i].talker.GetComponent<Animator>();
                if(animator != null&&dialogs[i].animationState != null)
                {
                    animator.Play(dialogs[i].animationState);
                }
                

                yield return StartCoroutine(DialogManager.instance.CreateDialog(dialogs[i].dialog, dialogs[i].talker,ref hdr));
                if (animator != null && dialogs[i].animationState != null)
                {
                    animator.Rebind();
                    animator.Update(0f);
                }
                yield return new WaitForSeconds(interval);

                yield return new WaitUntil(() => UserInput.WasPressed(UserInput.UserAction.Space));
                hdr();
                hdr = null;
            }
            DialogManager.instance.SetCurrentDialog(false);
            if(nextPlayableDirector != null)
            {
                nextPlayableDirector.Play();
            }
            SaveManager.instance.SetDisposableUsed(this.gameObject.name.GetHashCode(), true);
        }

        
    }
}