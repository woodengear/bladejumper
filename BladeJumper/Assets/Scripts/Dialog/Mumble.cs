using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dialog
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class Mumble : MonoBehaviour
    {
        [SerializeField]
        string[] text;
        [SerializeField]
        GameObject target;
        
        int index;

        [SerializeField]
        bool random = false;


        [SerializeField]
        float removeTime = 2;
        [SerializeField]
        float interval = 3;


        [SerializeField]
        DialogControl dialogPlayCheck;

        DialogEndEvent dehdr;

        BoxCollider2D boxCollider;
        bool isvisible = false;


        private void OnDrawGizmos()
        {
            Color color = Color.cyan;
            color.a = .5f;
            Gizmos.color = color;

            Bounds bounds = GetComponent<BoxCollider2D>().bounds;
            Gizmos.DrawCube(bounds.center, bounds.size);
            if(target != null)
            {
                Gizmos.DrawLine(bounds.center, target.transform.position);
            }
        }

        private void Awake()
        {
            boxCollider = GetComponent<BoxCollider2D>();
            if(target == null)
            {
                target = this.gameObject;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
           
        }

        // Update is called once per frame
        void Update()
        {

        }
        bool PlayerApproach()
        {
            if(boxCollider == null)
            {
                return true;
            }
            Vector2 playerPos = BladeJumper.bladeJumper.GetComponent<BoxCollider2D>().bounds.center;
            if (playerPos.y > boxCollider.bounds.min.y && playerPos.y < boxCollider.bounds.max.y &&
                playerPos.x > boxCollider.bounds.min.x && playerPos.x < boxCollider.bounds.max.x)
            {
                
                return true;
            }
            return false;
        }
        private void OnBecameVisible()
        {
            isvisible = true;
            StartCoroutine(MumbleRutine());
        }

        private void OnBecameInvisible()
        {
            isvisible = false;
            StopAllCoroutines();
            if (dehdr != null)
            {
                dehdr();
                dehdr = null;
            }
        }

        IEnumerator MumbleRutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(interval);
                
                if (!PlayerApproach()||DialogManager.instance.IsDialog())
                {
                    continue;
                }
                
                if (dialogPlayCheck != null && !SaveManager.instance.GetDisposableUsed(dialogPlayCheck.gameObject.name.GetHashCode()))
                {
                    continue;
                }
                if (random)
                {
                    index = Random.Range(0, text.Length);
                }
                
                yield return StartCoroutine(DialogManager.instance.CreateDialog(text[index], target, ref dehdr));
                
                yield return new WaitForSeconds(removeTime);
                if(dehdr != null)
                {
                    dehdr();
                    dehdr = null;
                }
                index++;
                if(index >= text.Length)
                {
                    index = 0;
                }
            }
            
        }
    }
}