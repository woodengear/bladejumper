using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using PlatformerControl;

public class SaveManager : MonoBehaviour
{
    internal static SaveManager instance;
    [SerializeField]
    bool dataReset = false;
    [SerializeField]
    GameObject dulgibot;
    
    
    PlayerData data = new PlayerData();
    CharacterCollider col;

    internal PlayerData userData { get => data; }

    internal Dictionary<int, int> usedObject = new Dictionary<int, int>();
    // Start is called before the first frame update
    private void Awake()
    {
        UserInput.ResetPad();
        instance = this;
        col = BladeJumper.bladeJumper.GetComponent<CharacterCollider>();
        LoadData();
    }

    private void Start()
    {
        CameraControl.controler.GridChange(SaveBestHeight);
       
    }
    internal void LoadData()
    {
        Debug.Log("LoadData");
        if (dataReset)
        {
            PlayerPrefs.DeleteKey("PlayerData");
            PlayerPrefs.DeleteKey("UsedObject");
        }
            
        if (PlayerPrefs.HasKey("PlayerData"))
        {
            data = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString("PlayerData"));
            BladeJumper.bladeJumper.transform.position = data.position;
            col.AddPower(data.velocity);
            Camera.main.transform.position = data.cameraPosition;
            dulgibot.transform.position = data.dulgiBotPos;
            KKRCandy.candyCount = data.candyCount;
            usedObject = JsonUtility.FromJson<Serialization<int,int>>(PlayerPrefs.GetString("UsedObject")).ToDictionary();
            Debug.Log("UsedObject" + usedObject.Count);
        }
        else
        {
            KKRCandy.candyCount = 0;
            data.candyCount = 0;
            data.position = BladeJumper.bladeJumper.transform.position;
        }
    }
    internal void SaveData()
    {
        data.position = BladeJumper.bladeJumper.transform.position;
        data.velocity = col.GetVelocity();
        data.cameraPosition = Camera.main.transform.position;
        data.dulgiBotPos = dulgibot.transform.position;
        data.candyCount = KKRCandy.candyCount;
        PlayerPrefs.SetString("PlayerData",JsonUtility.ToJson(data));


        

        string usedObjectjson = JsonUtility.ToJson(new Serialization<int, int>(usedObject));
        PlayerPrefs.SetString("UsedObject", usedObjectjson);
    }

    void SaveBestHeight(Vector2Int grid)
    {
        if (grid.x == 0 && data.bestHeight < grid.y)
        {
            data.bestHeight = grid.y;
        }
    }

    // Update is called once per frame
    void Update()
    {
        SaveData();
    }

    internal void SetDisposableUsed(int id, bool value)
    {
        if(usedObject.ContainsKey(id))
            usedObject[id] += 1;
        else
        {
            usedObject.Add(id, 1);
        }
    }

    internal bool GetDisposableUsed(int id)
    {
        //Debug.Log("id : "+ id);
        if (usedObject.ContainsKey(id))
        {
            return usedObject[id]>0;
        }
        else
        {
            return false;
        }
    }
    internal int GetUsedCount(int id)
    {
        if (usedObject.ContainsKey(id))
        {
            return usedObject[id];
        }
        else
        {
            return 0;
        }
    }
    internal void SetUsedCount(int id, int value)
    {
        if (usedObject.ContainsKey(id))
            usedObject[id] = value;
        else
        {
            usedObject.Add(id, value);
        }
    }
}

[Serializable]
public class PlayerData
{
    [SerializeField]
    internal Vector2 position;

    [SerializeField]
    internal Vector2 velocity;

    [SerializeField]
    internal Vector3 cameraPosition;

    [SerializeField]
    internal Vector2 dulgiBotPos;

    [SerializeField]
    internal int candyCount;

    [SerializeField]
    internal int bestHeight;

    public PlayerData(Vector2 position)
    {
        this.position = position;
    }

    public PlayerData() { }
}