using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Utilities;


public static class UserInput
{
    public enum UserAction {UP,Down,Left,Right,Space,Agree,Cancle,Menu}
    public enum UserDir { Up,Down,Left,Right}

    enum Device { Keyboard,Gamepad}

    struct StickMove
    {
        public bool up;
        public bool down;
        public bool left;
        public bool right;
    }
    


    static StickMove[] stickMove = new StickMove[2];
    static Vector2[] stickPivot = new Vector2[2];


    static Keyboard _keyboard
    {
        get 
        {
            
            if (Keyboard.current != null&& currentDevice == Device.Gamepad && Keyboard.current.IsPressed(.5f))
            {
                Debug.Log("keyboard");
                currentDevice = Device.Keyboard;
            }
            return Keyboard.current;
        }
    }
    static Gamepad _gamepad
    {
        get
        {
            return Gamepad.current;
        }
    }


    static Device currentDevice;
    internal static void ResetPad()
    {
        stickPivot[0] = GetGamepadStick();
        stickPivot[1] = GetGamepadStick(GamepadButton.RightShoulder);
        Debug.Log(stickPivot[0]);
    }
    internal static bool WasPressed(UserAction action,float threshold = 0.4f)
    {
        switch (action)
        {
            case UserAction.UP:
                return WasPressed(Key.UpArrow) || WasMoveStick(UserDir.Up, threshold);
            case UserAction.Down:
                return WasPressed(Key.DownArrow) || WasMoveStick(UserDir.Down, threshold);
            case UserAction.Left:
                return WasPressed(Key.LeftArrow) || WasMoveStick(UserDir.Left, threshold);
            case UserAction.Right:
                return WasPressed(Key.RightArrow) || WasMoveStick(UserDir.Right,threshold);
            case UserAction.Space:
                return WasPressed(Key.Space) || WasPressed(GamepadButton.A);
            case UserAction.Agree:
                return WasPressed(Key.Enter) || WasPressed(Key.Space) || WasPressed(GamepadButton.A);
            case UserAction.Cancle:
                return WasPressed(Key.Escape) || WasPressed(GamepadButton.B);
            case UserAction.Menu:
                return WasPressed(Key.Escape) || WasPressed(GamepadButton.Start);
        }
        return false;
    }

    internal static bool IsPress(UserAction action)
    {
        switch (action)
        {
            case UserAction.UP:
                return IsPressed(Key.UpArrow) || GetGamepadStick().y > 0;
            case UserAction.Down:
                return IsPressed(Key.DownArrow) || GetGamepadStick().y < 0;
            case UserAction.Left:
                return IsPressed(Key.LeftArrow) || GetGamepadStick().x < 0;
            case UserAction.Right:
                return IsPressed(Key.RightArrow) || GetGamepadStick().x > 0;
            case UserAction.Space:
                return IsPressed(Key.Space) || IsPressed(GamepadButton.A);
            case UserAction.Agree:
                return IsPressed(Key.Enter) || IsPressed(Key.Space) || IsPressed(GamepadButton.A);
            case UserAction.Cancle:
                return IsPressed(Key.Escape) || IsPressed(GamepadButton.B);
            case UserAction.Menu:
                return IsPressed(Key.Escape) || IsPressed(GamepadButton.Start);
        }
        return false;
    }

    

    internal static bool WasMoveStick(UserDir uDir,float threshhold = 0,GamepadButton stick = GamepadButton.LeftStick)
    {
        Vector2 dir = GetGamepadStick(stick);
        int stickIndex = 0;
        if(stick == GamepadButton.RightStick)
        {
            stickIndex = 1;
        }
        switch (uDir)
        {
            case UserDir.Up:
                if(dir.y > threshhold)
                {
                    if(stickMove[stickIndex].up == false)
                    {
                        stickMove[stickIndex].up = true;
                        return true;
                    }
                }
                else
                {
                    stickMove[stickIndex].up = false;
                }

                break;
            case UserDir.Down:
                if (dir.y < -threshhold)
                {
                    if (stickMove[stickIndex].down == false)
                    {
                        stickMove[stickIndex].down = true;
                        return true;
                    }
                }
                else
                {
                    stickMove[stickIndex].down = false;
                }
                break;
            case UserDir.Left:
                if (dir.x < -threshhold)
                {
                    if (stickMove[stickIndex].left == false)
                    {
                        stickMove[stickIndex].left = true;
                        return true;
                    }
                }
                else
                {
                    stickMove[stickIndex].left = false;
                }
                break;
            case UserDir.Right:
                if (dir.x > threshhold)
                {
                    if (stickMove[stickIndex].right == false)
                    {
                        stickMove[stickIndex].right = true;
                        return true;
                    }
                }
                else
                {
                    stickMove[stickIndex].right =false;
                }
                break;
        }
        return false;
    }

    internal static bool AnyPressed()
    {
        if ((_keyboard != null&& _keyboard.wasUpdatedThisFrame))
        {
            
            return true;
        }
        if(Gamepad.current != null)
        {
            for(int i = 4; i < 8; i++)
            {
                if (WasPressed((GamepadButton)i))
                {
                    return true;
                }
            }
        }
        return false;
    }

    internal static bool WasPressed(Key key)
    {
        if(_keyboard != null && _keyboard[key].wasPressedThisFrame)
        {
            return true;
        }
        return false;
    }
    internal static bool IsPressed(Key key)
    {
        if (_keyboard != null && _keyboard[key].isPressed)
        {
            return true;
        }
        return false;
    }

    internal static float ReadValue(Key key)
    {
        if (_keyboard != null)
        {
            return _keyboard[key].ReadValue();
        }
        return 0;
    }

    internal static Key IsPressCurrentKey()
    {
        if(_keyboard != null)
        {
            foreach(KeyControl key in _keyboard.allKeys)
            {
                if (key.isPressed)
                {
                    return key.keyCode;
                }
            }
        }
        return Key.None;
    }
    internal static Key WasPressedCurrentKey()
    {
        if (Keyboard.current != null)
        {
            foreach (KeyControl key in _keyboard.allKeys)
            {
                if (key.wasPressedThisFrame)
                {
                    return key.keyCode;
                }
            }
            
        }
        return Key.None;
    }


    internal static bool WasPressed(GamepadButton button)
    {
        if (_gamepad != null && _gamepad[button].wasPressedThisFrame)
        {
            currentDevice = Device.Gamepad;
            return true;
        }
        return false;
    }
    internal static bool IsPressed(GamepadButton button)
    {
        if (_gamepad != null && _gamepad[button].isPressed)
        {
            currentDevice = Device.Gamepad;
            return true;
        }
        return false;
    }

    internal static float ReadValue(GamepadButton button)
    {
        if (_gamepad != null)
        {
            return _gamepad[button].ReadValue();
        }
        return 0;
    }

    internal static string IsPressCurrentGamepadButton()
    {
        if (_gamepad != null)
        {
            foreach (ButtonControl key in _gamepad.allControls)
            {
                if (key.isPressed)
                {
                    return key.path;
                }
            }
        }
        return null;
    }
    internal static string WasPressedCurrentGamepadButton()
    {
        if (_gamepad != null)
        {
            foreach (InputControl key in _gamepad.allControls)
            {
                
                if (key.IsPressed())
                {
                    return key.path;
                }
            }
        }
        return null;
    }
    internal static Vector2 GetGamepadStick(GamepadButton stick = GamepadButton.LeftStick,float threshold = .2f)
    {
        if(_gamepad != null)
        {
            Vector2 dir = new Vector2();
            if(stick == GamepadButton.LeftStick)
            {
                dir = _gamepad.leftStick.ReadValue();
            }
            else if(stick == GamepadButton.RightStick)
            {
                dir = _gamepad.rightStick.ReadValue();
            }
            if(dir.magnitude > .2f)
            {
                return dir;
            }
            
        }
        return Vector2.zero;
    }

    internal static void PadVibrate(float low,float high)
    {
        if(_gamepad != null&&currentDevice == Device.Gamepad)
        {

            _gamepad.SetMotorSpeeds(low, high);
        }
    }
}
