using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EffectGenerator", menuName = "Effect/EffectGenerator")]
public class EffectGenerateControl : ScriptableObject
{
    [System.Serializable]
    class EffectInfo
    {
        public string name;
        public GameObject effect;
        [ColorUsage(true,true)]
        public Color color = new Color(1, 1, 1, 1);
        public Vector3 scale = new Vector3(1, 1, 1);
    }

    [SerializeField]
    EffectInfo[] effects;

    Dictionary<string, EffectInfo> effectDic;


    void CreateDictionary()
    {
        effectDic = new Dictionary<string, EffectInfo>();
        for(int i = 0; i < effects.Length; i++)
        {
            effectDic.Add(effects[i].name, effects[i]);
        }
    }
    internal GameObject GetEffect(string name)
    {
        if (effects.Length < 1) return null;
        if (effectDic == null) CreateDictionary();
        if (effectDic.ContainsKey(name))
        {
            return effectDic[name].effect;
        }
        else
        {
            Debug.LogError(name+" 이름의 이펙트가 없음");
            return null;
        }
    }

    internal GameObject GenerateEffect(string name, Vector2 position,bool flipX = false,Transform parent = null)
    {
        GameObject ef = GetEffect(name);
        if(ef != null)
        {
            GameObject ne = Instantiate<GameObject>(ef);
            ne.transform.position = position;
            ne.transform.parent = parent;
            ne.transform.localScale = effectDic[name].scale;
            SpriteRenderer neSr;
            if ((neSr = ne.GetComponent<SpriteRenderer>()) != null)
            {
                neSr.flipX = flipX;
                neSr.color = effectDic[name].color;
            }
            return ne;
        }
        return null;
    }

    internal GameObject GetEffect(int index)
    {
        if (effects.Length >= index) return null;
        return effects[index].effect;
    }
    internal bool Contains(string name)
    {
        if (effects.Length < 1) return false;
        if (effectDic == null) CreateDictionary();
        return effectDic.ContainsKey(name);
    }
}
