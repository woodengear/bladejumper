using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;


public enum KeyAction {UP,Down,LEFT,RIGHT,JUMP,KeyCount}


public static class KeySetting
{
    internal static Dictionary<KeyAction, Key> keys = new Dictionary<KeyAction, Key>();
    
}

public class KeyManager : MonoBehaviour
{
    const string keyVer = "KeySettingVersion";
    const string keyVerValue = "0.5";
    const string keyData = "KeySetting";
    internal static KeyManager instance;

    Key[] defaultKey = { Key.UpArrow ,Key.DownArrow, Key.LeftArrow, Key.RightArrow, Key.Space};
    internal delegate void KeyUpdateHandler();
    internal KeyUpdateHandler keyUpdateHandler;


    internal void Awake()
    {
        if (PlayerPrefs.HasKey(keyVer))
        {
            if(PlayerPrefs.GetString(keyVer) != keyVerValue)
            {
                SetDefaultKey();
                PlayerPrefs.SetString(keyVer, keyVerValue);
            }
        }
        else
        {
            SetDefaultKey();
            PlayerPrefs.SetString(keyVer, keyVerValue);
        }
        BladeJumperInputAction inputActions = new BladeJumperInputAction();
        //SetDefaultKey();
        instance = this;
        LoadKeySetting();
    }

    internal void LoadKeySetting()
    {
        if (PlayerPrefs.HasKey(keyData))
        {
            try
            {
                string setting = PlayerPrefs.GetString(keyData);
                if (setting != "")
                {
                    KeySetting.keys = JsonUtility.FromJson<Serialization<KeyAction, Key>>(setting).ToDictionary();
                }
            }
            catch (Exception)
            {
                SetDefaultKey();
                Debug.LogWarning("KeySetting Load Error");
                throw;
            }
            
        }
        else
        {
            SetDefaultKey();
            SaveKeySetting();
        }
        
        
    }

    internal void SaveKeySetting()
    {
        string setting = JsonUtility.ToJson(new Serialization<KeyAction, Key>(KeySetting.keys));
        PlayerPrefs.SetString(keyData, setting);
        
    }

    internal void SetDefaultKey()
    {
        KeySetting.keys.Clear();
        for(int i = 0; i < (int)KeyAction.KeyCount; i++)
        {
            KeySetting.keys.Add((KeyAction)i, defaultKey[i]);
        }
        SaveKeySetting();
        if(keyUpdateHandler != null)
            keyUpdateHandler();
    }

    internal void SetKey(KeyAction action, Key key)
    {
        KeySetting.keys[action] = key;
        SaveKeySetting();
        if (keyUpdateHandler != null)
            keyUpdateHandler();
    }

    
}
