using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct MonitorSetting
{
    [SerializeField]
    public int select;
    [SerializeField]
    public bool fullScreen;
}
public class GameWindowManager : MonoBehaviour
{
    const string PlayerData = "MonitorSetting";
    [SerializeField]
    static internal Vector2Int[] resolutions;
    [SerializeField]
    Vector2Int[] _resolutions;
    static internal MonitorSetting setting;
    private void Awake()
    {
        resolutions = _resolutions;
        if (PlayerPrefs.HasKey(PlayerData))
        {
            setting = JsonUtility.FromJson<MonitorSetting>(PlayerPrefs.GetString(PlayerData));
        }
        else
        {
            setting.fullScreen = true;
            setting.select = 5;
        }
        Screen.SetResolution(resolutions[setting.select].x, resolutions[setting.select].y, setting.fullScreen);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
