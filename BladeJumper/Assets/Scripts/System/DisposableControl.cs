using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisposableControl : MonoBehaviour
{
    [SerializeField]
    Component[] disposableScript;
    // Start is called before the first frame update
    void Start()
    {
        if (SaveManager.instance.GetDisposableUsed(this.GetInstanceID()))
        {
            for(int i = 0; i < disposableScript.Length; i++)
            {
                Destroy(disposableScript[i]);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void UpdateDisposable()
    {
        SaveManager.instance.SetDisposableUsed(this.GetInstanceID(),true);
        for (int i = 0; i < disposableScript.Length; i++)
        {
            Destroy(disposableScript[i]);
        }
    }
}
