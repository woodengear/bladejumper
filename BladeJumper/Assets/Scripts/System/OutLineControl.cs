using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class OutLineControl : MonoBehaviour
{
    enum Draw {Awake, Signal}

    public Shader shader;
    [SerializeField]
    Draw draw;
    public Vector2 thickness = new Vector2(.004f,004f);
    [ColorUsage(true,true)]
    public Color color;
    List<Color> colorList = new List<Color>();

    SpriteRenderer sRenderer;
    Material material;
    Animator animator;
    AnimatorStateInfo stateInfo;
    AnimatorClipInfo[] clipInfos;
    Texture2D lastTexture;
    Texture2D baseTexture;
    Vector2 thickRate;
    Vector2 textureThickness;
    bool active = false;
    


    // Start is called before the first frame update
    void Start()
    {
        sRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

       
        material = new Material(shader);
        sRenderer.material = material;
        if (draw == Draw.Awake)
        {
            active = true;
        }
        baseTexture = sRenderer.sprite.texture;
        lastTexture = baseTexture;

        ResetData();
    }

    void ThicknessUpdate()
    {
        //lastTexture = sRenderer.sprite.texture;
        //textureThickness = new Vector2(thickRate.x/ lastTexture.width, thickRate.y / lastTexture.height);
        //Debug.Log(sRenderer.sprite.name + " rThick : " + textureThickness.x +", "+ textureThickness.y + ", rect : "+ lastTexture.width+", "+ lastTexture.height);
        
        if (active||colorList.Count > 0)
        {
            material.SetVector("_thickness", thickness);
        }
        else
        {
            material.SetVector("_thickness", Vector4.zero);
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (lastTexture != sRenderer.sprite.texture)
        {
            ThicknessUpdate();
            //Debug.Log(sRenderer.sprite.texture.name + ". ppu : " + sRenderer.sprite.pixelsPerUnit + ", width : " + sRenderer.sprite.rect.width + ", height : "+sRenderer.sprite.rect.height);
        }
    }
    internal void SetDraw(bool active)
    {
        this.active = active;
        ThicknessUpdate();
    }
    internal void AddColor(Color color)
    {
        material.SetColor("_Color", color);
        colorList.Remove(color);
        colorList.Add(color);
        ThicknessUpdate();
    }
    internal void RemoveColor(Color color)
    {
        colorList.Remove(color);
        if(colorList.Count>0)
            material.SetColor("_Color", colorList[colorList.Count - 1]);
        ThicknessUpdate();
    }
    internal void ResetColor()
    {
        material.SetColor("_Color", color);
    }
    internal void SetThickness(Vector2 thick)
    {
        thickRate = new Vector2(thick.x * baseTexture.width, thick.y * baseTexture.height);
    }
    internal void ResetTheckness()
    {
        thickRate = new Vector2(thickness.x * baseTexture.width, thickness.y * baseTexture.height);
    }
    internal void ResetData()
    {
        ResetTheckness();
        ResetColor();
        material.SetTexture("_MainTex", sRenderer.sprite.texture);
        ThicknessUpdate();

    }
}
