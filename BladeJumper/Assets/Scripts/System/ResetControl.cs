using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Reflection;

public interface IResetObject
{
    public void ResetStart();
}

public class ResetControl : MonoBehaviour
{
    

    [SerializeField]
    internal bool resettable = true;
    public Component[] resetComponents;
    public Component[] resetCustomComponents;
    PropertyInfo[][] propertyInfos;
    FieldInfo[][] fieldInfos;
    object[][] values;
    object[][] customValues;
    bool copyEnd = false;
    // Start is called before the first frame update
    bool visible = false;
    int layer;

    Animator animator;
    SpriteRenderer spriteRenderer;
    Color defaultColor;
    Vector2 defaultPosition;

    Component CopyComponent(Component original, GameObject destination)
    {
        
        
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        // Copied fields can be restricted with BindingFlags
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy;
    }


    void CopyProperty()
    {
        propertyInfos = new PropertyInfo[resetComponents.Length][];
        values = new object[resetComponents.Length][];

        for(int i = 0;i < resetComponents.Length; i++)
        {
            System.Type type = resetComponents[i].GetType();
            //Debug.Log(type);
            propertyInfos[i] = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic |  BindingFlags.Instance);
            //Debug.Log(propertyInfos[i]);
            
            values[i] = new object[propertyInfos[i].Length];
            for (int j = 0;j < propertyInfos[i].Length; j++)
            {
                
                try
                {
                    values[i][j] = propertyInfos[i][j].GetValue(resetComponents[i]);
                }
                catch
                {

                }
                
            }
            
        }
        copyEnd = true;
    }
    void CopyField()
    {
        fieldInfos = new FieldInfo[resetCustomComponents.Length][];
        customValues = new object[resetCustomComponents.Length][];

        for (int i = 0; i < resetCustomComponents.Length; i++)
        {
            System.Type type = resetCustomComponents[i].GetType();
            //Debug.Log(type);
            fieldInfos[i] = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
            //Debug.Log(propertyInfos[i]);

            customValues[i] = new object[fieldInfos[i].Length];
            for (int j = 0; j < fieldInfos[i].Length; j++)
            {
                try
                {
                    customValues[i][j] = fieldInfos[i][j].GetValue(resetCustomComponents[i]);
                }
                catch
                {

                }

            }

        }
        copyEnd = true;
    }
    void ResetComponents()
    {
        /*
        for (int i = 0; i < resetComponents.Length; i++)
        {
            for (int j = 0; j < propertyInfos[i].Length; j++)
            {
                if(resetComponents[i].GetType() == typeof(GunmanControl))
                    Debug.Log(resetComponents[i].GetType()+"."+propertyInfos[i][j].Name);
                try
                {
                    propertyInfos[i][j].SetValue(resetComponents[i], values[i][j]);
                }
                catch 
                {
                }
                if(j == propertyInfos[i].Length - 1)
                {
                    //Debug.Log("End");
                }
            }
        }
        */
        for (int i = 0; i < resetCustomComponents.Length; i++)
        {
            
            //Component ncmp = this.gameObject.AddComponent(resetCustomComponents[i].GetType());
            for (int j = 0; j < fieldInfos[i].Length; j++)
            {
                
                try
                {
                    fieldInfos[i][j].SetValue(resetCustomComponents[i], customValues[i][j]);
                    //if (resetCustomComponents[i].GetType() == typeof(GunmanControl)) Debug.Log(resetCustomComponents[i].GetType() + "." + fieldInfos[i][j].Name + " : " + fieldInfos[i][j].GetValue(resetCustomComponents[i]));
                }
                catch
                {
                }
                if (j == fieldInfos[i].Length - 1)
                {
                    //Debug.Log("End");
                }
            }
            Component tmp = resetCustomComponents[i];
            if(resetCustomComponents[i] !=null && resetCustomComponents[i].GetComponent<IResetObject>() != null)
            {
                resetCustomComponents[i].GetComponent<IResetObject>().ResetStart();
            }
            //resetCustomComponents[i] = ncmp;
            //Destroy(tmp);
            
        }
    }

    private void OnBecameInvisible()
    {
        
        if (resettable&&copyEnd&&visible)
        {
            ResetBasicComponent();
            ResetComponents();
            gameObject.layer = layer;
        }
    }
    private void OnBecameVisible()
    {
        visible = true;
    }
    private void Awake()
    {
        
    }
    void Start()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultColor = spriteRenderer.color;
        defaultPosition = transform.position;
        layer = gameObject.layer;
        CopyField();
        //CopyProperty();
    }

    void ResetBasicComponent()
    {
        transform.position = defaultPosition;
        //spriteRenderer.color = defaultColor;
        if (animator != null)
        {
            animator.Rebind();
            animator.Update(0f);


            foreach (var parameter in animator.parameters)
            {
                if (parameter.type == AnimatorControllerParameterType.Bool)
                {
                    animator.SetBool(parameter.name, false);
                }
            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        
    }
}
