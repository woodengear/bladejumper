using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    static internal TimeManager instance;

    static internal float timeScale = 1;
    static internal bool timePause = false;
    static bool zeroSpeed = false;
    internal delegate bool EndCheck();
    internal delegate void AfterAction();
    float deltaTime;


    static internal void TimePause(bool pause)
    {
        timePause = pause;
        Time.timeScale = timeScale * (timePause ? 0 : 1);
        Time.fixedDeltaTime = Time.timeScale * .02f;
        
        TimeUpdate();
    }

    static internal void TimeScaleReset()
    {
        timeScale = 1;
    }

    static internal void SetTimeScale(float speedRate)
    {
        if(speedRate == 0)
        {
            zeroSpeed = true;
        }
        else
        {
            timeScale *= speedRate;
        }
 
        TimeUpdate();
    }
    
    static internal void ResetTimeScale(float speedRate)
    {

        if (speedRate == 0)
        {
            zeroSpeed = false;
        }
        else
        {
            timeScale /= speedRate;
        }
        TimeUpdate();
    }

    static void TimeUpdate()
    {
        Time.timeScale = timeScale * (timePause ? 0 : 1) * (zeroSpeed ? 0 : 1);
        Time.fixedDeltaTime = Time.timeScale * .02f;
    }
    
    private void Awake()
    {
        instance = this; 
    }
    internal void StartBulletTime(float speedRate, float endTime, AfterAction afterAction = null, EndCheck endCheck = null)
    {
        StartCoroutine(BulletTimeRoutine(speedRate,endTime,afterAction,endCheck));
    }

    static internal IEnumerator BulletTimeRoutine(float speedRate, float endTime,AfterAction afterAction =null,EndCheck endCheck = null)
    {
        if(speedRate == 0)
        {
            zeroSpeed = true;
        }
        else
        {
            timeScale *= speedRate;
        }
        TimeUpdate();
        Debug.Log("ss");
        float deltaTime = Time.fixedUnscaledDeltaTime;
        float time = 0;
        WaitUntil pauseEnd = new WaitUntil(() => timePause == false);
        WaitForSecondsRealtime wfs = new WaitForSecondsRealtime(deltaTime);
        while (time < endTime)
        {
            yield return wfs;
            yield return pauseEnd;
            if (endCheck != null&& endCheck())
            {
                break;
            }
            if(timePause == false)
                time += deltaTime;
        }
        
        if (speedRate == 0)
        {
            zeroSpeed = false;
        }
        else
        {
            timeScale /= speedRate;
        }
        TimeUpdate();

        if (afterAction != null)
        {
            afterAction();
        }
    }
}
