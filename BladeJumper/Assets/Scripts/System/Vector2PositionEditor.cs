using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(Vector2Position))]
public class Vector2PositionEditor : PropertyDrawer
{


    Vector3 obj;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //base.OnGUI(position, property, label);
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        
        var posRect = new Rect(position.x, position.y, 200,position.height);
        var buttonRect = new Rect(position.x -62,position.y,60,position.height);
        EditorGUI.PropertyField(posRect,property.FindPropertyRelative("position"),GUIContent.none);
        if (GUI.Button(buttonRect, "current") )
        {
            property.FindPropertyRelative("position").vector2Value = Selection.activeTransform.position;
        }
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    /*
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        EditorGUILayout.BeginHorizontal();
        obj = EditorGUILayout.Vector3Field("Positions", obj);
        if (GUILayout.Button("Capture Position"))
        {
            obj = Selection.activeTransform.position;
        }
        EditorGUILayout.EndHorizontal();
    }*/
}
#endif