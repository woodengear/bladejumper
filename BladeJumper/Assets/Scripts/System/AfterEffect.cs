using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterEffect : MonoBehaviour
{
    [SerializeField]
    float interval = .1f;
    [SerializeField]
    float lifetime = .2f;
    [SerializeField]
    Color effectColor = new Color(1,1,1);
    new SpriteRenderer renderer;

    IEnumerator _start;
    GameObject group;

    [SerializeField]
    internal bool active = true;
    
    private void Awake()
    {
        group = new GameObject(gameObject.name +" AfterEffect");
        renderer = GetComponent<SpriteRenderer>();
        _start = EffectStart();
    }
    private void Start()
    {
        StartCoroutine(EffectStart());
    }

    private void OnBecameVisible()
    {
        
    }
    private void OnBecameInvisible()
    {
        
    }

    IEnumerator EffectStart()
    {
        WaitForSeconds wfs = new WaitForSeconds(interval);
        WaitUntil use = new WaitUntil(() => active);
        while (true)
        {
            yield return use;
            GameObject ngo = new GameObject();
            SpriteRenderer sRenderer = ngo.AddComponent<SpriteRenderer>();
            sRenderer.sortingLayerID = renderer.sortingLayerID;
            sRenderer.sortingOrder = renderer.sortingOrder - 10;
            sRenderer.sprite = renderer.sprite;
            ngo.transform.position = transform.position;
            ngo.transform.rotation = transform.rotation;
            ngo.transform.localScale = transform.localScale;
            sRenderer.flipX = renderer.flipX;
            sRenderer.color = effectColor;
            AEF aEF = ngo.AddComponent<AEF>();
            aEF.lifeTime = lifetime;
            ngo.transform.parent = group.transform;
            yield return wfs;
            
        }
    }

    public class AEF : MonoBehaviour
    {
        SpriteRenderer sRenderer;
        internal float lifeTime;
        float startAlpha = 1;
        private void Start()
        {
            sRenderer = GetComponent<SpriteRenderer>();
            startAlpha = sRenderer.color.a;
        }
        private void FixedUpdate()
        {
            
            Color color = sRenderer.color;
            color.a -= 10* startAlpha * (Time.fixedDeltaTime/lifeTime);
            if(color.a <= 0)
            {
                Destroy(this.gameObject);
            }
            sRenderer.color = color;
        }
    }
}
