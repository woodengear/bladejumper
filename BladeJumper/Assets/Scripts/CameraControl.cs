using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


[RequireComponent(typeof(Camera))]
public class CameraControl : MonoBehaviour
{
    internal static CameraControl controler;

    Camera camera;

    public int gizmoHeight = 50;
    public int gizmoWeight = 50;
    public int gizmoLeft = 12;
    public int gizmoDown = 12;
    [SerializeField]
    bool viewGizmo = true;
    float orthographicSize = 5;
    Vector2 startPos;

    [SerializeField]
    internal Vector2Int grid;
    float height;
    float width;

    internal delegate void GridChangeHdr(Vector2Int vector2Int);

    GridChangeHdr gcHdr;

    internal void GridChange(GridChangeHdr hdr)
    {
        Debug.Log("gridC");
        gcHdr += hdr;
    }

    private void Awake()
    {
        controler = this;
        grid = Vector2Int.down;
        height = orthographicSize * 2;
        width = orthographicSize * Screen.width / Screen.height * 2;

    }

    int currentScene;

    private void OnDrawGizmos()
    {
        if (viewGizmo == false) return;
        int length = 5000;
        int center = 25;
        float height = Camera.main.orthographicSize;
        float wide = height * Camera.main.aspect;

        height = 5;
        wide = 8.888889f;
        int centerHeigth = gizmoDown * 2 + 1;
        int centerWidth = gizmoLeft * 2 + 1;

        float widthLength = ((gizmoHeight-1)*2- centerHeigth) * height;
        float heightLength = ((gizmoWeight-1)*2- centerWidth) * wide;
        Gizmos.color = Color.white;
        for(int i = 0; i < gizmoHeight; ++i)
        {
            Gizmos.DrawLine( Vector2.up * height * (i * 2 - centerHeigth) + Vector2.right * -centerWidth * wide, Vector2.up * height * (i * 2 - centerHeigth) + Vector2.right * heightLength);
        }
        for(int i = 0;i < gizmoWeight; ++i)
        {
            Gizmos.DrawLine( Vector2.right * wide * (i * 2 - centerWidth) + Vector2.up * -centerHeigth * height,  Vector2.right * wide * (i * 2 - centerWidth) + Vector2.up * widthLength);

        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
        currentScene = SceneManager.GetActiveScene().buildIndex;
        camera = GetComponent<Camera>();
        //Debug.Log(camera.orthographicSize);

        int setWidth = 1200; // 사용자 설정 너비
        int setHeight = 900; // 사용자 설정 높이

        int deviceWidth = Screen.width; // 기기 너비 저장
        int deviceHeight = Screen.height; // 기기 높이 저장

        //Screen.SetResolution(setWidth, (int)(((float)deviceHeight / deviceWidth) * setWidth),true); // SetResolution 함수 제대로 사용하기
        //Screen.SetResolution(setWidth, setHeight, true);
        

    }

    // Update is called once per frame
    void Update()
    {
        CalculateGrid();
        
        /*
        if (BladeJumper.bladeJumper.transform.position.y > camera.transform.position.y + camera.orthographicSize)
        {
            transform.Translate(Vector2.up * camera.orthographicSize * 2);
        }
        else if (BladeJumper.bladeJumper.transform.position.y < camera.transform.position.y - camera.orthographicSize)
        {
            transform.Translate(Vector2.down * camera.orthographicSize * 2);
        }


        if (BladeJumper.bladeJumper.transform.position.x > camera.transform.position.x + camera.orthographicSize * Screen.width / Screen.height)
        {
            transform.Translate(Vector2.right * camera.orthographicSize * Screen.width / Screen.height * 2);
        }
        else if (BladeJumper.bladeJumper.transform.position.x < camera.transform.position.x - camera.orthographicSize * Screen.width / Screen.height)
        {
            transform.Translate(Vector2.left * camera.orthographicSize * Screen.width / Screen.height * 2);
        }
        */
    }

    void CalculateGrid()
    {
        Vector2 playerPos = BladeJumper.bladeJumper.transform.position;
        Vector2Int preGrid = grid;
        grid = new Vector2Int(Mathf.FloorToInt((playerPos.x - width / 2) / width)+1 -3, Mathf.FloorToInt((playerPos.y - height / 2) / height)+1 +2);
        if(preGrid != grid)
        {
            Vector3 pos = transform.position;
            pos.x = (grid.x +3) * width;
            pos.y = (grid.y -2) * height;
            transform.position = pos;
            preGrid = grid;

            
            if(gcHdr != null)
            {
                
                gcHdr(grid);
            }
        }

        
    }

}
