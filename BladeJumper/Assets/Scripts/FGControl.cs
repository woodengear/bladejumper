using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(BoxCollider2D))]
public class FGControl : MonoBehaviour
{
    [SerializeField]
    LayerMask playerMask;
    [SerializeField]
    float alpha = .5f;


    BoxCollider2D col;

    Color color;
    ContactFilter2D cf2D = new ContactFilter2D();
    Collider2D[] overlapColliders = new Collider2D[1];

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<BoxCollider2D>();
        color = GetComponent<SpriteRenderer>().color;
        cf2D.layerMask = playerMask;
        cf2D.useLayerMask = true;
    }

    // Update is called once per frame
    void Update()
    {

        //Collider2D hit = Physics2D.OverlapBox(col.bounds.center, col.bounds.size, 90, playerMask);
        if (col.OverlapCollider(cf2D, overlapColliders) > 0)
        {
            color.a = alpha;
        }
        else
        {
            color.a = 1f;
        }
        GetComponent<SpriteRenderer>().color = color;
    }
}
