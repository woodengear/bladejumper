
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class SoundSetting
{
    const string soundData = "SoundDataInfo";
    [System.Serializable]
    class Setting
    {
        [SerializeField]
        public float bgmVolume;
        [SerializeField]
        public float fxVolume;
        [SerializeField]
        public float uiVolume;
    }

    static Setting setting;

    internal static float bgmVolume
    {
        get
        {
            if (setting == null)
            {
                LoadData();
            }
            return setting.bgmVolume;
        }

        set
        {
            setting.bgmVolume = value;
            SaveData();
        }
    }
    internal static float fxVolume
    {
        get
        {
            if (setting == null)
            {
                LoadData();
            }
            return setting.fxVolume;
        }

        set
        {
            setting.fxVolume = value;
            SoundFXControl.fxVolume = setting.fxVolume;
            SaveData();
        }
    }
    internal static float uiVolume
    {
        get
        {
            if(setting == null)
            {
                LoadData();
            }
            return setting.uiVolume;
        }

        set
        {
            setting.uiVolume = value;
            SaveData();
        }
    }
    public static AudioClip bgm;
    internal static void SetBGM(AudioClip clip)
    {
        if(clip!=null&&bgm != clip)
        {
            bgm = clip;
            SoundManager.instance.PlayBGM();
        }
    }
    internal static void SaveData()
    {
        PlayerPrefs.SetString(soundData, JsonUtility.ToJson(setting));
        
    }
    internal static void LoadData()
    {
        if (PlayerPrefs.HasKey(soundData))
        {
            setting = JsonUtility.FromJson<Setting>(PlayerPrefs.GetString(soundData));
            Debug.Log("sound setting bgm : " + setting.bgmVolume + ", fx : " + setting.fxVolume);
        }
        else
        {
            setting = new Setting();
            setting.bgmVolume = 1;
            setting.fxVolume = 1;
            setting.uiVolume = 1;
            PlayerPrefs.SetString(soundData, JsonUtility.ToJson(setting));
        }
        SoundFXControl.fxVolume = fxVolume;
    }
    
    
}
[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{

    internal static SoundManager instance;

    AudioSource audioSource;
    [SerializeField]
    AudioSource bgmAudio;
    float bgmvolume;
    // Start is called before the first frame update
    void Awake()
    {
        SoundSetting.LoadData();
        instance = this;
        audioSource = GetComponent<AudioSource>();
        bgmAudio.volume = SoundSetting.bgmVolume;
        bgmvolume = SoundSetting.bgmVolume;
    }

    // Update is called once per frame
    void Update()
    {
        if(bgmvolume != SoundSetting.bgmVolume)
        {
            bgmvolume = SoundSetting.bgmVolume;
            bgmAudio.volume = SoundSetting.bgmVolume;
        }
    }
    internal void PlayBGM(AudioClip nBGM)
    {

    }

    internal void PlayBGM()
    {
        StartCoroutine(BGMChange(SoundSetting.bgm));
    }

    internal void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    internal void PlaySound(AudioClip audioClip, float volume)
    {
        audioSource.PlayOneShot(audioClip, volume);
    }

    IEnumerator BGMChange(AudioClip nBGM)
    {
        float volume = bgmAudio.volume;
        for (int i = 0; i < 20; i++)
        {
            bgmAudio.volume -= volume * .05f;
            yield return new WaitForSeconds(.05f);
        }
        bgmAudio.Stop();
        bgmAudio.clip = nBGM;
        bgmAudio.volume = volume;
        yield return new WaitForSeconds(1f);
        bgmAudio.Play();
    }
}
