using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SoundInfo
{
    public string name;
    public AudioClip audioClip;
    [Range(0,1)]
    public float volume;
}

[RequireComponent(typeof(AudioSource))]
public class SoundFXControl : MonoBehaviour
{
    internal static float fxVolume = 1;
    [SerializeField]
    [Range(0,1)]
    float soundVolume = 1;
    public SoundInfo[] sounds;
    [SerializeField]
    bool playSoundManager = false;

    Dictionary<string, SoundInfo> audioClips = new Dictionary<string, SoundInfo>();
    AudioSource audioSource;

    // Start is called before the first frame update
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        for (int i = 0; i < sounds.Length; i++)
        {
            audioClips.Add(sounds[i].name, sounds[i]);
        }
    }

    // Update is called once per frame
    public void PlaySound(string clipName)
    {
        if (audioSource == null)
        {
            return;
        }
        //audioSource.clip = audioClips[clipName];
        if (playSoundManager)
        {
            if(SoundManager.instance != null)
            {
                SoundManager.instance.PlaySound(audioClips[clipName].audioClip, audioClips[clipName].volume * soundVolume* fxVolume);
            }
            else
            {
                Debug.LogWarning("카메라에 SoundManager 스크립트 추가");
            }
        }
        else
        {
            if (!audioClips.ContainsKey(clipName))
            {
                
                Debug.LogError("SoundFXError : "+gameObject.name+" is AudioClip(" + clipName +") not Found");
            }
            else
            {
                //audioSource.volume = fxVolume;
                audioSource.PlayOneShot(audioClips[clipName].audioClip, audioClips[clipName].volume * soundVolume * fxVolume);
            }
        }
    }
    internal void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip, soundVolume * fxVolume);
    }
    internal void PlaySound(string clipName, float volume, bool manager = false)
    {
        if (playSoundManager|| manager)
        {
            if (SoundManager.instance != null)
            {
                SoundManager.instance.PlaySound(audioClips[clipName].audioClip,volume * soundVolume * fxVolume);
            }
            else
            {
                Debug.LogWarning("카메라에 SoundManager 스크립트 추가");
            }
        }
        else
        {
            audioSource.volume = fxVolume;
            audioSource.PlayOneShot(audioClips[clipName].audioClip,volume*soundVolume * fxVolume);
        }
    }

    public void LocalSoundVolumeSet(float vol)
    {
        audioSource.volume = vol;
    }
}
