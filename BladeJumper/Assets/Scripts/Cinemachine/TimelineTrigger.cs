using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PlayableDirector))]
public class TimelineTrigger : MonoBehaviour
{
    static internal PlayableDirector currentPlay;
    internal PlayableDirector director;
    // Start is called before the first frame update

    [SerializeField]
    internal bool pause = false;

    [SerializeField]
    KeyCode playKey = KeyCode.Space;

    [SerializeField]
    bool playAwake = false;

    internal bool used = false;

    AnimationControl animCon;

    bool isPlay = false;

    [SerializeField]
    GameObject[] activeTarget;

    private void OnDrawGizmos()
    {
        Bounds bounds = GetComponent<BoxCollider2D>().bounds;
        Gizmos.color = new Color(0,1,0,.7f);
       
        Gizmos.DrawCube(bounds.center, bounds.size);
    }

    private void Awake()
    {
        director = GetComponent<PlayableDirector>();
        used = SaveManager.instance.GetDisposableUsed(this.gameObject.name.GetHashCode());
        if (playAwake&& !used)
        {
            Debug.Log("playawke");
            director.Play();
            PlayerInputControl.currentDirector = director;
            currentPlay = director;
            used = true;
            SaveManager.instance.SetDisposableUsed(this.gameObject.name.GetHashCode(), true);
        }
        if(used && activeTarget.Length > 0)
        {
            for(int i = 0; i < activeTarget.Length; i++)
            {
                if(activeTarget[i] != null)
                    activeTarget[i].SetActive(true);
            }
        }
    }
    void Start()
    {
        animCon = BladeJumper.bladeJumper.GetComponent<AnimationControl>();
        
    }

    private void Update()
    {
        if (pause)
        {
            if (UserInput.WasPressed(UnityEngine.InputSystem.Key.Space))
            {
                director.playableGraph.GetRootPlayable(0).SetSpeed(1);
                pause = false;
            }
        }
        if (isPlay&&used == false)
        {
            if ((animCon.currentAnim == AnimationControl.AnimState.Idle || animCon.currentAnim == AnimationControl.AnimState.Walk))
            {
                //플레이어가 트리거를 킬때 애니메이션을 초기화 하는 코드 작성 필요
                director.Play();
                PlayerInputControl.currentDirector = director;
                used = true;
                SaveManager.instance.SetDisposableUsed(this.gameObject.name.GetHashCode(), true);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Trigger");
        if (!used&&collision.gameObject.tag == "Player")
        {
            isPlay = true;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collision");
        
    }
    public void Pause()
    {
        Debug.LogWarning("Pause!!!!!!");
        pause = true;
        director.playableGraph.GetRootPlayable(0).SetSpeed(0);
    }


}
