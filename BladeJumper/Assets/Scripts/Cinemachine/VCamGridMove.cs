using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;

public class VCamGridMove : MonoBehaviour
{
    CinemachineVirtualCamera vcam;
    Camera camera;
    float orthographicSize = 5;
    Vector2 startPos;
    Vector2Int grid;
    float height;
    float width;

    private void Awake()
    {
        grid = Vector2Int.zero;
        height = orthographicSize * 2;
        width = orthographicSize * Screen.width / Screen.height * 2;

    }

    void Start()
    {
        vcam = GetComponent<CinemachineVirtualCamera>();
        camera = Camera.main;
        //Debug.Log(camera.orthographicSize);

        int setWidth = 1200; // 사용자 설정 너비
        int setHeight = 900; // 사용자 설정 높이

        int deviceWidth = Screen.width; // 기기 너비 저장
        int deviceHeight = Screen.height; // 기기 높이 저장

        //Screen.SetResolution(setWidth, (int)(((float)deviceHeight / deviceWidth) * setWidth),true); // SetResolution 함수 제대로 사용하기
        //Screen.SetResolution(setWidth, setHeight, true);
        

    }

    // Update is called once per frame
    void Update()
    {
        if (CinemachineCore.Instance.IsLive(vcam))
        {
            CalculateGrid();

        }
        
    }
    void CalculateGrid()
    {
        Vector2 playerPos = BladeJumper.bladeJumper.transform.position;

        grid = new Vector2Int(Mathf.FloorToInt((playerPos.x - width / 2) / width) + 1, Mathf.FloorToInt((playerPos.y - height / 2) / height) + 1);

        Vector3 pos = transform.position;
        pos.x = grid.x * width;
        pos.y = grid.y * height;
        transform.position = pos;
    }

}

