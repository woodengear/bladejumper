using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class TimeLineDialogPlayableClip : PlayableAsset, ITimelineClipAsset
{
    public TimeLineDialogPlayableBehaviour template = new TimeLineDialogPlayableBehaviour ();

    public double clipStart;
    public double clipEnd;

    public ClipCaps clipCaps
    {
        get { return ClipCaps.All; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<TimeLineDialogPlayableBehaviour>.Create (graph, template);
        TimeLineDialogPlayableBehaviour clone = playable.GetBehaviour ();
        clone.clip = this;
        
        return playable;
    }

    
}
