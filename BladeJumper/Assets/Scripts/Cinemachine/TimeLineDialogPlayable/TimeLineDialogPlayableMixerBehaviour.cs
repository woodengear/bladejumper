using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Dialog;

public class TimeLineDialogPlayableMixerBehaviour : PlayableBehaviour
{
    bool dialogSet = false;

    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        GameObject trackBinding = playerData as GameObject;

        if (!trackBinding)
            return;

        if (dialogSet)
        {
            Debug.Log("DialogSet");
            if (dialogSet && DialogManager.instance != null)
            {
                DialogManager.instance.SetCurrentDialog(true);
                dialogSet = false;
            }
        }
        

        int inputCount = playable.GetInputCount ();

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<TimeLineDialogPlayableBehaviour> inputPlayable = (ScriptPlayable<TimeLineDialogPlayableBehaviour>)playable.GetInput(i);
            TimeLineDialogPlayableBehaviour input = inputPlayable.GetBehaviour ();


            //behaviour가 끝날때, 해당 behaviour의  EndBehaviour()를 실행하게 한다.
            if (input.end==false&&playable.GetPreviousTime() > input.clip.clipEnd)
            {
                input.EndBehaviour();
            }
            // Use the above variables to process each frame of this playable.
            
        }
    }

    public override void OnPlayableCreate(Playable playable)
    {
        //해당 트랙이 생성될 때, Dialog가 켜져있는것으로  취급
        if(DialogManager.instance != null)
        {
            DialogManager.instance.SetCurrentDialog(true);
        }
        else
        {
            dialogSet = true;
        }


        base.OnPlayableCreate(playable);
       
    }
    public override void OnPlayableDestroy(Playable playable)
    {
        //해당 트랙이 종료되면  켰던 Dialog 종료
        if (DialogManager.instance != null)
            DialogManager.instance.SetCurrentDialog(false);
        
        base.OnPlayableDestroy(playable);
    }
}
