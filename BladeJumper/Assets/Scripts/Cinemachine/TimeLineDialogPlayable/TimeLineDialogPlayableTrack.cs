using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackColor(0.855f, 0.8623f, 0.87f)]
[TrackClipType(typeof(TimeLineDialogPlayableClip))]
[TrackBindingType(typeof(GameObject))]
public class TimeLineDialogPlayableTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        foreach (var clip in GetClips())
        {
            var customClip = clip.asset as TimeLineDialogPlayableClip;
            if (customClip != null)
            {
                //해당 클립에 클립의 시작 시간시간과 종료 시간 저장
                customClip.clipStart = clip.start;
                customClip.clipEnd = clip.end;
               
            }
        }
        return ScriptPlayable<TimeLineDialogPlayableMixerBehaviour>.Create (graph, inputCount);
    }

    protected override void OnAfterTrackDeserialize()
    {
        
        base.OnAfterTrackDeserialize();
    }
    
    
}
