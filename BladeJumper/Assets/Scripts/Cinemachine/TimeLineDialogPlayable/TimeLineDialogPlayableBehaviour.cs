using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using Dialog;
[Serializable]
public class TimeLineDialogPlayableBehaviour : PlayableBehaviour
{
    /* 함수 설명
     * 타임라인에서 Dialog 사용하기 위한 Behaviour 코드
     * 
     */
    /* 변수 설명
     * bool inputWait
     *      입력을 대기할지에 대한 여부
     *      true    :   입력할 때까지 해당 behaviour 반복(끝에 도달하면 timeline 시간이 behaviour 시작 시간으로 변경됨)  
     *      false   :   behaviour이 끝나면 해당 dialog 가 종료됨
     * string dialogText
     *      대화상자에 들어갈 텍스트
     *      
     * bool start
     *      대화가 시작되었는지 확인
     *      false 라면 대화상자를 생성하고 true로 변경한다.
     *      
     * bool end
     *      behaviour가 끝났는지 확인한다.
     *      DialogManager가 확인하는 용도로 사용
     *      
     * bool dialogEnd
     *      대화가 끝났을때 true
     *      DialogManager가 해당 대화 루틴 종료시 true로 변경
     *      
     * TimeLineDialogPlaybleClip clip
     *      해당 behaviour의 clip
     *      시작시간과 종료시간을 확인하기 위해 사용
     *        
     * Playable currentPlayable
     *      해당 behaviour가 종료될 때 사용하기위해 Playable을 저장
     */
    public bool inputWait = false;
    public string dialogText;

    internal bool start = false;
    internal bool end = false;
    internal bool dialogEnd = false;
    internal TimeLineDialogPlayableClip clip;
    Playable currentPlayable;
    

    

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        //대화상자를 시작하지 않았으면 실행.
        if (start == false&&dialogText != "")
        {
            
            currentPlayable = playable;
            
            start = true;
            if (DialogManager.instance != null)
                DialogManager.instance.TimelineDialog(playable,playerData as GameObject, this,dialogText,inputWait);
        }
        base.ProcessFrame(playable, info, playerData);
    }
    
    internal void EndBehaviour()
    {
        end = true;
        if (inputWait&&dialogEnd == false)
        {
            end = false;
            currentPlayable.GetGraph().GetRootPlayable(0).SetTime(clip.clipStart);
        }
    }
}
