using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;


public class AnimationControl : MonoBehaviour
{
    /*
     * BladeJumper의 애니메이션 관리
     * bj의 상태에 따라서 애니메이션 설정
     * 
     * 우선 순위
     *      대기
     *      걷기
     *      
     */

    BladeJumper bj;
    CharacterCollider col;
    Animator anim;
    PlayerInputControl pi;
    SpriteRenderer sr;
    [SerializeField]
    GameObject attackArea;
    internal enum AnimState {Idle,Walk,SlashFront,SlashDelay,SlashPreDelay,JumpDown,JumpUp,FallDown,Hit}

    [SerializeField]
    internal AnimState currentAnim = AnimState.Idle;

    float faceDir = 1;
    float lastFaceDir = 1;
    internal bool lastSlash = false;

    [SerializeField]
    internal bool delay = false;

    (AnimationState, bool) del;

    /*
     * 
     */

    // Start is called before the first frame update
    void Start()
    {
        
        bj = GetComponent<BladeJumper>();
        col = GetComponent<CharacterCollider>();
        anim = GetComponent<Animator>();
        pi = GetComponent<PlayerInputControl>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.speed = col.GetTimeScale();

        if (Dialog.DialogManager.instance.IsDialog() || !pi.GetInputEnable("Animation"))
        {
            
            return;
        }
        
        /*
        float faceVelocity = PlayerInput.direction.x;
        if (col.colBox.descendingSlope || !col.colBox.below || (col.colBox.below && bj.slashStart))
        {
            faceVelocity = col.GetVelocity().x;
        }

        if (false&&Mathf.Abs(faceVelocity) > .01f && faceDir * faceVelocity < 0)
        {
            faceDir = Mathf.Sign(faceVelocity);
            Vector3 scale = transform.localScale;
            scale.x = faceDir * Mathf.Abs(scale.x);
            transform.localScale = scale;
        }

        */
        FaceFlip();

        StateUpdate();

        AnimationUpdate();

        

        lastSlash = bj.slashStart;
    }

    void StateUpdate()
    {
        if (delay)
        {
            if (bj.playerHit)
            {
                pi.SetEnable("BladeJumper", true);
                delay = false;
            }
            if (bj.instantSlash)
            {
                pi.SetEnable("BladeJumper", true);
                delay = false;
            }
            return;
        }

        currentAnim = AnimState.Walk;

        if (bj.velocity.x == 0 && col.colBox.below)
        {
            currentAnim = AnimState.Idle;
        }

        if (bj.slashKeyDown > 0)
        {
            currentAnim = AnimState.SlashPreDelay;
        }

        if (!col.colBox.below || (col.GetVelocity().y < 0 && !col.colBox.climbing) || col.colBox.descendingSlope)
        {

            if (col.GetVelocity().y < 0)
                currentAnim = AnimState.JumpDown;
            else
                currentAnim = AnimState.JumpUp;

        }

        if (bj.slashStart)
        {
            currentAnim = AnimState.SlashFront;
        }

        if (lastSlash && !bj.slashStart)
        {
            delay = true;


            pi.SetEnable("BladeJumper", false);
            //Debug.Log("delay");
            currentAnim = AnimState.SlashDelay;
        }

        if (bj.instantSlash)
        {
            currentAnim = AnimState.JumpDown;
        }


        if (bj.fallDown)
        {
            delay = false;
            currentAnim = AnimState.FallDown;
        }

        if (bj.playerHit)
        {
            delay = false;
            pi.SetEnable("BladeJumper", true);
            currentAnim = AnimState.Hit;
        }
    }
    void AnimationUpdate()
    {
        switch (currentAnim)
        {
            case AnimState.Idle:
                anim.Play("PlayerIdle");
                break;
            case AnimState.Walk:
                InputFace();
                anim.Play("PlayerWalk");
                break;
            case AnimState.SlashFront:
                anim.Play("Player_Slash_Front");
                break;
            case AnimState.SlashDelay:
                anim.Play("Slash_Front_Delay");
                break;
            case AnimState.JumpDown:
                anim.Play("Player_Jump_Down");
                break;
            case AnimState.JumpUp:
                anim.Play("Player_Jump_Up");
                break;
            case AnimState.SlashPreDelay:
                anim.Play("Player_Slash_PreDelay");
                break;
            case AnimState.FallDown:
                anim.Play("Player_Fall_Down");
                break;
            case AnimState.Hit:
                anim.Play("Player_Hit");
                break;
        }
    }

    public void SlashEnd()
    {
        pi.SetEnable("BladeJumper", true);
        delay = false;
    }

    void FaceFlip()
    {
        Vector2 velocity = col.GetVelocity();
        
        if(velocity.x != 0)
        {
            sr.flipX = Mathf.Sign(velocity.x) < 0 ? true : false;
        }else if(bj.slashStart ==false&&delay == false&&(PlayerInputControl.GetPlayerDir().x != 0 && bj.slashKeyDown > 0))
        {
            sr.flipX = Mathf.Sign(PlayerInputControl.GetPlayerDir().x) < 0 ? true : false;
        }
        AttackAreaFlip();
    }
    void InputFace()
    {
        if(PlayerInputControl.direction.x != 0)
        {
            //Vector3 scale = transform.localScale;
            //scale.x = Mathf.Abs(scale.x) * Mathf.Sign(PlayerInput.direction.x);

            //transform.localScale = scale;
            sr.flipX = Mathf.Sign(PlayerInputControl.direction.x) < 0 ? true : false;
        }
        AttackAreaFlip();
    }
    void AttackAreaFlip()
    {
        Vector2 scale = attackArea.transform.localScale;
        scale.x = Mathf.Abs(scale.x) * (sr.flipX ? -1 : 1);
        attackArea.transform.localScale = scale;
    }
}
