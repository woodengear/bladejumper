using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectControl : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    GameObject[] additionalEffects;

    internal BoxCollider2D target;

    public void DestroyEffect()
    {
        Destroy(this.gameObject);
    }
    void Start()
    {
        if(additionalEffects != null)
        {
            for (int i = 0; i < additionalEffects.Length; i++)
            {
                GameObject effect = Instantiate<GameObject>(additionalEffects[i]);
                effect.transform.position = transform.position;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            transform.position = target.transform.position;
        }
    }
}
