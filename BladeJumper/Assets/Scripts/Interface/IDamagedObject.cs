﻿using UnityEngine;
using System;

public interface IDamagedObject
{
    public int GetHP();
    public bool Damaged(int damage, Vector2 dir = new Vector2(), Vector2 power = new Vector2(), GameObject actor = default);
}