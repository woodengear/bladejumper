using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    static internal UIManager instance;
    [SerializeField]
    GameObject uiBox;

    
    internal Stack<GameObject> openUI = new Stack<GameObject>();

    [SerializeField]
    internal bool esc_use = true;

    [SerializeField]
    bool startOpen = false;

    bool padCancle = false;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    private void Start()
    {
        if (startOpen)
        {
            TimeManager.TimePause(false);
            UIPush(uiBox);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if((Dialog.DialogManager.instance != null && Dialog.DialogManager.instance.IsDialog())||ImageCutscene.playing)
        {
            return;
        }
         


        if (esc_use && openUI.Count > 0)
            UIControl.control.KeyControl();

        padCancle = UserInput.WasPressed(GamepadButton.B);
        bool padMenu = UserInput.WasPressed(GamepadButton.Start);
        
        

        if (esc_use&&(UserInput.WasPressed(Key.Escape) || padCancle))
        {
            
            ESC_Action();
            if (!startOpen&&uiBox.activeInHierarchy)
            {
                TimeManager.TimePause(true);
            }
            else
            {
                TimeManager.TimePause(false);
            }
        }

        if (padMenu)
        {
            if(openUI.Count > 0)
            {
                ReturnPlay();
            }
            else
            {
                ESC_Action();
            }
            if (!startOpen && uiBox.activeInHierarchy)
            {
                TimeManager.TimePause(true);
            }
            else
            {
                TimeManager.TimePause(false);
            }
        }
        
    }

    internal void ESC_Action()
    {
        if (startOpen && openUI.Count < 2)
        {
            return;
        }
        if (padCancle && openUI.Count == 0)
        {
            return;
        }

        if (openUI.Count == 0)
        {
            uiBox.SetActive(true);
            UIPush(uiBox);
        }
        else
        {
            UIPop().SetActive(false);
        }
    }

    void UIPush(GameObject nObj)
    {
        openUI.Push(nObj);
        UIControl.control.ButtonLoad(true);
    }
    GameObject UIPop()
    {
        GameObject ui = openUI.Pop();
        if(openUI.Count > 0)
            UIControl.control.ButtonLoad();
        return ui;
    }

    public void ReturnPlay()
    {
        if (!esc_use) return;
        while(openUI.Count > 0)
        {
            UIPop().SetActive(false);
        }
        TimeManager.TimePause(false);
    }

    public void MoveTitle()
    {
        TimeManager.TimePause(false);
        SceneManager.LoadScene(0);
    }

    public void NewGame()
    {
        PlayerPrefs.DeleteKey("PlayerData");
        PlayerPrefs.DeleteKey("UsedObject");
        GameStart();
    }



    public void GameStart()
    {
        TimeManager.TimePause(false);
        SceneManager.LoadScene(1);
        
    }

    public void ExitGame()
    {
        Application.Quit();   
    }

    public void OpenUI(GameObject ui)
    {
        if (openUI.Contains(ui))
        {
            if (!esc_use) return;
            while (openUI.Contains(ui))
            {
                UIPop().SetActive(false);
            }
        }
        else
        {
            ui.SetActive(true);
            UIPush(ui);
            
        }


    }
    public void ReturnUI(GameObject ui)
    {
        if (!esc_use) return;
        while (openUI.Contains(ui))
        {
            UIPop().SetActive(false);
        }
        
    }
    public void CloseAfterUI(GameObject ui)
    {
        if (!esc_use) return;
        while (openUI.Contains(ui) && openUI.Peek() != ui)
        {
            UIPop().SetActive(false);
        }
    }

    

}
