using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonitorSettingUI : MonoBehaviour
{
    const string PlayerData = "MonitorSetting";


    [SerializeField]
    static internal MonitorSetting setting;
    [SerializeField]
    Dropdown resolutionSelect;
    [SerializeField]
    GameObject resolutionSetLayout;
    [SerializeField]
    GameObject[] resolutionSelectUIs;
    [SerializeField]
    Text winmodText;

    Vector2Int[] resolutions;


    private void Awake()
    {
        resolutions = GameWindowManager.resolutions;
        resolutionSelect.ClearOptions();
        List<string> options = new List<string>();
        for(int i = 0; i < resolutions.Length; i++)
        {
            options.Add( resolutions[i].x + "x" + resolutions[i].y);
        }
        resolutionSelect.AddOptions(options);

        setting = GameWindowManager.setting;
        resolutionSelect.value = setting.select;
        if (setting.fullScreen)
        {
            winmodText.text = "창 모드";
        }
        else
        {
            winmodText.text = "전체 화면";
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void UpdateUI(int index)
    {
        Debug.Log("Reso index : " + index);
        for(int i = 0; i< 3; i++)
        {
            if(i == index)
            {
                resolutionSelectUIs[i].GetComponent<Image>().color = Color.white;
                Text[] inTexts = resolutionSelectUIs[i].GetComponentsInChildren<Text>();
                for(int j = 0; j < inTexts.Length; j++)
                {
                    inTexts[j].color = Color.black;
                }
                continue;
            }
            resolutionSelectUIs[i].GetComponent<Image>().color = Color.black;
            Text[] inTexts2 = resolutionSelectUIs[i].GetComponentsInChildren<Text>();
            for (int j = 0; j < inTexts2.Length; j++)
            {
                inTexts2[j].color = Color.white;
            }
        }
    }


    // Update is called once per frame
    IEnumerator ResolutionChangeRoutine()
    {
        resolutionSetLayout.SetActive(true);
        UIManager.instance.esc_use = false;

        int index = 0;
        int preIndex = -1;

        yield return null;
        while (true)
        {
            if (UserInput.WasPressed(UserInput.UserAction.UP))
            {
                if(index > 0) index--;
            }
            else if (UserInput.WasPressed(UserInput.UserAction.Down))
            {
                if(index < 2) index++;
            }
            if(preIndex != index)
            {
                UpdateUI(index);
                preIndex = index;
            }

            switch (index)
            {
                case 0:
                    if (UserInput.WasPressed(UserInput.UserAction.Left))
                    {
                        if(resolutionSelect.value > 0)
                        {
                            resolutionSelect.value = resolutionSelect.value - 1;
                        }
                    }
                    else if (UserInput.WasPressed(UserInput.UserAction.Right))
                    {
                        if (resolutionSelect.value < resolutions.Length -1)
                        {
                            resolutionSelect.value = resolutionSelect.value + 1;
                        }
                    }
                        break;
                case 1:
                    if (UserInput.WasPressed(UserInput.UserAction.Right))
                    {
                        index++;
                    }
                    if (UserInput.WasPressed(UserInput.UserAction.Agree))
                    {
                        resolutionSelect.value = setting.select;
                        index = -1;
                    }
                    break;
                case 2:
                    if (UserInput.WasPressed(UserInput.UserAction.Left))
                    {
                        index--;
                    }
                    if (UserInput.WasPressed(UserInput.UserAction.Agree))
                    {
                        ChangeResolution();

                        index = -1;
                    }
                    break;
            }

            if (UserInput.WasPressed(UserInput.UserAction.Cancle)||index < 0)
            {
                break;
            }

            yield return null;
        }

        yield return null;
        resolutionSetLayout.SetActive(false);
        UIManager.instance.esc_use = true;
    }

    public void ResolutionDropDown()
    {

        StartCoroutine(ResolutionChangeRoutine());
            
    }
    public void ChangeResolution()
    {
        Screen.SetResolution(resolutions[resolutionSelect.value].x, resolutions[resolutionSelect.value].y, setting.fullScreen);
        setting.select = resolutionSelect.value;
        PlayerPrefs.SetString(PlayerData, JsonUtility.ToJson(setting));

        StopAllCoroutines();
        resolutionSetLayout.SetActive(false);
        UIManager.instance.esc_use = true;
    }

    public void CancleButton()
    {
        StopAllCoroutines();
        resolutionSetLayout.SetActive(false);
        UIManager.instance.esc_use = true;
    }

    public void ChangeWindowMode(Text text)
    {
        setting.fullScreen = !setting.fullScreen;
        Screen.SetResolution(resolutions[setting.select].x, resolutions[setting.select].y, setting.fullScreen);
        if (setting.fullScreen)
        {
            text.text = "창 모드";
        }
        else
        {
            text.text = "전체 화면";
        }
        PlayerPrefs.SetString(PlayerData, JsonUtility.ToJson(setting));
    }

    
}
