using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIControl : MonoBehaviour
{

    Canvas playerUI;
    BladeJumper bj;
    [SerializeField]
    GameObject DirArrow;
    Vector2 pivot;


    // Start is called before the first frame update
    void Start()
    {
        bj = BladeJumper.bladeJumper;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        pivot = bj.GetComponent<BoxCollider2D>().bounds.center;

        if(PlayerInputControl.GetPlayerDir() != Vector2.zero&&PlayerInputControl.IsPressedJumpKey())
        {
            DirArrow.SetActive(true);
            DirArrow.transform.position = pivot + PlayerInputControl.GetPlayerDir().normalized;

            DirArrow.transform.eulerAngles = new Vector3(0,0,Vector2.SignedAngle(Vector2.right, PlayerInputControl.GetPlayerDir()));
        }
        else
        {
            DirArrow.SetActive(false);
        }
    }

    Vector2 PlayerPosition2Camera()
    {
        
        return Camera.main.WorldToScreenPoint(bj.transform.position); ;
    }
}
