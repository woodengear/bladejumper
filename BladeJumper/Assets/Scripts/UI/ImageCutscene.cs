using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class ImageCutscene : MonoBehaviour
{
    internal static ImageCutscene instance;
    internal static bool playing = false;
    [SerializeField]
    Image imageTarget;
    [SerializeField]
    Text subTitle;

    [SerializeField]
    GameObject uiScreen;
    [SerializeField]
    GameObject nextUI;


    [SerializeField]
    float textTypingInterval = .1f;

    [SerializeField]
    AudioClip typingSound;

    AudioSource audio;

    bool skip = false;

    private void Awake()
    {
        instance = this;
        uiScreen.SetActive(false);
        nextUI.SetActive(false);
        audio = GetComponent<AudioSource>();


    }

    internal void StartCutscene(ImageCutsceneData data)
    {
        uiScreen.SetActive(true);
        StartCoroutine(CutsceneRoutine(data));
    }

    private void Update()
    {
        if(skip == false && subTitle.text.Length > 2 && UserInput.WasPressed(UserInput.UserAction.Space))
        {
            skip = true;
        }
    }

    void ImageChange(Sprite sprite)
    {
        imageTarget.sprite = sprite;
    }
    // 텍스트를 키보드 입력처럼 표시
    IEnumerator TextTyping(string str)
    {
        subTitle.text = "";
        WaitForSeconds wfs = new WaitForSeconds(textTypingInterval);

        for(int i = 0; i < str.Length; i++)
        {
            if (skip)
            {
                subTitle.text = str;

                yield return wfs;

                skip = false;
                break;
            }
            audio.PlayOneShot(typingSound,SoundSetting.uiVolume);
            subTitle.text += str[i];
            yield return wfs;
        }
    }

    //다음텍스트 or 이미지를 보기위해 Key입력 및 설명 표시
    IEnumerator NextCheck(WaitUntil wu)
    {
        nextUI.SetActive(true);
        Animator anim = nextUI.GetComponent<Animator>();
        anim.Rebind();
        anim.Update(0f);
        yield return wu;
        nextUI.SetActive(false);
    }

    IEnumerator CutsceneRoutine(ImageCutsceneData cutscene)
    {
        PlayerInputControl.manager.SetEnable(GetType().ToString(),false);
        WaitUntil nextCheck = new WaitUntil(() => UserInput.WasPressed(UserInput.UserAction.Space));
        playing = true;
        if(TimelineTrigger.currentPlay != null)
        {
            TimelineTrigger.currentPlay.playableGraph.GetRootPlayable(0).SetSpeed(0);
        }
        AudioClip bgm = SoundSetting.bgm;

        for (int i = 0; i < cutscene.data.Length; i++)
        {
            
            ImageChange(cutscene.data[i].image);
            if(cutscene.data[i].bgm != null)
            {
                SoundSetting.SetBGM(cutscene.data[i].bgm);
            }
            for(int j = 0; j < cutscene.data[i].subTitle.Length; j++)
            {
                if(cutscene.data[i].subTitle[j].sound != null)
                {
                    audio.PlayOneShot(cutscene.data[i].subTitle[j].sound);
                }
                
                yield return TextTyping(cutscene.data[i].subTitle[j].text);
                yield return NextCheck(nextCheck);
                subTitle.text = "";
                skip = false;
            }
            yield return NextCheck(nextCheck);
        }
        if (TimelineTrigger.currentPlay != null)
            TimelineTrigger.currentPlay.playableGraph.GetRootPlayable(0).SetSpeed(1);
        playing = false;
        uiScreen.SetActive(false);
        SoundSetting.SetBGM(bgm);
        PlayerInputControl.manager.SetEnable(GetType().ToString(),true);
    }
}