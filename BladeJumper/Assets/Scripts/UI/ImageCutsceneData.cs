using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Playables;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class ImageCutsceneData : MonoBehaviour
{
    [Serializable]
    internal class DataSet
    {
        [SerializeField]
        internal Sprite image;
        [SerializeField]
        internal AudioClip bgm;
        [SerializeField]

        internal SubTitle[] subTitle;
    }

    [Serializable]
    internal class SubTitle
    {
        [SerializeField]
        internal AudioClip sound;
        [SerializeField]
        [TextArea]
        internal string text;
    }

    [SerializeField]
    bool playAwke = false;

    [SerializeField]
    internal DataSet[] data;


    bool visible = false;
    bool used = false;
    BoxCollider2D col;
    private void Start()
    {
        
        used = SaveManager.instance.GetDisposableUsed(this.gameObject.name.GetHashCode());
        Debug.Log("Name : " + gameObject.name +", InstanceID : " + this.gameObject.name.GetHashCode());
        col = GetComponent<BoxCollider2D>();
        if ((used == false)&&playAwke)
        {
            ImageCutscene.instance.StartCutscene(this);
            used = true;
            SaveManager.instance.SetDisposableUsed(this.gameObject.name.GetHashCode(), true);
        }
    }

    private void Update()
    {
        if (visible&&(used == false))
        {
            if (col.bounds.Contains(BladeJumper.bladeJumper.GetComponent<BoxCollider2D>().bounds.center))
            {
                ImageCutscene.instance.StartCutscene(this);
                used = true;
                SaveManager.instance.SetDisposableUsed(this.gameObject.name.GetHashCode(), true);
            }
            
        }
    }

    private void OnBecameVisible()
    {
        visible = true;
        
    }
    private void OnBecameInvisible()
    {
        visible = false;
    }

}
