using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.UI
{
    public class SoundSettingUI : MonoBehaviour
    {
        
        [System.Serializable]
        struct SliderText
        {
            public Slider slider;
            public Text textUI;
            public void UpdateText()
            {
                textUI.text = Mathf.Ceil(slider.value*100).ToString();
            }
        }


        [SerializeField]
        SliderText bgmInfo;
        [SerializeField]
        SliderText fxInfo;
        [SerializeField]
        SliderText uiFXInfo;

        bool start = false;

        public void ChangeValue()
        {
            if (start == false) return;
            UpdateText();
            SoundSetting.bgmVolume = bgmInfo.slider.value;
            SoundSetting.fxVolume = fxInfo.slider.value;
            SoundSetting.uiVolume = uiFXInfo.slider.value;
            SaveData();
        }
 
        void UpdateText()
        {
            bgmInfo.UpdateText();
            fxInfo.UpdateText();
            uiFXInfo.UpdateText();
        }
        void SaveData()
        {
            SoundSetting.SaveData();
        }
        // Start is called before the first frame update
        void Start()
        {
            bgmInfo.slider.value = SoundSetting.bgmVolume;
            fxInfo.slider.value = SoundSetting.fxVolume;
            uiFXInfo.slider.value = SoundSetting.uiVolume;
            UpdateText();
            start = true;
        }

        // Update is called once per frame
        void Update()
        {

        }

        IEnumerator SliderAction(GameObject ui)
        {
            UIManager.instance.esc_use = false;
            UIControl.control.controlActive = false;

            Slider slider = ui.GetComponentInChildren<Slider>();
            Image[] sliderColor = ui.GetComponentsInChildren<Image>();
            Image bg = ui.GetComponent<Image>();
            Text textUI = ui.GetComponentInChildren<Text>();
            Color textColor = textUI.color;

            textUI.color = Color.black;
            
            for(int i = 0; i< sliderColor.Length; i++)
            {
                sliderColor[i].color = Color.black;
            }
            bg.color = Color.white;

            while (true)
            {
                yield return null;
                if (UserInput.WasPressed(UserInput.UserAction.Left))
                {
                    if(slider.value > 0)
                    {
                        slider.value -= .05f;
                    }
                    
                }
                else if (UserInput.WasPressed(UserInput.UserAction.Right))
                {
                    if (slider.value < 1)
                    {
                        slider.value += .05f;
                    }
                }
                if (UserInput.WasPressed(UserInput.UserAction.Cancle))
                {
                    break;
                }
            }

            
            for (int i = 0; i < sliderColor.Length; i++)
            {
                sliderColor[i].color = Color.white;
            }
            textUI.color = textColor;
            bg.color = Color.black;
            UIManager.instance.esc_use = true;
            UIControl.control.controlActive = true;
        }

        public void SelectUI(GameObject ui)
        {
            if (InputSystem.Mouse.current.leftButton.wasReleasedThisFrame == false)
            {
                StartCoroutine(SliderAction(ui));
            }
        }
    }
}