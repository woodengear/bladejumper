using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.UI;
public class KeySettingUIControl : MonoBehaviour
{

    [SerializeField]
    Text subTitleText;

    [SerializeField]
    string subTitleDefault = "변경할 키를 선택하세요";
    [SerializeField]
    string subTitleKeySetText = "변경할 키를 입력하세요";
    int currentKey = 0;
    int changeKey = 0;



    private void Start()
    {
        KeyManager.instance.keyUpdateHandler += KeyCusorSet;
    }

    void KeyCusorSet()
    {
        UIControl.control.currentBtnIndex = changeKey;
        UIControl.control.CusorRefresh();
    }


    private void Update()
    {
        if (currentKey == 0)
        {
            
            currentKey = (int)UserInput.WasPressedCurrentKey();
            if (currentKey == 0 && UserInput.WasPressedCurrentGamepadButton() != null)
            {
                currentKey = (int)Key.Escape;
            }

        }
    }


    private void OnBecameInvisible()
    {
        UIManager.instance.esc_use = true;
        StopAllCoroutines();
        subTitleText.text = subTitleDefault;
    }

    public void CloseUI(GameObject ui)
    {
        UIManager.instance.esc_use = true;
        StopAllCoroutines();
        subTitleText.text = subTitleDefault;
        UIManager.instance.ReturnUI(ui);
    }

    public void ChangeKey(int changeKey)
    {
        this.changeKey = changeKey;
        UIManager.instance.esc_use = false;
        StartCoroutine(KeyChangeRoutine(changeKey));
    }

    public void ResetKey()
    {
        KeyManager.instance.SetDefaultKey();
    }

    IEnumerator KeyChangeRoutine(int changeKey)
    {
        
        subTitleText.text = subTitleKeySetText;
        yield return null;
        currentKey = 0;
        yield return new WaitUntil(()=> currentKey != 0 );
  
        if ((Key)currentKey == Key.Escape)
        {
            StopAllCoroutines();
            subTitleText.text = subTitleDefault;
            currentKey = 0;
        }
        else
        {
            Debug.Log(currentKey);
            KeyManager.instance.SetKey((KeyAction)changeKey, (Key)currentKey);
            subTitleText.text = subTitleDefault;
        }
        UIManager.instance.esc_use = true;
    }


}
