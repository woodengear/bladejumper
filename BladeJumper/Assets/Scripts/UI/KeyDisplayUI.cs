using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class KeyDisplayUI : MonoBehaviour
{
    [SerializeField]
    int key;
    private void Start()
    {
        KeyUpdate();
        KeyManager.instance.keyUpdateHandler += KeyUpdate;
    }

    void KeyUpdate()
    {
        GetComponent<Text>().text = KeySetting.keys[(KeyAction)key].ToString();
    }
}
