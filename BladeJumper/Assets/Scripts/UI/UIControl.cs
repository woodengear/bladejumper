using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{
    internal static UIControl control;
    bool controlOn = false;

    Button[] currentButtons;
    [SerializeField]
    internal int currentBtnIndex;

    [SerializeField]
    AudioClip uiFX;

    Stack<int> indexStack = new Stack<int>();

    bool padMoved = false;

    internal bool controlActive = true;



    // Start is called before the first frame update
    private void Awake()
    {
        control = this;
    }

    // Update is called once per frame

    internal void KeyControl()
    {
        if (controlActive == false) return;

        float dir = UserInput.GetGamepadStick().y;
        bool padEnter = UserInput.WasPressed(GamepadButton.A);
        //padMoved == true 일때 작동안함
        //Debug.Log("pad Move dir" + dir);
        if (padMoved)
        {
            if(Mathf.Abs(dir) < .1f)
            {
                padMoved = false;
            }
            dir = 0;
        }
        
        if(Mathf.Abs(dir)>.5f)
        {
            padMoved = true;
        }


        if (UserInput.WasPressed(Key.UpArrow)||dir > 0.5f)
        {
            currentBtnIndex -= 1;
            if (currentBtnIndex < 0)
            {
                currentBtnIndex = currentButtons.Length - 1;
            }
            CusorRefresh();
        }

        if (UserInput.WasPressed(Key.DownArrow) || dir < -0.5f)
        {
            currentBtnIndex += 1;
            if (currentBtnIndex >= currentButtons.Length)
            {
                currentBtnIndex = 0;
            }
            CusorRefresh();
        }

        if(UserInput.WasPressed(UserInput.UserAction.Agree))
        {
            currentButtons[currentBtnIndex].onClick.Invoke();
        }
        
    }



    internal void ButtonLoad(bool push = false)
    {
        
        currentButtons = UIManager.instance.openUI.Peek().GetComponentsInChildren<Button>();

        if (push)
        {
            indexStack.Push(currentBtnIndex);
            currentBtnIndex = 0;
        }
        else
        {
            
            currentBtnIndex = indexStack.Pop();
        }

        CusorRefresh();

    }
    internal void CusorRefresh()
    {
        SoundManager.instance.PlaySound(uiFX, SoundSetting.uiVolume);
        ButtonUnCusor();
        ButtonCusor(currentButtons[currentBtnIndex]);
    }
    void ButtonCusor(Button btn)
    {
       Text txt = btn.GetComponentInChildren<Text>();
       txt.text = "▶" + txt.text;
    }
    void ButtonUnCusor(Button btn)
    {
        Text txt = btn.GetComponentInChildren<Text>();

        if(txt.text[0] == '▶')
        {
            txt.text = txt.text.Substring(1);
        }
    }
    void ButtonUnCusor()
    {
        for (int i = 0; i < currentButtons.Length; i++)
        {
            ButtonUnCusor(currentButtons[i]);
        }
    }

    public void MouseClick(Button ui)
    {
        if (controlActive == false) return;
        bool currentCheck = false;
        int index = -1;
        for(int i = 0; i < currentButtons.Length; i++)
        {
            if(currentButtons[i] == ui)
            {
                currentCheck = true;
                index = i;
                break;
            }
        }

        

        if(currentCheck && currentBtnIndex != index)
        {
            currentBtnIndex = index;
            CusorRefresh();
        }
        
    }

}
