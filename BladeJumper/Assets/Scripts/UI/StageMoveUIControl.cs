using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Rendering;
public class StageMoveUIControl : MonoBehaviour
{
    [Serializable]
    struct StageInfo
    {
        [SerializeField]
        internal string name;
        [SerializeField]
        internal Vector2Int[] grids;
        [SerializeField]
        internal AudioClip stageBGM;
        [SerializeField]
        internal bool reverse;
        [SerializeField]
        internal VolumeProfile volumeProfile;

    }

    [SerializeField]
    GameObject stageUI;

    [SerializeField]
    StageInfo[] stageInfos;
    [SerializeField]
    Volume globalVolume;
    [SerializeField]
    VolumeProfile defaultProfile;

    Dictionary<Vector2Int, StageInfo> stageData = new Dictionary<Vector2Int, StageInfo>();

    StageInfo currentStage;

    // Start is called before the first frame update
    void Start()
    {
        CameraControl.controler.GridChange(UpdateStage);

        for(int i = 0; i < stageInfos.Length; i++)
        {
            for(int j = 0; j < stageInfos[i].grids.Length; j++)
            {
#if UNITY_EDITOR
                if (stageData.ContainsKey(stageInfos[i].grids[j]))
                {
                    Debug.LogError("Stage Grid 중복" + "   Grid : " + stageInfos[i].grids[j]);
                }
#endif
                if (stageData.ContainsKey(stageInfos[i].grids[j]) == false)
                {
                    stageData.Add(stageInfos[i].grids[j], stageInfos[i]);
                }
                
            }
        }
    }

    void UpdateStage(Vector2Int grid)
    {
        if (!stageData.ContainsKey(grid))
        {
            return;
        }
        
        if(stageData[grid].name != currentStage.name)
        {
            currentStage = stageData[grid];
            StopAllCoroutines();
            StartCoroutine(StageChage());
        }
        
    }

    IEnumerator StageChage()
    {
        Camera.main.transform.rotation = Quaternion.Euler(0,0,currentStage.reverse?180:0);
        
        if(currentStage.volumeProfile != null)
        {
            globalVolume.profile = currentStage.volumeProfile;
        }
        else
        {
            globalVolume.profile = defaultProfile;
        }

        yield return new WaitUntil(()=>Dialog.DialogManager.instance.IsDialog() == false && ImageCutscene.playing == false);
        stageUI.SetActive(true);
        SoundSetting.SetBGM(currentStage.stageBGM);
        stageUI.GetComponentInChildren<Text>().text = currentStage.name;
        yield return new WaitForSeconds(2);
        stageUI.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
