using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class KKRCandyUI : MonoBehaviour
{
    [SerializeField]
    Text candyCount;
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        candyCount.text = KKRCandy.candyCount.ToString();
    }
}
