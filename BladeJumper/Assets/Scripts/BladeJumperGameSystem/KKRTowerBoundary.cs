using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KKRTowerBoundary : MonoBehaviour
{
    /* KKRTowerBoundary
     * 스테이지의 경계면
     * 기본적으로 막혀있다.
     * KKRCandy 갯수에 따라 통과 가능
     * 
     */

    //KKRCandy.candyCount 와 비교했을 때, 작을경우 해당 오브젝트 삭제 
    [SerializeField]
    int necessaryCandy = 0;
    [SerializeField]
    int eatCount = 0;
    bool visible = false;
    // Start is called before the first frame update
    void Start()
    {
        eatCount = KKRCandy.candyCount;
        if (necessaryCandy <= KKRCandy.candyCount)
        {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (visible)
        {
            CandyUpdate(KKRCandy.candyCount);
        }
    }

    private void OnBecameVisible()
    {
        visible = true;
    }
    private void OnBecameInvisible()
    {
        visible = false;
        if(necessaryCandy <= KKRCandy.candyCount)
        {
            Destroy(this.gameObject);
            gameObject.SetActive(false);
        }
    }

    internal void CandyUpdate(int count)
    {
        if(necessaryCandy <= count)
        {
            ParticleSystem ps = GetComponent<ParticleSystem>();

            ParticleSystem.MainModule main = ps.main;
            main.loop = false;
            Destroy(this.GetComponent<BoxCollider2D>());
        }
    }

}
