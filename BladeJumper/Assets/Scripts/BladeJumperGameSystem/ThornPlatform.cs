using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;


public class ThornPlatform : HitArea
{
    [SerializeField]
    Vector2 power;
    [SerializeField]
    EffectGenerateControl effects;
    [SerializeField]
    string hitEffectName = "Hit";
    protected override void DamageAction(List<Collider2D> hitCol)
    {
        for(int i = 0; i < hitCol.Count; i++)
        {
            Vector2 dir = hitCol[i].bounds.center - col.bounds.center;
            IDamagedObject damagedObject = hitCol[i].GetComponent<IDamagedObject>();
            if (damagedObject != null)
            {

                damagedObject.Damaged(0, dir, new Vector2(Mathf.Sign(dir.x) * power.x, power.y));
                if(effects != null&&effects.Contains(hitEffectName))
                {
                    GameObject ngo = Instantiate<GameObject>(effects.GetEffect(hitEffectName));
                    ngo.transform.position = hitCol[i].bounds.ClosestPoint(transform.position);
                }

            }
        }
        

    }
}
