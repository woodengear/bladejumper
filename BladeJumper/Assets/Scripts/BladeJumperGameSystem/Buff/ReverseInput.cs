using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Buff
{

    [CreateAssetMenu(fileName = "ReverseInput", menuName = "Buff/Player/ReverseInput")]
    public class ReverseInput : BuffData
    {
        [System.Serializable]
        enum Dir {horizontal,vertical,all}

        [SerializeField]
        Dir dir;

        

        protected override void BuffStart(GameObject actor)
        {
            switch (dir)
            {
                case Dir.horizontal:
                    PlayerInputControl.reverseDir.x = true;
                    break;
                case Dir.vertical:
                    PlayerInputControl.reverseDir.y = true;
                    break;
                case Dir.all:
                    PlayerInputControl.reverseDir.x = true;
                    PlayerInputControl.reverseDir.y = true;
                    break;
            }
        }

        protected override void BuffEndAction(GameObject actor)
        {
            switch (dir)
            {
                case Dir.horizontal:
                    PlayerInputControl.reverseDir.x = false;
                    break;
                case Dir.vertical:
                    PlayerInputControl.reverseDir.y = false;
                    break;
                case Dir.all:
                    PlayerInputControl.reverseDir.x = false;
                    PlayerInputControl.reverseDir.y = false;
                    break;
            }
        }
    }

}

