using System.Collections;
using UnityEngine;


namespace Buff
{


    public class BuffData : ScriptableObject
    {
        /*
         * Buff Routine
         *      버프 대상에 버프 추가
         *      버프 대상의 behavior에서 코루틴 실행
         *      BuffStart() 실행
         *      BuffEffect() 실행 =>  true일 경우 반복 취소 + BuffAfterRoutine 코루틴 대기
         *                            false일 경우 반복  
         *      BuffEndAction() 실행
         *      버프 대상에서 버프 제거
         *      코루틴 종료
         *      
         */
        public Sprite icon;
        public float duration = 1f;

        public bool iconDraw = true;
        public BuffEffect buffEffect;
        public BuffEffectData effectData;

        internal enum Target { Self, Actor, Player }
        [SerializeField]
        Target _target;
        internal void BuffActive(IBuffUsed self, IBuffUsed target)
        {
            if (GetTargetFixed(target) == false)
            {
                switch (_target)
                {
                    case Target.Self:
                        if (self != null)
                        {
                            self.AddBuff(this);
                        }
                        break;
                    case Target.Actor:
                        if (target != null)
                        {
                            target.AddBuff(this);
                        }
                        break;
                    case Target.Player:
                        BladeJumper.bladeJumper.GetComponent<IBuffUsed>().AddBuff(this);
                        break;
                }
            }
        }
        internal void BuffActive(GameObject self, GameObject target)
        {
            BuffActive(self.GetComponent<IBuffUsed>(), target.GetComponent<IBuffUsed>());
        }
        internal virtual bool GetTargetFixed(IBuffUsed target)
        {
            return false;
        }

        internal virtual IEnumerator Action(GameObject actor)
        {
            
            //Debug.Log("BuffStart");
            IBuffUsed buffUsed = actor.GetComponent<IBuffUsed>();
            BuffEffect nBE = null;
            if (buffEffect != null)
            {
                nBE = Instantiate<BuffEffect>(buffEffect);
                nBE.target = buffUsed;
                nBE.buff = this;
            }
            if(effectData != null)
            {
                effectData.EffectAction(this, buffUsed);
            }

            BuffStart(actor);
            
            while (true)
            {
                
                yield return null;
                if (BuffBreakCheck(actor))
                {
                    //yield return BuffAfterRoutine(actor);
                    //Debug.Log("BuffBreak");
                    break;
                }
                if (buffUsed.AddTime(this, -Time.deltaTime) <= 0)
                {
                    break;
                }
            }
            BuffEndAction(actor);
            buffUsed.RemoveBuff(this);

            if (buffEffect != null)
            {
                Destroy(nBE.gameObject);
            }

            if (effectData != null)
            {
                effectData.BuffEnd(this, buffUsed);
            }
            //Debug.LogError("BuffEnd");
        }
        virtual protected void BuffStart(GameObject actor)
        {

        }
        internal virtual bool BuffBreakCheck(GameObject actor = null)
        {
            return false;
        }
        protected virtual IEnumerator BuffAfterRoutine(GameObject actor = null)
        {
            yield return null;
        }
        protected virtual void BuffEndAction(GameObject actor)
        {

        }
    }
}