using System.Collections;
using UnityEngine;

namespace Buff
{

    [CreateAssetMenu(fileName = "InstantSlash", menuName = "Buff/Player/InstantSlash")]
    public class InstantSlashBuff : BuffData
    {
        bool slash = false;

        internal override bool GetTargetFixed(IBuffUsed target)
        {
            BladeJumper.bladeJumper.GetComponent<IBuffUsed>().AddBuff(this);

            return true;
        }
        internal override bool BuffBreakCheck(GameObject actor = null)
        {
            slash = false;
            if (PlayerInputControl.WasPressedJumpKey())
            {
                BladeJumper.bladeJumper.InstantSlashReady();
                return true;
            }
            if (BladeJumper.bladeJumper.GetComponent<PlatformerControl.CharacterCollider>().colBox.below || BladeJumper.bladeJumper.playerHit)
            {
                return true;
            }

            return false;
        }
        protected override IEnumerator BuffAfterRoutine(GameObject actor = null)
        {
            if (slash)
            {
                yield return null;
            }

            slash = false;
        }
    }
}