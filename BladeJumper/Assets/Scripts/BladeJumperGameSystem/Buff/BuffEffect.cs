using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Buff
{


    public class BuffEffect : MonoBehaviour
    {

        internal IBuffUsed target;
        internal BuffData buff;
        BoxCollider2D col;
        

        // Start is called before the first frame update
        protected virtual void Start()
        {
            col = target.GetTarget().GetComponent<BoxCollider2D>();
        }
        // Update is called once per frame
        protected virtual void Update()
        {

        }
        protected virtual void FixedUpdate()
        {
            transform.position = col.bounds.center;
        }
    }
}