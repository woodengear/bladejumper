using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Buff
{
    public class BuffIconSet : MonoBehaviour
    {
        /*
         * buffICon UI
         * 
         * 버프 대상에 아이콘 UI 추가 및 업데이트
         */
        [SerializeField]
        Vector2 pivot;

        BoxCollider2D col;

        internal GameObject target;
        internal IBuffUsed buffUsed;

        Dictionary<string, float> buffs ;

        Dictionary<string,GameObject> icons = new Dictionary<string, GameObject>();
        
        private void Start()
        {
            buffUsed = target.GetComponent<IBuffUsed>();
            buffs = buffUsed.GetBuffs();
            col = target.GetComponent<BoxCollider2D>();
        }
        private void FixedUpdate()
        {
            transform.position = new Vector2(col.bounds.center.x, col.bounds.max.y) + pivot;
        }

        /*
         * 버프 변경 확인
         *      매개변수 buff에서
         *      buff 가 buffs에 없다면 해당 버프가 제거된 것 -> 아이콘 오브젝트 제거 및 icons에서 remove
         *      buff 가 buffs에 있고 icons에 없다면 버프가 추가된 것 -> 아이콘 오브젝트 추가 및 icons에 add
         */
        internal void BuffUpdate(BuffData buff)
        {
            if(buffs == null)
            {
                buffUsed = target.GetComponent<IBuffUsed>();
                buffs = buffUsed.GetBuffs();
                col = target.GetComponent<BoxCollider2D>();
            }
            if (buff.icon != null)
            {
                if (buffs.ContainsKey(buff.name))
                {
                    if(buff.iconDraw == true&&icons.ContainsKey(buff.name) == false)
                    {
                        GameObject _icon = new GameObject();
                        _icon.transform.parent = this.transform;
                        _icon.AddComponent<Image>().sprite = buff.icon;
                        icons.Add(buff.name, _icon);
                    }
                }
                else
                {
                    if(icons.ContainsKey (buff.name))
                    {
                        Destroy(icons[buff.name].gameObject);
                        icons.Remove(buff.name);
                    }
                }
            }

           
        }
    }
}