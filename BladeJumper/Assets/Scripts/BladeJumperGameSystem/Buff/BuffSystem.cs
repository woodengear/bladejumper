using System.Collections.Generic;
using UnityEngine;
using Buff;
using UnityEngine.UI;

namespace Buff
{
    public class BuffSystem : MonoBehaviour
    {
        /*
         * 버프 UI 시스템
         * 
         */
        static internal BuffSystem instance;

        public GameObject uiGroup;
        public BuffIconSet iconSet;
        
        Dictionary<IBuffUsed, BuffIconSet> iconSets = new Dictionary<IBuffUsed, BuffIconSet>();
        
        private void Awake()
        {
            instance = this;
        }

        internal void CreateUI(IBuffUsed buffUsed)
        {
            iconSet = Instantiate<BuffIconSet>(iconSet,uiGroup.transform);
            iconSet.target = buffUsed.GetTarget();
            iconSets.Add(buffUsed, iconSet);
        }

        internal void UpdateBuff(IBuffUsed buffUsed,BuffData buff)
        {
            if(iconSets.ContainsKey(buffUsed) == false)
            {
                CreateUI(buffUsed);
            }
            iconSets[buffUsed].BuffUpdate(buff);
        }

        

    }

    public interface IBuffUsed
    {
        public GameObject GetTarget();
        public void AddBuff(BuffData buff);
        public void RemoveBuff(BuffData buff);
        public float GetTime(BuffData buff);
        public float AddTime(BuffData buff, float time);
        public Dictionary<string, float> GetBuffs();
    }
}
public partial class BladeJumper : MonoBehaviour,IBuffUsed
{
    
    Dictionary<string, float> buffDic = new Dictionary<string, float>();

    

    public void AddBuff(BuffData buff)
    {
        if(buffDic.ContainsKey(buff.name) == false)
        {
            buffDic.Add(buff.name,buff.duration);
            StartCoroutine(buff.Action(this.gameObject));
            
        }
        else
        {
            buffDic[buff.name] = buff.duration;
        }

        BuffSystem.instance.UpdateBuff(this, buff);
    }

    public void RemoveBuff(BuffData buff)
    {
        buffDic.Remove(buff.name);
        BuffSystem.instance.UpdateBuff(this,buff);
    }

    public float GetTime(BuffData buff)
    {
        return buffDic[buff.name];
    }

    public float AddTime(BuffData buff,float time)
    {
        buffDic[buff.name] += time;
        return buffDic[buff.name];
    }
    public GameObject GetTarget()
    {
        return this.gameObject;
    }
    public Dictionary<string, float> GetBuffs()
    {
        return buffDic;
    }
}