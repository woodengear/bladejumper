using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;

namespace Buff
{


    [CreateAssetMenu(fileName = "Slow", menuName = "Buff/DeBuff/Slow")]
    public class SlowBuff : BuffData
    {
        [SerializeField]
        float timeRate = .5f;
        protected override void BuffStart(GameObject actor)
        {
            actor.GetComponent<CharacterCollider>().timeScale *= timeRate;
        }

        protected override void BuffEndAction(GameObject actor)
        {
            actor.GetComponent<CharacterCollider>().timeScale *= 1 / timeRate;
        }
        
    }
}