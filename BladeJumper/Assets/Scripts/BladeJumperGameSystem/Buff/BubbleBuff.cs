using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Buff
{
    [CreateAssetMenu(fileName = "Bubble", menuName = "Buff/DeBuff/Bubble")]
    public class BubbleBuff : SlowBuff
    {


        internal override bool BuffBreakCheck(GameObject actor = null)
        {

            if (actor.GetComponent<IBuffUsed>().GetBuffs()[this.name] < duration - .1f && actor == BladeJumper.bladeJumper.gameObject && BladeJumper.bladeJumper.GetHitThisFrame())
            {
                return true;
            }

            return false;
        }
    }
}