using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Buff
{

    [CreateAssetMenu(fileName = "Outline", menuName = "Buff/Effect/Outline")]
    public class OutLineEffect : BuffEffectData
    {
        [ColorUsage(true, true)]
        public Color outlineColor;

        Dictionary<IBuffUsed, OutLineControl> targetOutline = new Dictionary<IBuffUsed, OutLineControl>();

        internal override void EffectAction(BuffData buff, IBuffUsed buffUsed)
        {
            if (targetOutline.ContainsKey(buffUsed) == false)
            {
                if(buffUsed.GetTarget().GetComponent<OutLineControl>() != null)
                {
                    targetOutline.Add(buffUsed, buffUsed.GetTarget().GetComponent<OutLineControl>());
                    targetOutline[buffUsed].AddColor(outlineColor);
                }
            }
        }
        internal override void BuffEnd(BuffData buff, IBuffUsed buffUsed)
        {
            if (targetOutline.ContainsKey(buffUsed))
            {
                targetOutline[buffUsed].RemoveColor(outlineColor);
                targetOutline.Remove(buffUsed);
            }
        }
    }
}