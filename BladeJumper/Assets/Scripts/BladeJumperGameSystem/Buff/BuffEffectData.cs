using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Buff
{
    public class BuffEffectData : ScriptableObject
    {

        
        internal virtual void EffectAction(BuffData buff, IBuffUsed buffUsed)
        {
            
        }

        internal virtual void BuffEnd(BuffData buff, IBuffUsed buffUsed)
        {
            
        }
    }

}
