using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(BoxCollider2D))]
public class KKRCandy : MonoBehaviour
{
    /* class KKRCandy
     * bladejumber 게임 진행도 척도중 하나인 ㅋㅋㄹ 캔디에 대한 클래스
     * 
     */

    /* 변수
     * static int candyCount
     *      현재까지 먹은 캔디의 갯수
     *      시작시 데이터에서 불러온다.
     * bool eat
     *      해당 캔디를 먹었는지 확인.
     *      true 라면 드랍되지 않는다.
     * GameObject target
     *      drop_target을 inspector에서 받아오기 위한 변수
     * IDamagedObject drop_target
     *      해당 타겟의 hp가 0이하가 됬을 때 사탕이 보이게 된다.
     * int candyNumber
     *      해당 캔디의 순서
     *      candyCount보다 작으면 해당 캔디는 드랍되지 않는다.
     */
    internal static int candyCount;
    bool visible = true;
    [SerializeField]
    bool eat = false;
    [SerializeField]
    GameObject target;
    IDamagedObject drop_target;

    [SerializeField]
    int candyNumber;

    SpriteRenderer sr;
    Color color;

    
    

    BoxCollider2D boxCol;
    
    /* 진행 과정
     *      게임 시작시 캔디를 투명하게함
     *      candyCount를 데이터에서 로드
     *      candynumber가 candyCount 이하일때 해당 오브젝트 삭제
     * 
     *      해당 캔디를 먹은 경우가 아닐때,
     *      target의 hp가 0 이하가 될경우
     *      캔디를 보이게 변경
     *      이때, 캐릭터가 접근할 경우
     *      캔디의 투명도를 올린후
     *      캔디 먹는 애니메이션 루틴 실행
     *          루틴에서 candycount +1
     *      루틴 종료후 해당 오브젝트 삭제
     * 
     */

    // Start is called before the first frame update
    private void Awake()
    {
        drop_target = target.GetComponent<IDamagedObject>();
        sr = GetComponent<SpriteRenderer>();
        color = sr.color;
        color.a = 0;
        sr.color = color;

        boxCol = GetComponent<BoxCollider2D>();
    }
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if(eat == false && visible == false &&target != null &&drop_target.GetHP() > 0)
        {

            color.a = 0;
            sr.color = color;
            transform.parent = target.transform;
        }
        if (eat == false&&visible)
        {
            
            if (candyNumber > candyCount && (target == null||(drop_target.GetHP() <= 0 && drop_target.GetHP() != -10)))
            {
                if(color.a == 0)
                {
                    transform.parent = null;
                    
                    color.a = 1;
                    sr.color = color;
                }
                else
                {
                    if (PlayerAproach())
                    {
                        eat = true;
                        color.a = 0;
                        sr.color = color;
                        StartCoroutine(CandyEatAnimation());
                    }
                }
                
            }
        }   
    }

    bool PlayerAproach()
    {
       
        Bounds playerBounds =  BladeJumper.bladeJumper.gameObject.GetComponent<BoxCollider2D>().bounds;
        if (boxCol.bounds.Contains(playerBounds.center)&&BladeJumper.bladeJumper.GetComponent<AnimationControl>().currentAnim == AnimationControl.AnimState.Idle)
        {
            Debug.Log(candyCount);
            return true;
        }
        return false;
    }

    IEnumerator CandyEatAnimation()
    {
        PlayerInputControl.manager.SetEnable("Animation",false);
        Animator anim = BladeJumper.bladeJumper.GetComponent<Animator>();
        anim.Play("Player_Candy_Idle");
        yield return new WaitForSeconds(1);
        anim.Play("Player_Candy_Eat");
        
        float time = anim.GetCurrentAnimatorStateInfo(0).length;
        
        yield return null;
        candyCount = candyNumber;
        Debug.Log("Eat Time : " + time);
        yield return new WaitForSeconds(1.1f);
        anim.Play("PlayerIdle");
        yield return new WaitForSeconds(1f);
        
        PlayerInputControl.manager.SetEnable("Animation", true);
        

        Destroy(this.gameObject);
    }

    private void OnBecameVisible()
    {
        visible = true;
    }
    private void OnBecameInvisible()
    {

        visible = false;
    }

}
