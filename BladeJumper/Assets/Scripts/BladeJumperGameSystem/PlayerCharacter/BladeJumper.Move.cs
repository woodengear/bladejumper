using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;
public partial class BladeJumper : MonoBehaviour
{
   [Header("Move Inspector")]
    [SerializeField]
    float speed;
    internal Vector2 velocity = new Vector2();


    internal bool playerHit = false;
    int hitFrame = 0;
    internal bool fallDown = false;

    [SerializeField]
    float fallTime = 1;
    float fallDownTime = 0;

    [SerializeField]
    float fallHeight = 10;
    [SerializeField]
    float maxY = -10000;

    internal bool GetHitThisFrame()
    {
        return hitFrame == Time.frameCount;
    }

    void Move_Init()
    {
        collider.collisonEvent += new CollisionEventHandler(WallBound);
    }

    void MoveUpdate()
    {
        

        if (playerHit)
        {
            if (collider.colBox.below)
            {
                playerHit = false;

                FallDown();
            }
        }

        FallDownUpdate();
        JumpDown();

        if (collider.colBox.below && PlayerInputControl.jump == 0 && slashKeyDown == 0)
        {
            velocity.x = PlayerInputControl.direction.x * speed;
            if (collider.colBox.left && velocity.x < 0) velocity.x = 0;
            if (collider.colBox.right && velocity.x > 0) velocity.x = 0;
        }

        if (collider.colBox.below && !slashStart && (collider.GetVelocity().x * velocity.x < 0 || Mathf.Abs(collider.GetVelocity().x) < Mathf.Abs(velocity.x)))
        {
            collider.AddPower(velocity);
        }
    }


    void FallDownUpdate()
    {
        if (maxY < collider.colBox.center.y)
        {
            maxY = collider.colBox.center.y;
        }
        if (fallDown)
        {
            
            //Slash 변수 초기화
            slashKeyDown = 0;
            slashingTime = 0;
            fallDownTime += Time.deltaTime;

            if (collider.colBox.below != true)
            {
                fallDownTime = 0;
                playerHit = true;
                return;
            }
            if (fallDownTime > fallTime)
            {
                GetComponent<PlayerInputControl>().SetEnable("BladeJumper", true);
                fallDown = false;
                fallDownTime = 0;
            }
            else
            {
                return;
            }
        }
    }

    void JumpDown()
    {
        if (collider.colBox.below)
        {
            if (maxY - collider.colBox.center.y > fallHeight)
            {
                SetPadVibrate(1, 1, .5f);
                maxY = collider.colBox.center.y;
                FallDown();
                return;
            }
            else if (maxY - collider.colBox.center.y > 1f)
            {
                soundFXCon.PlaySound("JumpDown");
            }
            maxY = collider.colBox.center.y;
        }
    }

    internal void FallDown()
    {
        maxY = collider.colBox.center.y;
        soundFXCon.PlaySound("FallDown");
        Instantiate<GameObject>(effectGenerate.GetEffect("Downfall")).transform.position = transform.position;
        GetComponent<PlayerInputControl>().SetEnable("BladeJumper", false);
        fallDown = true;
    }
    void WallBound(RaycastHit2D hit)
    {
        float angle = Vector2.Angle(hit.normal, Vector2.up);
        if (angle == 90)
        {
            if (collider.standing == false)
            {
                soundFXCon.PlaySound("WallBound");
                if (slashStart)
                {
                    GameObject ngo = Instantiate<GameObject>(effectGenerate.GetEffect("WallJump"));
                    ngo.transform.position = transform.position;
                    ngo.GetComponent<SpriteRenderer>().flipX = GetComponent<SpriteRenderer>().flipX;
                }
                else
                {
                    GameObject ngo = Instantiate<GameObject>(effectGenerate.GetEffect("PlayerHitWall"));
                    ngo.transform.position = transform.position;
                    ngo.GetComponent<SpriteRenderer>().flipX = GetComponent<SpriteRenderer>().flipX;
                }
            }


            slashVelocity.x = Mathf.Abs(slashVelocity.x) * Mathf.Sign(hit.normal.x);
            velocity.x = Mathf.Abs(velocity.x) * Mathf.Sign(hit.normal.x);
        }

        if (hit.normal.y < 0)
        {
            if (hit.normal.x != 0)
            {
                slashVelocity.x = Mathf.Abs(slashVelocity.x) * Mathf.Sign(hit.normal.x);
                velocity.x = Mathf.Abs(velocity.x) * Mathf.Sign(hit.normal.x);
            }

            velocity.y = Mathf.Abs(velocity.y) * Mathf.Sign(hit.normal.y);
            slashVelocity.y = Mathf.Abs(slashVelocity.y) * Mathf.Sign(hit.normal.y);
        }
    }

    internal void ForceMove(Vector2 moveAmount, bool reset = false)
    {
        if (slashStart)
        {
            collider.gravityActive = true;
            slashStart = false;
            StopCoroutine(slashRoutine);
        }

        if (reset)
        {
            collider.VelocityReset();
        }
        collider.AddPower(moveAmount);
    }
}
