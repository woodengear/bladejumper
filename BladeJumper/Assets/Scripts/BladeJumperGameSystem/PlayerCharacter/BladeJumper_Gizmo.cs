using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class BladeJumper : MonoBehaviour
{

#if UNITY_EDITOR
    [System.Serializable]
    class DebugInfo
    {
        [SerializeField]
        internal bool debug;
        [SerializeField]
        internal bool right = true;
        [SerializeField]
        [Range(0, 40)]
        internal int step = 0;
        [SerializeField]
        internal float gravity = 30;

        [SerializeField]
        [Range(0.001f, 1)]
        internal float deltaTime = .1f;
        [SerializeField]
        internal float gravityCheckTime = 4;
        [SerializeField]
        internal LayerMask bottomLayer;

        internal Bounds bounds = new Bounds();
        internal bool jump = false;
    }
    [Header("Debug Inspector")]
    [SerializeField]
    DebugInfo _debug;

    private RaycastHit2D PreViewHit(Vector2 center, Vector2 size, ref Vector2 dir, ref float distance, Collider2D preHit = null)
    {
        ContactFilter2D contactFilter2D = new ContactFilter2D();
        contactFilter2D.layerMask = _debug.bottomLayer;

        RaycastHit2D[] hit = Physics2D.BoxCastAll(center, size, 0, dir, distance, _debug.bottomLayer);

        if (hit.Length > 0)
        {
            for (int i = 0; i < hit.Length; i++)
            {

                if (hit[i].collider == preHit)
                {
                    continue;
                }
                if (hit[i].normal.y < 0)
                    dir.y *= -1;
                else if (hit[i].normal.x != 0)
                    dir.x *= -1;
                distance -= hit[i].distance;
                return hit[i];
            }
        }
        //Gizmos.DrawLine(center, center + dir.normalized * distance);
        return new RaycastHit2D();
    }
    private void preViewGravity(Vector2 start, Vector2 size, Vector2 dir)
    {

        _debug.deltaTime = Time.fixedDeltaTime;

        if (_debug.deltaTime <= 0) return;

        Vector2 velocity = (dir * stepRate[_debug.step].magnitude * slashWideMax * (1.0f / slashTime)) * new Vector2(.3f, .2f);
        dir.y = Mathf.Abs(dir.y);
        if (_debug.jump)
        {
            //Debug.Log("Debug gvel : " +velocity);
        }
        float debugTime = 0;
        while (debugTime < _debug.gravityCheckTime)
        {
            debugTime += _debug.deltaTime;
            velocity.y -= _debug.gravity * _debug.deltaTime;
            if (velocity.y < -20) velocity.y = -20;
            Gizmos.color = Color.red;

            RaycastHit2D hit = Physics2D.BoxCast(start, size, 0, velocity, velocity.magnitude * _debug.deltaTime, _debug.bottomLayer);
            if (hit)
            {
                Gizmos.DrawCube(hit.centroid, size);
                Gizmos.DrawLine(start, hit.centroid);
                start = hit.centroid;
                if (hit.normal.y == 1)
                {
                    break;
                }
                if (hit.normal.y < 0)
                {
                    velocity.y *= -1;
                }
                else if (hit.normal.x != 0)
                {
                    velocity.x *= -1;
                }

                Gizmos.DrawLine(start, start + velocity * _debug.deltaTime);
                start = start + velocity * _debug.deltaTime;
                debugTime += _debug.deltaTime;

            }
            else
            {
                Gizmos.DrawLine(start, start + velocity * _debug.deltaTime);
                start = start + velocity * _debug.deltaTime;
            }



        }
    }
    private void OnDrawGizmos()
    {
        if (!_debug.debug) return;



        if (stepRate.Length > 0)
        {
            Vector2 dir = stepRate[_debug.step];
            if (_debug.right == false || (GetComponent<SpriteRenderer>() && GetComponent<SpriteRenderer>().flipX))
            {
                dir.x = -stepRate[_debug.step].x;
            }
            if (_debug.jump == false)
            {
                _debug.bounds.center = GetComponent<BoxCollider2D>().bounds.center;
                _debug.bounds.size = GetComponent<BoxCollider2D>().bounds.size;
            }

            Gizmos.color = Color.blue;
            Gizmos.DrawCube(_debug.bounds.center, _debug.bounds.size);
            float slashDistance = dir.magnitude * slashWideMax * (1.0f / slashTime) * 0.16f;
            dir.Normalize();
            Collider2D preHitCol = null;
            Vector2 lastPosition = new Vector2();



            for (int i = 0; i < 4; i++)
            {

                RaycastHit2D hit = PreViewHit(_debug.bounds.center, _debug.bounds.size, ref dir, ref slashDistance, preHitCol);
                if (hit)
                {
                    Gizmos.DrawLine(_debug.bounds.center, hit.centroid);
                    Gizmos.DrawCube(hit.centroid, _debug.bounds.size);
                    _debug.bounds.center = hit.centroid;
                    preHitCol = hit.collider;
                    lastPosition = hit.centroid;
                }
                else
                {
                    
                    Gizmos.DrawLine(this._debug.bounds.center, (Vector2)this._debug.bounds.center + dir.normalized * slashDistance);
                    Vector2 velocity = dir.normalized * stepRate[_debug.step].magnitude * slashWideMax * (1.0f / slashTime);
                    lastPosition = (Vector2)_debug.bounds.center + dir.normalized * slashDistance;
                    break;
                }
            }
            preViewGravity(lastPosition, _debug.bounds.size, dir);


        }
    }

    void GizmoUpdate()
    {
        if (UserInput.WasPressed(UnityEngine.InputSystem.Key.Digit5))
        {
            _debug.debug = !_debug.debug;
        }
        if (_debug.debug)
        {
            UnityEditor.SceneView sceneView = UnityEditor.SceneView.lastActiveSceneView;
            sceneView.pivot = Camera.main.transform.position;


            _debug.jump = !(collider.colBox.below && (collider.GetVelocity().y >= 0));


            if (UserInput.WasPressed(UnityEngine.InputSystem.Key.Digit2) || UserInput.IsPressed(UnityEngine.InputSystem.Key.Digit4))
            {
                _debug.step += 1;
                if (_debug.step > stepRate.Length - 1) _debug.step = stepRate.Length - 1;
            }
            if (UserInput.WasPressed(UnityEngine.InputSystem.Key.Digit1) || UserInput.IsPressed(UnityEngine.InputSystem.Key.Digit3))
            {
                _debug.step -= 1;
                if (_debug.step < 0) _debug.step = 0;
            }
        }
    }
#endif
}
