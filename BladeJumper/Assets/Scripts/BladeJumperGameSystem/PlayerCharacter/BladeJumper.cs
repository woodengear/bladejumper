using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;
using Dialog;

[RequireComponent(typeof(PlayerInputControl))]
[RequireComponent(typeof(CharacterCollider))]
public partial class BladeJumper : MonoBehaviour, IDamagedObject
{
    static internal BladeJumper bladeJumper;

    new CharacterCollider collider;

    [SerializeField]
    EffectGenerateControl effectGenerate;

    SoundFXControl soundFXCon;



    private void Awake()
    {
        attackArea.gameObject.SetActive(false);
        collider = GetComponent<CharacterCollider>();
        bladeJumper = this;
    }

    // Start is called before the first frame update
    void Start()
    {

        soundFXCon = GetComponent<SoundFXControl>();
        Move_Init();
        Slash_Init();
    }
   
    private void Update()
    {
#if UNITY_EDITOR
        GizmoUpdate();
#endif
    }
    
    private void FixedUpdate()
    {
        SlashUpdate();
        MoveUpdate();
    }


    public void AttackAreaOn(bool on)
    {
        attackArea.gameObject.SetActive(on);
    }

    public bool Damaged(int damage, Vector2 dir = default, Vector2 power = default, GameObject actor = default)
    {
        if(slashRoutine != null)
            StopCoroutine(slashRoutine);
        ForceMove(power, true);
        slashKeyDown = 0;
        fallDownTime = 0;
        playerHit = true;
        hitFrame = Time.frameCount;
        SetPadVibrate(1f, 1f,.4f);

        return true;
    }

    void SetPadVibrate(float lowF, float highF, float time)
    {

        StartCoroutine(PadVibrateRoutine(lowF, highF, time));
    }
   
    IEnumerator PadVibrateRoutine(float lowF, float highF, float time)
    {
        UserInput.PadVibrate(lowF, highF);
        yield return new WaitForSecondsRealtime(time);
        UserInput.PadVibrate(0, 0);
    }

    public int GetHP()
    {
        return 0;
    }
    
}
