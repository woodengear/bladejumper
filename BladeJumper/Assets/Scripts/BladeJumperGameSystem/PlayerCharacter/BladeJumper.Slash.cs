using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;

public partial class BladeJumper : MonoBehaviour
{
    [System.Serializable]
    class SlashInfo
    {
        
    }
    [Header("Slash Inspector")]

    [SerializeField]
    LayerMask EnemyLayer;

    [SerializeField]
    internal BoxCollider2D attackArea;

    [SerializeField]
    GameObject trailEffectPrefabs;

    [SerializeField]
    GameObject slashEffect;

    List<GameObject> effectedList = new List<GameObject>();

    [SerializeField]
    GameObject jumpEffect;

    TrailRenderer trailEffect;

    internal bool slashStart = false;
    [SerializeField]
    float slashHeightMax = 3;
    [SerializeField]
    float slashHeightMin = 0.5f;
    [SerializeField]
    float slashWideMax = 3;
    [SerializeField]
    float slashWideMin = 2;

    internal float slashKeyDown = 0;
    [SerializeField]
    Vector2[] stepRate;

    int stepCount = 0;

    [SerializeField]
    float slashKeyDownTime = .15f;

    [SerializeField]
    float KeyDownTimeMin = .5f;
    internal Vector2 slashVelocity;

    [SerializeField]
    float slashTime = .1f;



    [SerializeField]
    GameObject wallEffect;


    CollisionEventHandler slashAngleChange;
    List<IDamagedObject> damagedObjects = new List<IDamagedObject>();

    Vector2 slashStartPos;
    Vector2 lastPos;

    float slashingTime = 0;

    IEnumerator slashRoutine;
    internal bool instantSlash = false;
    [SerializeField]
    float instantSlashTime = .2f;
    [SerializeField]
    float instantSlashSlowRate = .05f;
    [SerializeField]
    [ColorUsage(true, true)]
    Color instantSlashColor;
    struct InstantSlashInfo
    {
        public bool start;
        public float readyTime;
        public float currentTime;

        public void Ready()
        {
            start = true;
            readyTime = 2.5f;
            currentTime = readyTime;

        }
    }
    InstantSlashInfo instantSlashStat = new InstantSlashInfo();
    void Slash_Init()
    {
        
        collider.translateEventHandelr += new TranslateEvent(SlashRunHit);
    }

    void SlashStart()
    {
        if (slashStart)
        {
            if (trailEffect&& slashingTime != 0)
            {
                trailEffect.transform.position = collider.colBox.center;
            }
            slashingTime += Time.fixedDeltaTime;
            if (slashingTime*collider.GetTimeScale() >= slashTime)
            {
                slashingTime = 0;
                Slash();
                //Debug.Break();
            }
        }
    }

    void Slash_JumpKeyDown()
    {
        if (PlayerInputControl.jump > 0 && !slashStart && collider.colBox.below && !collider.colBox.descendingSlope)
        {
            velocity.x = 0;
            slashKeyDown += Time.deltaTime * collider.GetTimeScale();
            stepCount += 1;
            if (slashKeyDown >= slashKeyDownTime)
            {
                slashKeyDown = slashKeyDownTime;
                slashRoutine = SlashRun(slashTime);
                StartCoroutine(slashRoutine);
                //Slash();
            }
        }
        else if (!slashStart)
        {
            if (slashKeyDown > 0)
            {
                slashRoutine = SlashRun(slashTime);
                StartCoroutine(slashRoutine);
                //Slash();
            }
            stepCount = 0;
        }
    }

    void SlashUpdate()
    {
        
        if (playerHit) return;

        SlashStart();
        Slash_JumpKeyDown();
    }

    void SlashAngleFix(RaycastHit2D hit)
    {
        float angle = Vector2.Angle(hit.normal, Vector2.up);
        float rad = angle * Mathf.Deg2Rad;
        Vector2 slopeDir = new Vector2(Mathf.Cos(rad) * Mathf.Sign(hit.normal.x * -1), Mathf.Sin(rad)).normalized;
        float slopeDistance = slashVelocity.x * Mathf.Cos(rad) * Mathf.Sign(hit.normal.x * -1) + slashVelocity.y * Mathf.Sin(rad);

        if (hit.point.y > collider.col.bounds.max.y)
        {
            return;
            slopeDistance = slashVelocity.x * Mathf.Cos(rad) * Mathf.Sign(hit.normal.x * -1) - slashVelocity.y * Mathf.Sin(rad);
        }

        if (hit.normal == Vector2.down)
        {

        }
        else
        {
            slashVelocity = slopeDir * slopeDistance;
        }


        //Debug.Log("ChangeAngle");
    }
    public void SlashEnd()
    {
        if (trailEffect)
        {
            //Destroy(trailEffect.gameObject);
        }
        effectedList.Clear();
        //Slash 변수 초기화
        //slashKeyDown = 0;
        //slashingTime = 0;
        //Debug.Log("SEnd");
    }

    //Į��
    void Slash()
    {
        collider.angleHit -= slashAngleChange;
        Vector2 vel = collider.GetVelocity();
        if (vel != Vector2.zero)
        {
            SlashAfter();
            if (trailEffect)
            {
                trailEffect.transform.position = collider.colBox.center + vel.normalized;
            }
        }
        else
        {
            collider.gravityActive = true;
        }
        

        slashStart = false;
    }

    void SlashRunHit(Vector2 moveAmount)
    {
        //������ ������
        if (!slashStart) return;

        if (slashVelocity == Vector2.zero) return;

        RaycastHit2D[] hit = Physics2D.BoxCastAll(collider.colBox.center, collider.colBox.size, 0, moveAmount, moveAmount.magnitude, EnemyLayer);
        //hit = Physics2D.RaycastAll(collider.colBox.center, moveAmount, moveAmount.magnitude, EnemyLayer);
        hit = Physics2D.RaycastAll(collider.colBox.center, moveAmount, moveAmount.magnitude, ~0 & ~(1 << gameObject.layer));
        if (trailEffect)
        {
            if (hit.Length > 0)
            {
                trailEffect.forceRenderingOff = false;

            }
            



        }

        if (hit.Length > 0)
        {
            for (int i = 0; i < hit.Length; ++i)
            {
                if (hit[i].collider.GetComponent<IDamagedObject>() != null)
                {
                    SlashEffectGenerate(hit[i]);
                    //hit[i].collider.GetComponent<IDamagedObject>().Damaged(1, transform.position);
                }

            }
        }
    }
    void SlashHit()
    {
        List<Collider2D> enemise = new List<Collider2D>();
        ContactFilter2D cf = new ContactFilter2D();
        cf.layerMask = ~0;
        cf.layerMask &= ~(1 << gameObject.layer);
        cf.useLayerMask = true;
        attackArea.OverlapCollider(cf, enemise);

        if (enemise.Count > 0)
        {
            for (int i = 0; i < enemise.Count; ++i)
            {
                if (enemise[i].GetComponent<IDamagedObject>() != null)
                {
                    SlashEffectGenerate(enemise[i].gameObject);

                    //enemise[i].GetComponent<IDamagedObject>().Damaged(1);
                }

            }
        }

    }

    void JumpEffectGenerate()
    {
        GameObject effect = Instantiate<GameObject>(jumpEffect, transform.position, transform.rotation);
        Vector2 scale = effect.transform.localScale;
        scale.x = Mathf.Abs(scale.x) * (slashVelocity.x < 0 ? -1 : 1);
        effect.transform.localScale = scale;
    }

    void SlashEffectGenerate(RaycastHit2D hit)
    {
        if (!effectedList.Contains(hit.collider.gameObject) && hit.collider.GetComponent<IDamagedObject>().Damaged(1, transform.position, default, this.gameObject))
        {
            Instantiate<GameObject>(slashEffect, hit.point, transform.rotation);
            SetPadVibrate(.5f, .5f, .5f);
            effectedList.Add(hit.collider.gameObject);

            //instantSlashStat.Ready();
        }
    }
    void SlashEffectGenerate(GameObject target)
    {
        if (!effectedList.Contains(target) && target.GetComponent<IDamagedObject>().Damaged(1, default, default, this.gameObject))
        {
            Instantiate<GameObject>(slashEffect, target.GetComponent<BoxCollider2D>().ClosestPoint(attackArea.bounds.center), transform.rotation);
            SetPadVibrate(.5f, .5f, .5f);
            effectedList.Add(target);

            //instantSlashStat.Ready();
        }
    }
    // Update is called once per frame

    IEnumerator SlashRun(float time)
    {
        //sfxCon.PlaySound("Slash");
        //Debug.LogWarning("SlashRun");
        if (PlayerInputControl.direction == Vector2.zero)
        {
            if (slashKeyDown < KeyDownTimeMin)
            {
                slashKeyDown = 0;
            }
            else
            {
                stepCount = 0;
                slashKeyDown = 0;
                slashStart = true;
                //Invoke("SlashEnd", .01f);

                yield return new WaitForSeconds(0.01f);
                Slash();

            }
        }
        else
        {
            TrailEffectGenerate();
            //trailEffect.forceRenderingOff = true;

            slashAngleChange = SlashAngleFix;

            collider.angleHit += slashAngleChange;
            float dirX = PlayerInputControl.direction.x == 0 ? 0 : Mathf.Sign(PlayerInputControl.direction.x);
            float dirY = PlayerInputControl.direction.y == 0 ? 0 : Mathf.Sign(PlayerInputControl.direction.y);
            float wideSpeed = slashKeyDown * 3f / (slashKeyDownTime);
            if (wideSpeed > 1) wideSpeed = 1f;
            if (stepCount > stepRate.Length) stepCount = stepRate.Length;

#if UNITY_EDITOR
            if (_debug.debug)
            {
                stepCount = _debug.step + 1;
            }
#endif
            slashVelocity.x = (slashWideMax * stepRate[stepCount - 1].x) * dirX;
            slashVelocity.y = (slashWideMax * stepRate[stepCount - 1].y) * dirY;
            slashVelocity *= 1.0f / slashTime;


            stepCount = 0;
            slashKeyDown = 0;
            slashStart = true;

            //Debug.Log("Slasing");
            //collider.VelocityReset();
            if (slashVelocity != Vector2.zero)
            {
                JumpEffectGenerate();
                collider.gravityActive = false;
                collider.VelocityReset();
                collider.AddPower(slashVelocity);
                trailEffect.transform.position = collider.colBox.center - slashVelocity.normalized;
                Debug.Log("GSlash : " + slashVelocity);
            }
            else
            {
                collider.gravityActive = true;
            }
        }
    }

    void SlashAfter()
    {
        Vector2 vel = collider.GetVelocity();
        collider.VelocityReset();
        collider.AddPower(vel * new Vector2(.3f, .2f));
        collider.gravityActive = true;
    }

    void SlashReset()
    { 

        effectedList.Clear();
        if (slashStart)
        {
            SlashAfter();
        }
        slashVelocity = Vector2.zero;
        slashingTime = 0;
        slashKeyDown = 0;
        stepCount = 0;
        slashStart = false;
        velocity = Vector2.zero;
    }

    internal void InstantSlash(Vector2 dir)
    {
        GameObject effect = Instantiate<GameObject>(effectGenerate.GetEffect("SecondJump"));
        effect.transform.position = transform.position;
        effect.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Vector2.SignedAngle(Vector2.right, dir)));
        SlashReset();
        maxY = this.transform.position.y;
        slashVelocity = 3 * dir/ slashTime;
        collider.gravityActive = false;
        slashStart = true;
        if(trailEffect == null)
        {
            TrailEffectGenerate();
        }
        trailEffect.GetComponent<TrailEffectControl>().SetColor(instantSlashColor);
        //TrailEffectGenerate(instantSlashColor);
        collider.VelocityReset();
        collider.AddPower(slashVelocity);
        Debug.Log("ISlash : " + slashVelocity);
        instantSlash = false;
    }

    bool InstantSlashBreak()
    {
        if(PlayerInputControl.IsPressedJumpKey() == false)
        {
            Vector2 dir = PlayerInputControl.GetPlayerDir();
            if(dir != Vector2.zero)
                InstantSlash(dir);
            return true;
        }
        if (collider.colBox.below||playerHit)
        {
            return true;
        }
        return false;
    }
    
    void InstantSlashEnd()
    {
        if (instantSlash == true&& collider.colBox.below == false && playerHit == false&& PlayerInputControl.IsPressedJumpKey() == true)
        {
            Vector2 dir = PlayerInputControl.GetPlayerDir();
            if (dir != Vector2.zero)
                InstantSlash(dir);
        }
        instantSlash = false;
        
    }

    internal void InstantSlashReady()
    {
        SlashReset();
        instantSlash = true;
        GameObject effect = Instantiate<GameObject>(effectGenerate.GetEffect("Parrying"));
        effect.transform.position = GetComponent<BoxCollider2D>().bounds.center;
        soundFXCon.PlaySound("InstantSlash");
        
        //TimeManager.instance.StartBulletTime(1f, .8f, InstantSlashEnd, InstantSlashBreak);
        TimeManager.instance.StartBulletTime(instantSlashSlowRate, instantSlashTime, InstantSlashEnd, InstantSlashBreak);
    }

    void TrailEffectGenerate()
    {
        trailEffect = Instantiate<GameObject>(trailEffectPrefabs).GetComponent<TrailRenderer>();
        trailEffect.transform.position = collider.colBox.center;
    }
    void TrailEffectGenerate(Color color)
    {
        trailEffect = Instantiate<GameObject>(trailEffectPrefabs).GetComponent<TrailRenderer>();
        trailEffect.transform.position = collider.colBox.center;
        trailEffect.GetComponent<TrailEffectControl>().SetColor(color);
    }
}
