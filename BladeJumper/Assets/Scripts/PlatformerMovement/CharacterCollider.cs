using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerControl
{
    public delegate void AngleChange(RaycastHit2D hit);
    public delegate void CollisionEventHandler(RaycastHit2D hit);

    [RequireComponent(typeof(BoxCollider2D))]
    public class CharacterCollider : ColliderControl
    {
        /* 기능 설명
         * 
         * 캐릭터들의 물리 구현
         * 
         * 움직임 기능
         * 힘 추가
         * 경사면
         * 모서리 미끄러짐
         * 충돌 기능
         * 미끄러짐 기능
         * 
         */

        /* 변수 설명
         * LayerMask walls
         *      충돌 판정의 레이어 마스크
         *      
         * internal float gravity = 1
         *      초당 떨어지는 속도 증가량
         *      
         * bool gravityActive = true;
         *      중력 적용 여부 설정
         *      
         * internal bool standing;
         *      지면에 서있는지 확인
         *      
         * Vector2 velocity = new Vector2();
         *      현재 속도
         *      
         * float velocityYMin = -10;
         *      velcoty.y의 하한선
         *      중력으로 인해 필요 이상의 속도로 떨어지는것을 방지
         *      
         * float frictionTimeCoefficient = 0;
         *      마찰계수
         *      
         * float maxSlopeAngle = 30;
         *      올라갈수 있는 최대 각도
         *      
         * internal AngleChange angleHit;
         *      타고 올라가는 각도 변경 핸들러
         *      
         * internal CollisionEventHandler collisonEvent;
         *      충돌시 발생되는 핸들러
         */

        [SerializeField]
        internal LayerMask walls;


        [SerializeField]
        internal float gravity = 1;

        internal bool gravityActive = true;

        internal bool standing;

        Vector2 velocity = new Vector2();
        [SerializeField]
        float velocityYMin = -10;
        [SerializeField]
        float frictionTimeCoefficient = 0;

        [SerializeField]
        float maxSlopeAngle = 30;

        internal BoxCollider2D col;


        internal CollisionEventHandler angleHit;
        internal CollisionEventHandler collisonEvent;


        static internal float coliderTimeScale = 1;
        internal float timeScale = 1;

        // Start is called before the first frame update
        void Start()
        {
            Physics2D.autoSyncTransforms = true;
            col = GetComponent<BoxCollider2D>();
            colBox.SetPoint(col);
            
            angleHit += CollisionAction;
            collisonEvent += CollisionAction;
        }

        internal float GetTimeScale()
        {
            return timeScale * coliderTimeScale;
        }

        internal void AddPower(Vector2 movement)
        {
            velocity += movement;
        }
        internal Vector2 GetVelocity()
        {
            return velocity;
        }
        internal float GetGravity()
        {
            return gravity;
        }

        internal void VelocityReset()
        {
            velocity = Vector2.zero;
        }
        

        private void FixedUpdate()
        {
            MoveCalculate();
        }

        void MoveCalculate()
        {
            if(GetTimeScale() > 0)
            {
                GravityAdd();//중력 계산
                FrictionCalculate();// 마찰 계산
                                    //if(velocity.x != 0)
                                    //Debug.Log(velocity.x);
                if (velocity.y < velocityYMin)
                {
                    velocity.y = velocityYMin;
                }
                //움직임 실행
                Move(velocity * Time.fixedDeltaTime * coliderTimeScale * timeScale);
                // ColliderBox2D 주변의 상태 확인(below, above, right, left)
                BoxStatusUpdate();
                PostVelocityCaculate();
            }
            else
            {
                colBox.below = false;
            }
        }

        /// <summary>
        /// 중력 처리
        /// </summary>
        void GravityAdd()
        {
            if (gravityActive)
            {
                velocity.y -= gravity * Time.fixedDeltaTime * coliderTimeScale * timeScale;
            }
        }

        /// <summary>
        /// 마찰 처리
        /// </summary>
        void FrictionCalculate()
        {


        }

        /// <summary>
        /// 운동량에 따라 이동.
        /// </summary>
        /// <param name="moveAmount">운동량</param>

        void Move(Vector2 moveAmount)
        {
            
            colBox.SetPoint(col);
            // 바닥의 각도 확인
            BottomCheck(ref moveAmount);
            
            MoveCalculate(ref moveAmount);
            if(moveAmount.magnitude > .0f)
            {
                //Debug.Log("Add");
                MoveCalculate(ref moveAmount);
            }

            SkinMove(Vector2.down);
            
            //충돌 지점까지 translate
            //raycast를 이용하여 충돌면의 법선벡터 확인
            //각도계산
            //collisionEvent
            //운동량의 남는 부분이 기중보다 클 경우 다시 반복
        }


        /* void BoxStatusUpdate()
         * colBox 정보 Update
         * 
         * 1. col 의 상,좌,우를 skinwidth 만큼 검사
         * 2. col 의 아래를 검사.
         *      모서리에 서있으면 경사면에 서있는 것으로 처리
         */
        void BoxStatusUpdate()
        {
            bool edge;
            RaycastHit2D hit;
            //RaycastHit2D hit = BoxCast(Vector2.down, skinWidth, out edge);


            colBox.SetPoint(col);
            if ((hit = BoxCast(Vector2.right, skinWidth)) && (hit.normal.x == -1 || Vector2.Angle(hit.normal, Vector2.up) >= maxSlopeAngle)) colBox.right = true;
            if ((hit = BoxCast(Vector2.left, skinWidth)) && (hit.normal.x == 1 || Vector2.Angle(hit.normal,Vector2.up) >= maxSlopeAngle)) colBox.left = true;
            if (hit = BoxCast(Vector2.up, skinWidth)) colBox.above = true;
            
            if (hit = BoxCast(Vector2.down, skinWidth, out edge))
            {
                standing = true;
                colBox.bottomCollider = hit.collider;
                if(!edge) 
                {
                    if(Vector2.Angle(hit.normal, Vector2.up) < maxSlopeAngle)
                    {
                        colBox.below = true;
                    }
                    else
                    {
                        colBox.descendingSlope = true;
                    }
                }
            }
            else
            {
                standing = false;
            }
        }

        /* void PostVelocityCaculate()
         * 모서리에 서있는 경우에 대한 처리
         * 
         */
        void PostVelocityCaculate()
        {
            if (colBox.above)
            {

            }
            if(colBox.below)
            {
                if (gravityActive)
                {
                    if(colBox.bottomCollider != null && colBox.bottomCollider.tag == "slippery")
                    {
                        velocity.x = velocity.x * (1-.1f);
                        if(Mathf.Abs( velocity.x )< 0.1f)
                        {
                            velocity.x = 0;
                        }
                    }
                    else
                    {
                        velocity.x = 0;
                    }
                    velocity.x = 0;
                    velocity.y = 0;
                }
            }

            if (colBox.right && velocity.x > 0)
            {
                //velocity.x = 0;
            }
            if (colBox.left && velocity.x < 0)
            {
                //velocity.x = 0;
            }
        }

        void BottomCheck(ref Vector2 moveAmount)
        {
            if(moveAmount.y > 0 )
            {
                return;
            }
            bool edge;
            RaycastHit2D hit = BoxCast(Vector2.down, skinWidth ,out edge);
            if (hit)
            {
                if(hit.normal.y != 0)
                {
                    if(moveAmount.y < 0&& Vector2.Angle(hit.normal, Vector2.up) < maxSlopeAngle&&gravityActive)
                    {
                        moveAmount.y = 0;
                    }
                    MoveRotate(ref moveAmount, hit);
                }
                
                if (edge)
                {
                 
                    float edgeDistance = hit.centroid.x + (moveAmount.x > 0? colBox.extends.x:-colBox.extends.x) - hit.point.x;
                    moveAmount.y = 0;
                    if (Mathf.Abs(edgeDistance) > Mathf.Abs(moveAmount.x))
                    {

                    }
                    if(Mathf.Abs(moveAmount.x) < .1f)
                    {
                       moveAmount.x = .1f;
                    }
                    if(hit.point.x < transform.position.x)
                    {
                       moveAmount.x = Mathf.Abs(moveAmount.x);
                    }
                    else
                    {
                        moveAmount.x = -Mathf.Abs(moveAmount.x);
                    }
                   
                }
                collisonEvent(hit);
            }
        }

        void MoveCalculate(ref Vector2 moveAmount)
        {
            Debug.DrawRay(colBox.center, moveAmount * 10, Color.red);
            float rayLength = moveAmount.magnitude ;
            RaycastHit2D hit = BoxCast(moveAmount, rayLength);

            if (hit)
            {
                float rDistance = rayLength-hit.distance;
                moveAmount = moveAmount.normalized * (hit.distance) ;
                Translate(moveAmount);
                SkinMove(moveAmount);
                moveAmount = moveAmount.normalized * rDistance;
                if(Mathf.Abs( hit.normal.x) == 1)
                {
                    moveAmount.x *= -1;
                    //Debug.Log("WallHit. centroid : " + hit.centroid + ", currentCenter : "+colBox.center);
                    
                }
                if(hit.normal.y > 0)
                {
                    //MoveRotate(ref moveAmount, hit);
                }
                else
                {
                    //Debug.Log("rDistance : " + rDistance);
                    //moveAmount = Vector2.zero;
                }
                
                Debug.DrawRay(colBox.center, moveAmount.normalized * 10, Color.green);
                collisonEvent(hit);
                
            }
            else
            {
                Translate(moveAmount);
                SkinMove(moveAmount);
                moveAmount = Vector2.zero;
            }
        }
        /* SkinMove
         * dir 방향으로 skinWidth 거리 안에 충돌체가 있으면 skinWidth만큼 떨어지게 하는 함수 
         */
        void SkinMove(Vector2 dir)
        {

            Vector2 dirY = Vector2.down;
            RaycastHit2D hit = BoxCast(dirY, skinWidth);
            if (hit && hit.point.y <= colBox.bottomRight.y)
            {
                
                Translate(-dirY * (skinWidth - hit.distance));
            }
            dirY = Vector2.up;
            hit = BoxCast(dirY, skinWidth);
            if (hit && hit.point.y >= colBox.topRight.y)
            {
                Translate(-dirY * (skinWidth - hit.distance));
            }

            Vector2 dirX = Vector2.right;
            hit = BoxCast(dirX, skinWidth);
            if (hit && hit.point.x >= colBox.bottomRight.x)
            {
                
                Translate(-dirX * (skinWidth - hit.distance));
            }
            dirX = Vector2.left;
            hit = BoxCast(dirX, skinWidth);
            if (hit && hit.point.x <= colBox.bottomLeft.x)
            {
                Translate(-dirX * (skinWidth - hit.distance));
            }
        }
        void CollisionCheck(Vector2 moveAmount)
        {
            float distance = (moveAmount + Vector2.one * skinWidth).magnitude;


        }

        void VelocityWallHit(RaycastHit2D hit)
        {
            
        }
        void CollisionAction(RaycastHit2D hit)
        {
            //Debug.Log(Vector2.Angle(hit.normal, velocity));

            if(Mathf.Abs(hit.normal.x) > .8)
            {
                velocity.x = Mathf.Abs(velocity.x) * Mathf.Sign(hit.normal.x);
            }
            else if (hit.normal.y == -1)
            {
               // Debug.Log("revY");
                velocity.y = -Mathf.Abs(velocity.y) ;
            }
            else if (hit.normal.y < 0)
            {
                velocity = Vector2.Reflect(velocity.normalized, hit.normal) * velocity.magnitude;
            }
            else
            {
                MoveRotate(ref velocity, hit);
            }
            
            Debug.DrawRay(colBox.center + Vector2.up * -.1f,velocity.normalized * 5, Color.green);
            
        }
        
        



        internal void MoveRotate(ref Vector2 moveAmount, RaycastHit2D hit)
        {

            /*
            float angle = Vector2.Angle(hit.normal, Vector2.up);
            float rad = angle * Mathf.Deg2Rad;
            Vector2 slopeDir = new Vector2(Mathf.Cos(rad) * Mathf.Sign(hit.normal.x * -1), Mathf.Sin(rad)).normalized;
            float slopeDistance = moveAmount.x * Mathf.Cos(rad) * Mathf.Sign(hit.normal.x * -1) + moveAmount.y * Mathf.Sin(rad);

            moveAmount = slopeDir * slopeDistance;
            */
            

            float sRad = Vector2.SignedAngle(Vector2.up,hit.normal) * Mathf.Deg2Rad;
            float mRad = Vector2.SignedAngle(moveAmount,hit.normal) * Mathf.Deg2Rad;
            float slopeDistance = moveAmount.magnitude * Mathf.Sin(mRad);
            moveAmount = new Vector2(Mathf.Cos(sRad), Mathf.Sin(sRad)) * slopeDistance;


            Debug.DrawRay(colBox.center, new Vector2(Mathf.Cos(sRad), Mathf.Sin(sRad)) * 5,Color.blue);
            Debug.DrawRay(colBox.center + Vector2.up * .1f, (new Vector2(Mathf.Cos(sRad), Mathf.Sin(sRad)) * slopeDistance).normalized * 5, Color.white);


        }
    }
}