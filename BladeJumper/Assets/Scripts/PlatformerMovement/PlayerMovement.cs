using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;


[RequireComponent(typeof(PlayerInputControl))]
[RequireComponent(typeof(CharacterCollider))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    float speed;

    [SerializeField]
    float jumpPower;

    [SerializeField]
    float gravity = 3f;
    new CharacterCollider collider;

    bool jump = false;

    float smoothX = 0;
    internal Vector2 velocity = new Vector2();



    private void Awake()
    {
        collider = GetComponent<CharacterCollider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        
        velocity.x = PlayerInputControl.direction.x * speed * Time.deltaTime; // 좌우 방향키 조작

        if(collider.colBox.below)
        {
            jump = false;
        }

        if(collider.colBox.below && PlayerInputControl.jump > 0&& !jump)
        {
            velocity.y = jumpPower * Time.deltaTime;//중력 적용
            jump = true;
        }

        velocity.y -= gravity * Time.deltaTime;//중력 적용
        
        

        
        collider.AddPower(velocity);//CharacterCollider에 Input

    }

    // Update is called once per frame
    

    
}
