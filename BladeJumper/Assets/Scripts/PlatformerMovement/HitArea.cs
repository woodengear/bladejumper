using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerControl
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class HitArea : MonoBehaviour
    {
        [SerializeField]
        LayerMask hitLayer;
        [SerializeField]
        float interval = 1;
        float current_internval = 0;
        // Start is called before the first frame update

        protected BoxCollider2D col;
        bool visible;
        private void OnBecameVisible()
        {
            visible = true;
        }
        private void OnBecameInvisible()
        {
            visible = false;
        }

        private void Awake()
        {
            col = GetComponent<BoxCollider2D>();
        }

        private void OnDrawGizmos()
        {
           
        }

        private void FixedUpdate()
        {
            if(current_internval < interval)
            {
                current_internval += Time.fixedDeltaTime;
            }
            else
            {
                if (visible)
                {
                    ContactFilter2D cfil = new ContactFilter2D();
                    cfil.layerMask = hitLayer;
                    cfil.useLayerMask = true;
                    List<Collider2D> hitCols = new List<Collider2D>();
                    col.OverlapCollider(cfil, hitCols);

                    if (hitCols.Count > 0)
                    {
                        DamageAction(hitCols);
                        current_internval = 0;
                    }
                }
            }
        }

        protected virtual void DamageAction(List<Collider2D> hitCol)
        {

        }
    }
}