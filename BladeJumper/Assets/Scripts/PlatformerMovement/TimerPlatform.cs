using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PlatformerControl
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class TimerPlatform : MonoBehaviour
    {
        [SerializeField]
        LayerMask characterLayer;

        [SerializeField]
        float _time = 3;
        [SerializeField]
        float _resetTime = 10;

        [SerializeField]
        GameObject child;
        [System.Serializable]
        struct PlatformAnimation
        {
            public string brokenAnimation;
            public  string resetAnimation;
        }
        [SerializeField]
        PlatformAnimation _animation;
        bool visible = false;
        bool passenger = false;


        
        BoxCollider2D col;
        SpriteRenderer sRenderer;
        Animator animator;
        bool animationEnd = false;
        private void OnBecameVisible()
        {
            PlatformControl(true);
            passenger = false;
            visible = true;
            
        }
        private void OnBecameInvisible()
        {
            StopAllCoroutines();
            visible = false;
            if (animator != null)
            {
                animator.Rebind();
                animator.Update(0);
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            col = GetComponent<BoxCollider2D>();
            sRenderer = GetComponent<SpriteRenderer>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void FixedUpdate()
        {
            if (visible)
            {
                if (passenger == false)
                {
                    
                    RaycastHit2D hit = Physics2D.BoxCast((Vector2)transform.position + col.offset, col.size, 0, Vector2.up, 0.1f, characterLayer);
                    if (hit)
                    {
                        passenger = true;
                        StartCoroutine(BreakRoutine());
                    }
                }
            }
        }

        IEnumerator BreakRoutine()
        {
            float cTime = _time;
            while ((cTime -= Time.deltaTime)>0)
            {
                
                yield return null;
            }

            PlayAnimation(_animation.brokenAnimation);

            Bounds bounds = col.bounds;
            PlatformControl(false);
            cTime = _resetTime;
            while ((cTime -= Time.deltaTime) > 0)
            {
                yield return null;
            }

            ContactFilter2D cfil = new ContactFilter2D();
            cfil.layerMask = characterLayer;
            cfil.useLayerMask = true;
            List<Collider2D> hitCols = new List<Collider2D>();
            col.OverlapCollider(cfil, hitCols);

            yield return new WaitUntil(() => CharacterCheck(bounds));
            yield return WaitAnimaionEnd(_animation.resetAnimation);
            PlatformControl(true);
            passenger = false;
        }

        void PlayAnimation(string animation)
        {
            if (animator != null)
            {
                animator.Play(animation);
            }
        }

        IEnumerator WaitAnimaionEnd(string animation)
        {
            if(animator != null)
            {
                animationEnd = false;
                animator.Play(animation);
                yield return new WaitUntil(() => animationEnd);
            }
        }
        public void AnimationEnd()
        {
            animationEnd = true;
        }
        bool CharacterCheck(Bounds bounds)
        {
            int count = Mathf.CeilToInt((bounds.extents.x/2) / .05f);
            RaycastHit2D hit;
            for (int i = 0; i <count; i++)
            {
                hit = Physics2D.Linecast((Vector2)bounds.min + Vector2.right *.2f*i - Vector2.up * .4f, new Vector2(bounds.min.x, bounds.max.y + .4f) + Vector2.right * .2f * i, characterLayer);
                Debug.DrawLine((Vector2)bounds.min + Vector2.right * .2f * i, new Vector2(bounds.min.x,bounds.max.y) + Vector2.right * .2f * i);
                if (hit)
                {
                    return false;
                }
            }
            return true;
        }
        void PlatformControl(bool active)
        {
            if(child != null)
                child.SetActive(active);
            Color color = sRenderer.color;
            color.a = active ? 1 : 0;
            sRenderer.color = color;
            col.enabled = active;
        }
    }
}