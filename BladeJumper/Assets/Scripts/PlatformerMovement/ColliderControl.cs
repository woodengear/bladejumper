using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerControl
{

    public delegate void TranslateEvent(Vector2 moveAmount);

    public class ColliderControl : MonoBehaviour
    {
        
        [SerializeField]
        protected LayerMask collisionMask;
        [SerializeField]
        protected float skinWidth = 0.035f;

        internal TranslateEvent translateEventHandelr;

        internal ColliderBox colBox;
        
        internal struct ColliderBox
        {
            internal Vector2 topLeft;
            internal Vector2 topRight;
            internal Vector2 bottomLeft;
            internal Vector2 bottomRight;

            internal Vector2 center;
            internal Vector2 extends;
            internal Vector2 size;
            internal float width;
            internal float height;

            internal bool below, above;
            internal bool right, left;

            internal float sideAngle;

            internal bool climbing;
            internal bool slope;
            internal float slopeAngle;
            internal float slopeOldAngle;
            internal Vector2 slopeNormal;
            internal bool slidingSlope;
            internal bool descendingSlope;
            internal Vector2 movementOld;

            internal Collider2D bottomCollider;

            internal int rayCountVertical, rayCountHorizontal;
            internal float spacingVertical, spacingHorizontal;

            internal float bottomLeftAngle, bottomRightAngle;
            internal float topLeftAngle, topRightAngle;

            internal BoxCollider2D collider;
            internal void SetPoint(BoxCollider2D col)
            {
                below = false;
                above = false;
                right = false;
                left = false;

                climbing = false;
                slope = false;
                slopeOldAngle = slopeAngle;
                slopeAngle = 0;
                slopeNormal = Vector2.zero;

                slidingSlope = false;
                descendingSlope = false;

                sideAngle = 0;

                RefreshBounds(col);
            }

            internal void RefreshBounds(BoxCollider2D col)
            {
                collider = col;
                Bounds bounds = col.bounds;
                this.center = bounds.center;
                extends = bounds.extents;
                size = bounds.size;
                topLeft = new Vector2(bounds.min.x, bounds.max.y);
                topRight = new Vector2(bounds.max.x, bounds.max.y);
                bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
                bottomRight = new Vector2(bounds.max.x, bounds.min.y);
                //Debug.Log(bottomLeft.y);
                width = bounds.size.x;
                height = bounds.size.y;



                rayCountHorizontal = Mathf.RoundToInt(height / 0.1f);
                rayCountVertical = Mathf.RoundToInt(width / 0.05f);

                spacingHorizontal = bounds.size.y / (rayCountHorizontal - 1);
                spacingVertical = bounds.size.x / (rayCountVertical - 1);

                bottomLeftAngle = Vector2.SignedAngle(Vector2.right, bottomLeft - center);
                bottomRightAngle = Vector2.SignedAngle(Vector2.right, bottomRight - center);
                topLeftAngle = Vector2.SignedAngle(Vector2.up, topLeft - center);
                topRightAngle = Vector2.SignedAngle(Vector2.up, topRight - center);

                
                bottomCollider = null;
            }

            internal void Translate(Vector2 movement)
            {
                center += movement;
                topLeft += movement;
                topRight += movement;
                bottomLeft += movement;
                bottomRight += movement;
            }
        }

        /// <summary>
        /// direction 방향으로 distance에 있는 collisionMask 레이어에 속한 충돌체와의 충돌 검사
        /// </summary>
        /// <param name="direction">BoxCast의 방향</param>
        /// <param name="distance">검사할 거리</param>
        /// <returns></returns>
        protected RaycastHit2D BoxCast(Vector2 direction, float distance)
        {
            RaycastHit2D hit = Physics2D.BoxCast(colBox.center, colBox.size, 0, direction, distance, collisionMask);
            if (hit)
            {
                hit.distance = Vector2.Distance(colBox.center, hit.centroid);
            }

            return hit;
        }

        /// <summary>
        /// direction 방향으로 distance에 있는 collisionMask 레이어에 속한 충돌체와의 충돌 검사
        /// </summary>
        /// <param name="direction">BoxCast의 방향</param>
        /// <param name="distance">검사할 거리</param>
        /// <param name="bottomEdge">충돌체의 모서리 부분과 충돌 확인</param>
        /// <returns></returns>
        protected RaycastHit2D BoxCast(Vector2 direction, float distance, out bool bottomEdge)
        {
            RaycastHit2D hit = BoxCast(direction, distance);
            bottomEdge = false;
            
            if (hit &&hit.normal.y == 1 && direction.y < 0 &&hit.point.y <= hit.centroid.y - colBox.extends.y &&
                (hit.point.x <= hit.centroid.x + colBox.extends.x && hit.point.x >= hit.centroid.x - colBox.extends.x))
            {
                RaycastHit2D rHit = Physics2D.Raycast(hit.point + (Vector2.up * skinWidth), Vector2.down, skinWidth * 1.1f, collisionMask);
                if(rHit&&hit.normal != rHit.normal)
                {
                    Debug.Log("Edge Hit");
                    hit.normal = rHit.normal;
                    bottomEdge = true;
                }
            }

            return hit;
        }

        protected RaycastHit2D BoxCastNormal(Vector2 direction, float distance)
        {
            RaycastHit2D hit = BoxCast(direction, distance);
            if (hit)
            {
                hit = Physics2D.Raycast(hit.point - direction.normalized * skinWidth, direction, skinWidth * 2, collisionMask);
            }
            
            return hit;
        }

        protected void Translate(Vector2 moveAmount)
        {
            if(translateEventHandelr != null)
            {
                translateEventHandelr(moveAmount);
            }

            transform.Translate(moveAmount);
            colBox.Translate(moveAmount);
        }
    }
}