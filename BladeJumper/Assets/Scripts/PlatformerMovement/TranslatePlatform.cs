using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerControl
{

    [RequireComponent(typeof(BoxCollider2D))]
    public class TranslatePlatform : MonoBehaviour
    {
        [SerializeField]
        LayerMask playerMask;
        [SerializeField]
        LayerMask playerCollisionMask;

        Vector2 startPos;
        [SerializeField]
        Vector2 targetPos;
        Vector2 endPos;

        [SerializeField]
        float speed;

        [SerializeField]
        float waitTime = .5f;

        BoxCollider2D col;
        bool reverse = false;
        [SerializeField]
        float rDistance;
        [SerializeField]
        float currentDistance;
        Vector2 dir;

        BoxCollider2D[] childBox;
        List<Collider2D> hitCol = new List<Collider2D>();

        // Start is called before the first frame update
        private void OnDrawGizmos()
        {
            
            if(startPos != null && endPos != startPos)
            {
                Debug.DrawLine(startPos, endPos, Color.yellow);
            }
            else
            {
                Debug.DrawLine(transform.position, (Vector2)transform.position + targetPos, Color.yellow);
            }
        }

        void Start()
        {
            startPos = transform.position;
            endPos = (Vector2)transform.position + targetPos;
            transform.position = startPos;
            col = GetComponent<BoxCollider2D>();
            dir = (endPos - startPos).normalized;
            rDistance = Vector2.Distance(startPos,endPos);
            childBox = GetComponentsInChildren<BoxCollider2D>();
        }

        private void FixedUpdate()
        {
            MovePlatform();
        }

        void MovePlatform()
        {
            Vector2 cDir = reverse ? -dir : dir;
            float distance = speed * Time.deltaTime;
            bool dirChange = false;
            RaycastHit2D[] hit = null;
            hitCol.Clear();

            for (int j = 0; j < childBox.Length; j++)
            {
                hit = Physics2D.BoxCastAll(childBox[j].bounds.center, childBox[j].bounds.size, 0, cDir, distance, playerMask);
                for (int i = 0; i < hit.Length; i++)
                {
                    if (hit[i])
                    {
                        if(hitCol.Contains(hit[i].collider) == false)
                        {
                            hitCol.Add(hit[i].collider);
                            
                            if ( PlayerHit(ref cDir, hit[i],childBox[j]))
                            {
                                
                                if (dirChange == false)
                                {
                                    dirChange = true;
                                    reverse = !reverse;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            
            if((hit != null && hit.Length == 0)|| !dirChange)
            {
                transform.Translate(cDir * distance);
                currentDistance += distance * (reverse ? -1 : 1);
            }


            PassengerCheck(cDir);
            if(reverse && currentDistance < 0)
            {
                transform.position = startPos;
                reverse = false;
                currentDistance = 0;
            }else if(!reverse && currentDistance > rDistance)
            {
                transform.position = endPos;
                reverse = true;
                currentDistance = rDistance;
            }
            
        }

        bool PlayerHit(ref Vector2 dir, RaycastHit2D hit,Collider2D col)
        {
            hit.distance = Vector2.Distance(col.bounds.center, hit.centroid);
            CharacterCollider player = hit.collider.GetComponent<CharacterCollider>();
            ColliderControl.ColliderBox colBox = player.colBox;

            RaycastHit2D cHit = Physics2D.BoxCast(player.col.bounds.center, player.col.bounds.size, 0, dir, 0.1f,playerCollisionMask);

            

            if (cHit || ((dir == Vector2.down && colBox.below) || (dir == Vector2.up && colBox.above) || (dir == Vector2.right && colBox.right) || (dir == Vector2.left && colBox.left)))
            {
                return true;
            }
            else
            {
                //transform.Translate(dir * speed * Time.deltaTime);
                player.transform.Translate(dir * (speed * Time.deltaTime + 0.01f - hit.distance));
                Debug.Log("Plat hit , dir :" + dir);
                //player.AddPower(dir * (speed * Time.deltaTime - hit.distance) / Time.deltaTime);
                //currentDistance += speed * Time.deltaTime * (reverse ? -1 : 1);
                return false;
            }
        }

        void PassengerCheck(Vector2 dir)
        {
            if (dir == Vector2.up) return;

            //자식 오브젝트의 boxCollider 를 포함하여 충돌 검사
            //List<BoxCollider2D> childBox = new List<BoxCollider2D>(GetComponentsInChildren<BoxCollider2D>());
            


            for(int j = 0; j< childBox.Length; j++)
            {
                RaycastHit2D[] hit = Physics2D.BoxCastAll(childBox[j].bounds.center, childBox[j].bounds.size, 0, Vector2.up, .4f, playerMask);
                for (int i = 0; i < hit.Length; ++i)
                {
                    if (hit[i])
                    {
                        CharacterCollider player = hit[i].collider.GetComponent<CharacterCollider>();
                        
                        ColliderControl.ColliderBox colBox = player.colBox;

                        if (player.standing&& player.gravityActive)
                        {
                            if(((Mathf.Abs(dir.x) > 0&&colBox.right)&& (Mathf.Abs(dir.x) < 0 && colBox.left))==false)
                            {
                                player.AddPower(dir * speed);
                            }
                        }

                    }
                }
            }
            
        }


        // Update is called once per frame
        void Update()
        {

        }
    }
}