using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.Playables;

public class PlayerInputControl : MonoBehaviour
{
    internal static PlayerInputControl manager;

    
    static internal Vector2 direction = new Vector2();
    
    

    static internal PlayableDirector currentDirector;

    static internal float jump;
    bool usedInput = true;

    static bool rightKeyDown;
    static bool leftKeyDown;

    static internal bool keyKeep = false;
    Dictionary<string, bool> inputable = new Dictionary<string, bool>();

    internal struct ReverseDir
    {
        public bool x;
        public bool y;

        public ReverseDir(bool x, bool y)
        {
            this.x = x;
            this.y = y;
        }
    }

    static internal ReverseDir reverseDir = new ReverseDir(false,false);
    
    private void Awake()
    {
        reverseDir = new ReverseDir(false, false); 
        manager = this;
    }
    // Start is called before the first frame update

    internal bool GetInputEnable()
    {
        return !inputable.ContainsValue(false);
    }
    internal bool GetInputEnable(string key)
    {
        if (inputable.ContainsKey(key))
        {
            return inputable[key];
        }
        return true;
    }
    // Update is called once per frame
    void Update()
    {
        direction = Vector2.zero;
        jump = 0;
        if (UserInput.WasPressed(Key.Q)&&inputable.Count > 0)
        {
            foreach (string key in inputable.Keys)
            {
                Debug.Log(key + " = " +inputable[key]);
            }
        }
        if (!inputable.ContainsValue(false))
        {
            direction = Vector2.zero;
            jump = 0;

            direction = UserInput.GetGamepadStick(GamepadButton.LeftStick);
            jump = UserInput.ReadValue(GamepadButton.A);


            if (Mathf.Abs(direction.y) > 0.2f)
            {
                direction.y = Mathf.Sign(direction.y);
            }
            else
            {
                direction.y = 0;
            }


            if (Mathf.Abs(direction.x) > 0.2f)
            {
                direction.x = Mathf.Sign(direction.x);
            }
            else
            {
                direction.x = 0;
            }

            if (direction == Vector2.zero)
            {
                //키보드
                if (UserInput.IsPressed(KeySetting.keys[KeyAction.LEFT]))
                {
                    direction.x -= 1;
                }
                if (UserInput.IsPressed(KeySetting.keys[KeyAction.RIGHT]))
                {
                    direction.x += 1;
                }
                if (UserInput.IsPressed(KeySetting.keys[KeyAction.UP]))
                {
                    direction.y += 1;
                }
                if (UserInput.IsPressed(KeySetting.keys[KeyAction.Down]))
                {
                    direction.y -= 1;
                }
                if (UserInput.IsPressed(KeySetting.keys[KeyAction.JUMP]))
                {
                    jump += 1;
                }
            }
        }
        if (reverseDir.x)
        {
            direction.x *= -1;
        }
        if (reverseDir.y)
        {
            direction.y *= -1;
        }
    }



    static internal Vector2 GetPlayerDir()
    {
        Vector2 direction = UserInput.GetGamepadStick(GamepadButton.LeftStick);
        if (Mathf.Abs(direction.y) > 0.3f)
        {
            direction.y = Mathf.Sign(direction.y);
        }
        else
        {
            direction.y = 0;
        }

        if (Mathf.Abs(direction.x) > 0.3f)
        {
            direction.x = Mathf.Sign(direction.x);
        }
        else
        {
            direction.x = 0;
        }

        if (direction == Vector2.zero)
        {
            //키보드
            if (UserInput.IsPressed(KeySetting.keys[KeyAction.LEFT]))
            {
                direction.x -= 1;
            }
            if (UserInput.IsPressed(KeySetting.keys[KeyAction.RIGHT]))
            {
                direction.x += 1;
            }
            if (UserInput.IsPressed(KeySetting.keys[KeyAction.UP]))
            {
                direction.y += 1;
            }
            if (UserInput.IsPressed(KeySetting.keys[KeyAction.Down]))
            {
                direction.y += -1;
            }
        }
        if (reverseDir.x)
        {
            direction.x *= -1;
        }
        if (reverseDir.y)
        {
            direction.y *= -1;
        }
        return direction;
    }

    static internal bool IsPressedJumpKey()
    {
        return UserInput.IsPressed(KeySetting.keys[KeyAction.JUMP])||UserInput.IsPressed(GamepadButton.A);
    }
    static internal bool WasPressedJumpKey()
    {
        return UserInput.WasPressed(KeySetting.keys[KeyAction.JUMP]) || UserInput.WasPressed(GamepadButton.A);
    }
    internal void SetEnable(string key,bool enable)
    {
        //Debug.Log(key + " : " + enable);
        if (inputable.ContainsKey(key))
        {
            inputable[key] = enable;
        }
        else
        {
            inputable.Add(key, enable);
        }
    }

    internal bool GetEnableInput()
    {
        return usedInput;
    }

    public void InputActive(bool active)
    {
        usedInput = active;
    }



}