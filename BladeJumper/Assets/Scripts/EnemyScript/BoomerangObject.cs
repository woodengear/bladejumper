using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    [System.Serializable]
    public class BoomerangStatus
    {
        internal Vector2 startPosition;
        internal Vector2 targetPosition;
        public BoomerangObject boomerang;
        public float power = 10;
        public float moveSpeed = 10;
        public float rotateSpeed = 5;
        public float endDelay = .5f;
        public bool endDestroy = true;
    }

    public class BoomerangObject : MonoBehaviour
    {
        /*
         * 목표 지점에 도달후 돌아오는 오브젝트
         * 속도가 점점 줄어들다
         * 목표지점에서 속도 0
         * 
         * 
         */
        BoomerangStatus status;
        [System.Serializable]
        class EffectRequire
        {
            public string hit = "Hit";
            
        }

        [SerializeField]
        EffectGenerateControl effectControl;

        [SerializeField]
        EffectRequire effectRequire;


        internal bool endBoomerang;

        bool start = false;
        bool _return = false;
        float time;
        float currentSpeed;
        float acc;
        float speed;
        Vector2 dir;
        Vector2 vel;

        Collider2D col;
        bool hit = false;
        float endTime = 0;
        internal void StartBoomerang(BoomerangStatus status)
        {
            this.status = status;
            start = true;
            dir = status.targetPosition-(Vector2)transform.position;
            dir.Normalize();
            float distance = Vector2.Distance(transform.position, status.targetPosition) + 3;
            time = distance/status.moveSpeed;
            acc = status.moveSpeed*2/time;
            //speed*2 = acc*time
            speed = status.moveSpeed * 2;
            currentSpeed = speed;
            col = GetComponent<Collider2D>();
            
        }

        internal void FixedUpdate()
        {
            if (start)
            {
                
                transform.Translate(dir * currentSpeed*Time.fixedDeltaTime, Space.World);
                transform.Rotate(Vector3.forward * status.rotateSpeed, Space.World);
                currentSpeed -= acc * Time.fixedDeltaTime;
                if(currentSpeed <= 0)
                {
                    if(endTime < status.endDelay)
                    {
                        currentSpeed = 0;
                        endTime += Time.fixedDeltaTime;
                    }
                    if(_return == false && hit == true)
                    {
                        hit = false;
                    }
                    _return = true;
                }
                if (_return&&currentSpeed < -speed)
                {
                    endBoomerang = true;
                    if (status.endDestroy)
                    {
                        Destroy(this.gameObject);
                    }
                    
                }
                if (hit==false && col.bounds.Contains(BladeJumper.bladeJumper.GetComponent<BoxCollider2D>().bounds.center))
                {
                    GameObject effect = effectControl.GetEffect(effectRequire.hit);
                    if (effect != null)
                    {
                        effect = Instantiate(effect);
                        effect.transform.position = BladeJumper.bladeJumper.GetComponent<BoxCollider2D>().bounds.center;
                    }
                    hit = true;
                    BladeJumper.bladeJumper.Damaged(1,dir*currentSpeed, Vector2.up * 6 + Vector2.right * Mathf.Sign((dir * currentSpeed).x) * status.power);
                }
            }
        }


    }
}