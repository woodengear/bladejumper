using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "ShotSlash", menuName = "Enemy/EnemyPattern/ShotSlash")]
    public class ShotSlash : EnemyShotPattern
    {
        [SerializeField]
        string ShotFireAnimation = "Shot_Fire";
        [SerializeField]
        EnemyPattern hitSlash;

        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            yield return WaitAnimation(usedControl,attackAnimationName);
            if(ShotFireAnimation != null)
            {
                usedControl.owner.PlayAnimation(ShotFireAnimation);
            }
            yield return HitCheckShot(usedControl,HitSlash(usedControl));

        }
        IEnumerator HitSlash(EnemyUsedControl usedControl)
        {
            Vector2 pos = usedControl.owner.transform.position;
            yield return new WaitUntil(()=>BladeJumper.bladeJumper.GetComponent<CharacterCollider>().colBox.below);
            usedControl.owner.transform.position = BladeJumper.bladeJumper.transform.position;
            yield return hitSlash.Action(usedControl);
            usedControl.owner.transform.position = pos;
        }
    }
}