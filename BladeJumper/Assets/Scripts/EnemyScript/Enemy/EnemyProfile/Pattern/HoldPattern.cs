using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "Hold", menuName = "Enemy/EnemyPattern/Basic/Hold")]
    public class HoldPattern : EnemyPattern
    {
        [SerializeField]
        Vector2 holdPosition;
        [SerializeField]
        bool forceMove =false;
        [SerializeField]
        float holdTime;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            BladeJumper bj = BladeJumper.bladeJumper;
            CharacterCollider charCol = bj.GetComponent<CharacterCollider>();
            
            float scale = charCol.timeScale;
            charCol.timeScale = 0;
            BladeJumper.bladeJumper.playerHit = true;

            charCol.VelocityReset();
            float currentTime = holdTime;
            Vector2 nPos =Vector2.zero;
            while((currentTime-=Time.deltaTime) > 0)
            {
                if (forceMove)
                {
                    bj.transform.position = Vector2.SmoothDamp(bj.transform.position, holdPosition, ref nPos, .5f);
                }
                yield return null;
            }
            charCol.timeScale = scale;
        }
        
    }
}