using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Enemy.Pattern
{
    public class AreaAttackPattern : EnemyPattern
    {

        [SerializeField]
        protected Vector2 power = new Vector2(10,6);

        [SerializeField]
        protected EnemyPattern hitNext;

        protected BoxCollider2D FindArea(EnemyUsedControl usedControl, string attackAreaName)
        {
            return FindChildObject(usedControl, attackAreaName).GetComponent<BoxCollider2D>();
        }

        protected virtual IEnumerator AreaAttack(EnemyUsedControl usedControl,string attackAreaName, string attackAnimation,WaitAction hitAction = null)
        {
            BoxCollider2D attackArea = FindArea(usedControl, attackAreaName);
            Debug.Log(attackArea.gameObject.name);
            usedControl.owner.animationEnd = false;
            PlayAnimation(usedControl, attackAnimation);
            
            
            while (usedControl.owner.animationEnd == false)
            {
                if (attackArea.bounds.Contains(usedControl.playerCenter))
                {
                    BladeJumper.bladeJumper.Damaged(1, default, Vector2.up * power.y + Vector2.right * (usedControl.sRenderer.flipX ? -1 : 1) * power.x, usedControl.owner.gameObject);
                    
                    break;
                }
                yield return null;
            }
            yield return new WaitUntil(()=> usedControl.owner.animationEnd);
        }

        internal IEnumerator Action(EnemyUsedControl usedControl,string animationName, string attackAreaName,bool signalUse, System.Action areaAction)
        {
            if (signalUse)
            {
                usedControl.owner.attackSignal = false;
            }
            BoxCollider2D area = FindArea(usedControl, attackAreaName);
            yield return WaitAnimation(usedControl, animationName, () => 
            {
                if (area.bounds.Contains(usedControl.playerCenter))
                {
                    if ((signalUse == false || usedControl.owner.attackSignal))
                    {
                        areaAction();
                        usedControl.owner.attackSignal = false;
                    }
                }
            });
            yield return null;
        }
    }
    [CreateAssetMenu(fileName = "AreaAttack", menuName = "Enemy/EnemyPattern/Basic/AreaAttack")]
    public class AreaAttackBasic : AreaAttackPattern
    {
        [SerializeField]
        string attackAnimation = "Attack";
        [SerializeField]
        string attackArea = "AttackArea";

        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            yield return AreaAttack(usedControl, attackArea, attackAnimation);
        }
    }
}

