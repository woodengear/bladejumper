using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "AttackSignalAction", menuName = "Enemy/EnemyPattern/AttackSignalAction")]
    public class AttackSignalAction : AttackPattern
    {
        [SerializeField]
        EnemyPattern signalAction;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            usedControl.owner.animationEnd = false;
            PlayAnimation(usedControl, attackAnimationName);

       

            while(usedControl.owner.animationEnd == false)
            {
                if (usedControl.owner.attackSignal)
                {
                    
                    yield return signalAction.Action(usedControl);
                    usedControl.owner.attackSignal = false;
                }
                yield return null;
            }
            yield return null;
        }
    }
}