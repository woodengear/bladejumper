using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "BarrageShot", menuName = "Enemy/EnemyPattern/BarrageShot")]
    public class BarrageShot : EnemyShotPattern
    {
        [System.Serializable]
        protected enum BarrageAiming { Up, Down, Left, Right, Player }
        [System.Serializable]
        protected struct BarrageStatus
        {
            [Range(1, 10)]
            public int barrageCount;
            [Range(0, 2f)]
            public float barrage_Interval;
            public BarrageAiming aiming;
            public bool rapid;
            [Range(0, 2f)]
            public float rapid_Interval;
            public bool barrageRapid;
            [Range(0, 360)]
            public float shotAngle;
            [Range(1, 30)]
            public int numberOfBullet;
        }

        [SerializeField]
        BarrageStatus barrageStatus;
        internal override IEnumerator Action(EnemyUsedControl enemyInfo)
        {
            usedControl = enemyInfo;
            yield return usedControl.WaitAnimation("Attack");

            Vector2 dir;
            switch (barrageStatus.aiming)
            {
                case BarrageAiming.Up:
                    dir = Vector2.up;
                    break;
                case BarrageAiming.Down:
                    dir = Vector2.down;
                    break;
                case BarrageAiming.Left:
                    dir = Vector2.left;
                    break;
                case BarrageAiming.Right:
                    dir = Vector2.right;
                    break;
                case BarrageAiming.Player:
                    dir = usedControl.playerDir;
                    break;
                default:
                    dir = usedControl.playerDir;
                    break;
            }
            WaitForSeconds rapidWait = new WaitForSeconds(barrageStatus.rapid_Interval);
            for (int j = 0; j < barrageStatus.barrageCount; j++)
            {
                for (int i = 0; i < barrageStatus.numberOfBullet; i++)
                {
                    if (j % 2 == 1 && barrageStatus.barrageRapid)
                    {
                        Shot(enemyInfo,RotateVector(dir, ((barrageStatus.shotAngle / barrageStatus.numberOfBullet) * (i - barrageStatus.numberOfBullet / 2))));
                    }
                    else
                    {
                        Shot(enemyInfo,RotateVector(dir, ((barrageStatus.shotAngle / barrageStatus.numberOfBullet) * (-i + barrageStatus.numberOfBullet / 2))));
                    }


                    if (barrageStatus.rapid)
                    {
                        yield return rapidWait;
                    }
                }
                if (barrageStatus.rapid == false || barrageStatus.barrageRapid == false)
                {
                    yield return new WaitForSeconds(barrageStatus.barrage_Interval);
                }

            }

        }
        Vector2 RotateVector(Vector2 vec, float angle)
        {
            float rad = Mathf.Deg2Rad * angle;
            return new Vector2(vec.x * Mathf.Cos(rad) + vec.y * Mathf.Sin(rad), -Mathf.Sin(rad) * vec.x + Mathf.Cos(rad) * vec.y);
        }
    }
}