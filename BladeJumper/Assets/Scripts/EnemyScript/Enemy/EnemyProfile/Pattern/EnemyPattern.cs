using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{

    
    public class EnemyPattern : ScriptableObject
    {
        protected EnemyUsedControl usedControl;

        
        internal virtual IEnumerator Action(EnemyUsedControl usedControl)
        {
            this.usedControl = usedControl;
            yield return null;
            
        }
        

        protected GameObject FindChildObject(EnemyUsedControl usedControl, string child_name)
        {
            for(int i =0; i < usedControl.owner.transform.childCount; i++)
            {
                if(usedControl.owner.transform.GetChild(i).gameObject.name == child_name)
                {
                    return usedControl.owner.transform.GetChild(i).gameObject;
                }
            }
            return null;
        }

        protected void PlayAnimation(EnemyUsedControl usedControl, string animationName)
        {
            usedControl.owner.PlayAnimation(animationName);
        }

        protected IEnumerator WaitAnimation(EnemyUsedControl usedControl, string animationName)
        {
            return usedControl.WaitAnimation(animationName);
        }
        protected delegate void WaitAction();

        protected IEnumerator WaitAnimation(EnemyUsedControl usedControl, string animationName,WaitAction waitAction, float breakTime = -1,bool realTime = false)
        {
            usedControl.owner.animationEnd = false;
            PlayAnimation(usedControl, animationName);
            float time = breakTime;
            while (usedControl.owner.animationEnd == false)
            {
                waitAction();
                yield return null;
                time -= realTime? Time.unscaledDeltaTime:Time.deltaTime;
                if(breakTime != -1&&time <= 0)
                {
                    usedControl.owner.animationEnd = true;
                    break;
                }
            }
            yield return new WaitUntil(() => usedControl.owner.animationEnd|| (breakTime != -1 && time <= 0));
        }
    }

    internal class PatternObject : MonoBehaviour
    {
        internal EnemyBase owner;
        float phase = -1;
        private void FixedUpdate()
        {
            if (phase == -1)
            {
                phase = owner.currentPhaseCount;
            }
            if (owner.GetHP() == 0 || owner.GetVisible() == false || phase != owner.currentPhaseCount)
            {
                Destroy(this.gameObject);
            }
        }
        internal void DestroyObject()
        {
            Destroy(this.gameObject);
        }
    }
}