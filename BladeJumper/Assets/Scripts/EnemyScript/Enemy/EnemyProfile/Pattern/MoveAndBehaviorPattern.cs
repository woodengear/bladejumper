using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{

    [CreateAssetMenu(fileName = "Move&Action", menuName = "Enemy/EnemyPattern/Move&Action")]
    public class MoveAndBehaviorPattern : MovePattern
    {
        [SerializeField]
        int actionCount = 3;

        [SerializeField]
        EnemyPattern action;

        [SerializeField]
        EnemyPattern approachAction;
        [SerializeField]
        float approachDistance = 2;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            for(int i = 0; i < actionCount; i++)
            {
                yield return Move(usedControl);
                
                if(approachAction != null &&usedControl.playerDistance <= approachDistance)
                {
                    yield return approachAction.Action(usedControl);
                }
                else
                {
                    yield return action.Action(usedControl);
                }


            }
        }
    }
}