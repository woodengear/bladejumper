using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Buff;

namespace Enemy.Pattern
{
    [System.Serializable]
    internal class BulletStatus
    {
        public EnemyBullet bullet;
        public bool aimShot = false;
        public float bulletSpeed;
        public bool infintySpeed = false;
        public float bulletPower;
        public bool reflective = true;
        public AudioClip fireSound;
        public bool predict = false;
        public BuffData[] buffs;
    }
    [CreateAssetMenu(fileName = "ShotPattern", menuName = "Enemy/EnemyPattern/ShotPattern")]
    public class EnemyShotPattern : AttackPattern
    {
        
        [SerializeField]
        internal BulletStatus bulletStatus;

        protected override void AttackEndAction(EnemyUsedControl usedControl)
        {
            usedControl.owner.EnemyFlip();
            Shot(usedControl);
        }

        public virtual EnemyBullet Shot(EnemyUsedControl usedControl)
        {
            EnemyBullet bullet = Instantiate<EnemyBullet>(bulletStatus.bullet);
            bullet.transform.position = usedControl.attackPivot.transform.position;
            bullet.buffs = bulletStatus.buffs;
            bullet.infinity = bulletStatus.infintySpeed;
            if (bulletStatus.predict)
            {
                bullet.Fire(bulletStatus.bulletSpeed, (bulletStatus.aimShot? usedControl.aimingDir:usedControl.attackDir), bulletStatus.bulletPower, usedControl.owner);
            }
            else
            {
                bullet.Fire(bulletStatus.bulletSpeed, (bulletStatus.aimShot ? usedControl.aimingDir : usedControl.attackDir).normalized, bulletStatus.bulletPower, usedControl.owner);
            }

            bullet.reflectAble = bulletStatus.reflective;
            if (bulletStatus.fireSound != null)
            {
                usedControl.sound.PlaySound(bulletStatus.fireSound);
            }
            return bullet;
        }
        public virtual EnemyBullet Shot(EnemyUsedControl usedControl, Vector2 dir, bool reflectable = true)
        {
            EnemyBullet bullet = Instantiate<EnemyBullet>(bulletStatus.bullet);
            bullet.transform.position = usedControl.attackPivot.transform.position;
            bullet.Fire(bulletStatus.bulletSpeed, dir, bulletStatus.bulletPower, usedControl.owner);
            bullet.reflectAble = bulletStatus.reflective;
            bullet.buffs = bulletStatus.buffs;
            bullet.infinity = bulletStatus.infintySpeed;
            if (bulletStatus.fireSound != null)
            {
                usedControl.sound.PlaySound(bulletStatus.fireSound);
            }
            return bullet;
        }
       
        protected IEnumerator HitCheckShot(EnemyUsedControl usedControl,IEnumerator hitAction = null, IEnumerator missAction = null)
        {
            EnemyBullet bullet = Shot(usedControl);

            while (true)
            {
                if(bullet == null)
                {
                    break;
                }
                yield return null;
            }
            if (BladeJumper.bladeJumper.playerHit)
            {
                yield return hitAction;
            }
            else
            {
                yield return missAction;
            }
        }
    }
}