using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{

    [CreateAssetMenu(fileName = "ThrowObject", menuName = "Enemy/EnemyPattern/ThrowObject")]
    public class ThrowObject : AttackPattern
    {

        [SerializeField]
        ThrownObject throwObject;

        [SerializeField]
        bool signal = false;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            if (signal)
            {
                usedControl.owner.attackSignal = false;
                yield return WaitAnimation(usedControl,attackAnimationName,delegate() 
                { 
                    if(usedControl.owner.attackSignal == true)
                    {
                        AttackEndAction(usedControl);
                        usedControl.owner.attackSignal = false;
                    }
                });
            }
            else
            {
                yield return base.Action(usedControl);
            }
        }

        protected override void AttackEndAction(EnemyUsedControl usedControl)
        {
            ThrownObject bomb = Instantiate<ThrownObject>(throwObject);
            bomb.transform.position = usedControl.attackPivot.transform.position;
            //bullet.SetForce((hit.point - (Vector2)firePosition.transform.position + Vector2.up*3)/(Time.deltaTime*5));
            bomb.SetForce((usedControl.GetPlayerDir(usedControl.attackPivot.transform.position) + Vector2.up * 1.5f));
        }

    }
}