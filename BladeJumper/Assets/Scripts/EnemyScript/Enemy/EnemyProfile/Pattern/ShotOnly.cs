using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    

    [CreateAssetMenu(fileName = "ShotOnly", menuName = "Enemy/EnemyPattern/Basic/ShotOnly")]
    public class ShotOnly : EnemyShotPattern
    {
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            Shot(usedControl);
            yield return null;
        }
    }
    
}