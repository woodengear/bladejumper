using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "AttackPattern", menuName = "Enemy/EnemyPattern/AttackPattern")]
    public class AttackPattern : EnemyPattern
    {
        [SerializeField]
        protected string attackAnimationName = "Attack";
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            this.usedControl = usedControl;
            yield return usedControl.WaitAnimation(attackAnimationName);
            AttackEndAction(usedControl);
        }
        protected virtual void AttackEndAction(EnemyUsedControl usedControl)
        {

        }
    }
}