using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    
    [CreateAssetMenu(fileName = "Slash", menuName = "Enemy/EnemyPattern/Slash")]
    public class SlashPattern : AreaAttackPattern
    {
        

        [System.Serializable]
        class SlashRequire
        {
            [Header("Animation State Name")]
            public string slashReady_Animation = "Slash_Ready";
            public string slashAction_Animation = "Slash_Action";
            [Space]
            [Header("GameObject Name")]
            public string attackAreaName = "SlashArea";
            [Space]
            [Header("Effects")]
            [Tooltip("Slash, SlashHit")]
            public EffectGenerateControl effects;
        }
        [SerializeField]
        bool fast = false;
        [SerializeField]
        bool signal = false;
        [SerializeField]
        SlashRequire slashRequire;

        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            if(fast == false&&slashRequire.slashReady_Animation != "")
                yield return usedControl.WaitAnimation(slashRequire.slashReady_Animation);

            if(signal == true)
            {
                BoxCollider2D area = FindArea(usedControl, slashRequire.attackAreaName);
                bool hit = false;
                usedControl.owner.attackSignal = false;
                bool flipCheck = false;
                yield return WaitAnimation(usedControl, slashRequire.slashAction_Animation, () => 
                {
                    if (usedControl.owner.attackSignal)
                    {
                        usedControl.owner.EnemyFlip();
                        area.transform.localScale = new Vector3(usedControl.sRenderer.flipX ? -1 : 1, 1, 1);
                        if (area.bounds.Contains(usedControl.playerCenter))
                        {
                            if(slashRequire.effects != null )
                            {
                                slashRequire.effects.GenerateEffect("SlashHit", usedControl.playerCenter);
                            }
                            hit = true;
                            BladeJumper.bladeJumper.Damaged(1, default, Vector2.up * power.y + Vector2.right * (usedControl.sRenderer.flipX ? -1 : 1) * power.x, usedControl.owner.gameObject);
                            usedControl.owner.attackSignal = false;
                        }
                    }
                });
                if (hit&&hitNext !=null)
                {
                    yield return hitNext.Action(usedControl);
                }
            }
            else
            {
                yield return AreaAttack(usedControl, slashRequire.attackAreaName, slashRequire.slashAction_Animation);
            }


            
            //yield return new WaitForSeconds(1f);
        }
    }
}