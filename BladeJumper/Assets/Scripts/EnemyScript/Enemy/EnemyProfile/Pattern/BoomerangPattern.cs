using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "Boomerang", menuName = "Enemy/EnemyPattern/Boomerang")]

    public class BoomerangPattern : AttackPattern
    {
        [SerializeField]
        bool returnWait = true;
        [SerializeField]
        protected BoomerangStatus boomerangStatus;


        // Start is called before the first frame update
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            boomerangStatus.startPosition = usedControl.attackPivot.position;
            boomerangStatus.targetPosition = usedControl.playerCenter;


            yield return usedControl.WaitAnimation(attackAnimationName);
            BoomerangObject boomerang = Instantiate<BoomerangObject>(boomerangStatus.boomerang);
            boomerang.transform.position = usedControl.attackPivot.position;
            boomerang.StartBoomerang(boomerangStatus);
            if(returnWait)
                yield return new WaitUntil(() => boomerang == null||boomerang.endBoomerang);
        }
    }
}