using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "JumpSlash", menuName = "Enemy/EnemyPattern/JumpSlash")]
    public class JumpSlash : AttackPattern
    {

        [Header("JumpSlash")]
        [SerializeField]
        string jumpAnimation = "Jump";
        [SerializeField]
        string landingAnimation = "Landing";
        [SerializeField]
        EffectGenerateControl effects;

        [SerializeField]
        string slashArea = "AttackArea";
        [SerializeField]
        Vector2 power = new Vector2(10,7);
        // Start is called before the first frame update
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            usedControl.owner.non_target = true;
            Vector2 _pos = usedControl.owner.transform.position;
            yield return WaitAnimation(usedControl, jumpAnimation);
            yield return new WaitForSeconds(.5f);
            yield return new WaitUntil(() => BladeJumper.bladeJumper.GetComponent<CharacterCollider>().colBox.below);
            usedControl.owner.transform.position = BladeJumper.bladeJumper.transform.position;
            usedControl.owner.attackSignal = false;
            BoxCollider2D area = FindChildObject(usedControl, slashArea).GetComponent<BoxCollider2D>();
            bool effectGen = false;
            yield return WaitAnimation(usedControl, attackAnimationName, delegate () 
            {
                if(usedControl.owner.attackSignal == true)
                {
                    if(effects != null&&effectGen == false)
                    {
                        effects.GenerateEffect("JumpSlash", usedControl.center, usedControl.sRenderer.flipX);
                        effectGen = true;
                        
                    }
                    if (area.bounds.Contains(usedControl.playerCenter))
                    {
                        if(effects != null)
                        {
                            effects.GenerateEffect("SlashHit", usedControl.playerCenter);
                        }
                        BladeJumper.bladeJumper.Damaged(1, default, new Vector2(usedControl.sRenderer.flipX?power.x:-power.x,power.y));
                        usedControl.owner.attackSignal = false;
                    }
                    
                }
            
            });
            yield return WaitAnimation(usedControl, landingAnimation);
            yield return Pattern.ForceMove.Move(usedControl, "MoveAction", _pos, 40);
            usedControl.owner.non_target = false;
        }
    }
}