using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "ForceMove", menuName = "Enemy/EnemyPattern/Basic/ForceMove")]
    public class ForceMove : EnemyPattern
    {
        [SerializeField]
        Vector2Position[] movePoints;
        [SerializeField]
        float speed;

        [SerializeField]
        string moveReadyAnimation = "Move";
        [SerializeField]
        string moveAnimation = "Move";

        int lastIndex = -1;

        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
           
            Debug.Log("force");
            int ran = Random.Range(0, movePoints.Length-1);
            lastIndex = ran < lastIndex ? ran : ran + 1;
            usedControl.sRenderer.flipX = usedControl.center.x > movePoints[lastIndex].position.x;
            if (moveReadyAnimation != "")
            {
                yield return WaitAnimation(usedControl, moveReadyAnimation);
            }
            yield return Move(usedControl,moveAnimation ,movePoints[lastIndex].position,speed);
        }
        internal IEnumerator Move(EnemyUsedControl usedControl, int index, System.Action<float, float> moveAction = null)
        {
            if (moveReadyAnimation != "")
            {
                yield return WaitAnimation(usedControl, moveReadyAnimation);
            }
            yield return Move(usedControl, moveAnimation, movePoints[index].position, speed, moveAction);
        }
        internal static IEnumerator Move(EnemyUsedControl usedControl,string animName, Vector2 pos, float speed, System.Action<float,float> moveAction = null)
        {
            AfterEffect aEff = usedControl.owner.GetComponent<AfterEffect>();
            if (aEff)
            {
                aEff.active = true;
            }
            usedControl.sRenderer.flipX = usedControl.center.x > pos.x;
            Debug.Log("Move");
            usedControl.owner.PlayAnimation(animName);
            float _timeMax = Vector2.Distance(usedControl.owner.transform.position, pos) /speed;
            float _time = _timeMax;
            Vector2 dir = (pos - (Vector2)usedControl.owner.transform.position).normalized;
            while (_time > 0)
            {
                yield return null;
                if(moveAction != null)
                    moveAction(_time, _timeMax);
                usedControl.owner.transform.Translate(dir * Time.deltaTime * speed, Space.World);
                _time -= Time.deltaTime;
            }
            usedControl.owner.transform.position = pos;

            if (aEff)
            {
                aEff.active = false;
            }
        }
    }
}