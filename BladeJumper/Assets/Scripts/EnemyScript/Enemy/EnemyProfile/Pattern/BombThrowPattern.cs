using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "ThrowBomb", menuName = "Enemy/EnemyPattern/ThrowBomb")]
    public class BombThrowPattern : AttackPattern
    {
        [SerializeField]
        EnemyBomb bombPrefab;
        [SerializeField]
        float bombTime = 2;
        [SerializeField]
        float radius = 2;
        [SerializeField]
        int bombCount = 1;


        [SerializeField]
        bool signal = false;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            if (signal)
            {
                usedControl.owner.attackSignal = false;
                yield return WaitAnimation(usedControl, attackAnimationName, delegate ()
                {
                    if (usedControl.owner.attackSignal == true)
                    {
                        AttackEndAction(usedControl);
                        usedControl.owner.attackSignal = false;
                    }
                });
            }
            else
            {
                yield return base.Action(usedControl);
            }
        }
        internal IEnumerator Action(EnemyUsedControl usedControl,System.Action<EnemyBomb> bombAction)
        {
            yield return null;
        }
        protected override void AttackEndAction(EnemyUsedControl usedControl)
        {
            BombThrow(usedControl);
        }
        internal EnemyBomb BombThrow(EnemyUsedControl usedControl, Vector2 force)
        {
            EnemyBomb bomb = Instantiate<EnemyBomb>(bombPrefab);
            bomb.transform.position = usedControl.attackPivot.transform.position;
            bomb.radius = radius;
            bomb.SetForce(force);
            return bomb;
        }
        internal void BombThrow(EnemyUsedControl usedControl, System.Action<EnemyBomb> bombAction = null)
        {
            for (int i = 0; i < bombCount; i++)
            {
                EnemyBomb bomb = Instantiate<EnemyBomb>(bombPrefab);
                bomb.transform.position = usedControl.attackPivot.transform.position;
                bomb.radius = radius;
                //bullet.SetForce((hit.point - (Vector2)firePosition.transform.position + Vector2.up*3)/(Time.deltaTime*5));
                bomb.SetForce((usedControl.GetPlayerDir(usedControl.attackPivot.transform.position) + Vector2.up * 1.5f * (1 + i)));
                bomb.bombTime = bombTime + .2f * i;
                if(bombAction != null)
                {
                    bombAction(bomb);
                }
            }
        }
    }
}