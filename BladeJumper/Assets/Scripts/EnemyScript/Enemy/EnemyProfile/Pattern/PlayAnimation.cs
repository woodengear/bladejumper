using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "PlayAnimation", menuName = "Enemy/EnemyPattern/Basic/PlayAnimation")]
    public class PlayAnimation : EnemyPattern
    {
        [SerializeField]
        string animation;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            return usedControl.WaitAnimation(animation);
        }
        static internal IEnumerator Action(EnemyUsedControl usedControl, string animName)
        {
            return usedControl.WaitAnimation(animName);
        }
    }

}
