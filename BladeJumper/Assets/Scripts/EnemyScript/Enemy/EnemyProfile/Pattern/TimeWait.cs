using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{

    [CreateAssetMenu(fileName = "TimeWait", menuName = "Enemy/EnemyPattern/Basic/TimeWait")]
    public class TimeWait : EnemyPattern
    {
        public float time;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            yield return new WaitForSeconds(time);
        }
    }
}