using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "Josh", menuName = "Enemy/EnemyPattern/Special/Josh")]
    public class JoshSpecialAction : EnemyPattern
    {

        public ForceMove moveAction;
        public int moveBombCount = 3;
        public float waitTime = 1;
        public Vector2Position centerPosition;
        public Vector2Position landingPosition;
        public BombThrowPattern bombAction;
        

        public EffectGenerateControl effects;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            Vector2 lastPos = usedControl.owner.transform.position;
            yield return moveAction.Move(usedControl, 0);


            int bombCheck = moveBombCount;
            List<EnemyBomb> bombs = new List<EnemyBomb>();
            yield return moveAction.Move(usedControl, 1, (_t, _tM) =>
            {
                if (_t / _tM < bombCheck / (moveBombCount + 1f))
                {

                    EnemyBomb bomb = bombAction.BombThrow(usedControl, new Vector2(0, 4));
                    bomb.GetComponent<Collider2D>().isTrigger = true;
                    
                    bombs.Add(bomb);

                    bombCheck -= 1;
                }
            });

            yield return WaitAnimation(usedControl, "Jump");
            usedControl.owner.transform.position = landingPosition.position;
            yield return new WaitForSeconds(waitTime);
            usedControl.owner.attackSignal = false;
            TimeManager.SetTimeScale(.5f);
            bombs.ForEach((b) => { if (b != null) b.ActiveArea(); });
            Vector2 vel = new Vector2();
            bool jumpEnd = false;
            yield return WaitAnimation(usedControl, "JumpSlashSP", () =>
             {
                 if(jumpEnd == false)
                    usedControl.owner.transform.position = Vector2.SmoothDamp(usedControl.owner.transform.position, centerPosition.position, ref vel, .1f);
                 if(jumpEnd == false && usedControl.owner.attackSignal)
                 {
                     TimeManager.ResetTimeScale(.5f);
                     TimeManager.instance.StartBulletTime(0.1f, .2f);
                     GameObject spe = effects.GenerateEffect("SPEffect", centerPosition.position, usedControl.sRenderer.flipX);
                     spe.GetComponent<Animator>().updateMode = AnimatorUpdateMode.UnscaledTime;
                     spe.GetComponent<SpriteRenderer>().sortingLayerName = "WE";
                     usedControl.owner.attackSignal = false;
                     jumpEnd = true;
                 }
                 if (usedControl.owner.attackSignal)
                 {

                     bombs.ForEach((b) => { if (b != null) b.Damaged(1); });
                     effects.GenerateEffect("JumpSlashSP", centerPosition.position, usedControl.sRenderer.flipX);
 
                   
                 }

             });
            usedControl.owner.transform.position = landingPosition.position;
            yield return WaitAnimation(usedControl, "Landing");
        }
    }

}
