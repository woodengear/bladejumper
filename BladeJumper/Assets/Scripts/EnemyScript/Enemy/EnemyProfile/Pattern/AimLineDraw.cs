using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "AimDraw", menuName = "Enemy/EnemyPattern/AimDraw")]
    public class AimLineDraw : EnemyPattern
    {
        enum DrawType {AttackPivot, Center, AttackPivot2,Center2}
        [SerializeField]
        DrawType drawType = DrawType.AttackPivot;
        [SerializeField]
        LineRenderer lineR;
        [SerializeField]
        LayerMask blockLayer;
        [SerializeField]
        float drawTime;
        [SerializeField]
        bool follow = true;
        [SerializeField]
        float smoothTime = 1;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            LineRenderer nlr = Instantiate<LineRenderer>(lineR);
            float time = drawTime;
            List<Vector3> vector3s = new List<Vector3>();
            nlr.gameObject.AddComponent<AimLine>().owner = usedControl.owner;
            Vector3 targetPos = usedControl.playerCenter;
            Vector3 vel = Vector3.zero;

            while (true)
            {
                if (follow || time == drawTime)
                {
                    usedControl.owner.EnemyFlip();
                    targetPos = Vector3.SmoothDamp(targetPos, usedControl.playerCenter, ref vel, smoothTime);
                    vector3s.Clear();
                    switch (drawType)
                    {
                        case DrawType.AttackPivot:
                        case DrawType.AttackPivot2:
                            vector3s.Add(usedControl.attackPivot.transform.position);
                            break;
                        case DrawType.Center:
                        case DrawType.Center2:
                            vector3s.Add(usedControl.center);
                            break;
                    }

                    RaycastHit2D hit = Physics2D.Raycast(vector3s[0], targetPos - vector3s[0], float.PositiveInfinity, blockLayer);
                    if (hit)
                    {
                        vector3s.Add(hit.point);
                    }
                    else
                    {
                        vector3s.Add(vector3s[0] + (targetPos - vector3s[0]) * 100);
                    }
                    usedControl.aimingDir = targetPos - vector3s[0];
                    switch (drawType)
                    {
                        case DrawType.AttackPivot2:
                        case DrawType.Center2:

                            nlr.positionCount = 3;
                            hit = Physics2D.Raycast(vector3s[0], -(targetPos - vector3s[0]), float.PositiveInfinity, blockLayer);
                            if (hit)
                            {
                                vector3s.Insert(0, hit.point);
                            }
                            else
                            {
                                vector3s.Insert(0, vector3s[0] + -(targetPos - vector3s[0]) * 100);
                            }

                            break;
                    }

                    nlr.SetPositions(vector3s.ToArray());
                }
                yield return null;
                time -= Time.deltaTime;
                if (time <= 0)
                {
                    break;
                }
            }
            
            nlr.gameObject.GetComponent<AimLine>().DestroyObject();
        }

        class AimLine : MonoBehaviour
        {
            internal EnemyBase owner;
            float phase = -1;
            private void FixedUpdate()
            {
                if(phase == -1)
                {
                    phase = owner.currentPhaseCount;
                }
                if(owner.GetHP() == 0||owner.GetVisible() == false||phase != owner.currentPhaseCount)
                {
                    Destroy(this.gameObject);
                }
            }
            internal void DestroyObject()
            {
                Destroy(this.gameObject);
            }
        }

    }
}