using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "SlothMissile", menuName = "Enemy/EnemyPattern/SlothMissile")]
    public class SlothMissile : EnemyPattern
    {
        /* 나무빠름보 스크립트
     * 
     * 루틴
     *      캐릭터가 공격 영역에 접근
     *      돌격 공격
     *      착지
     *      되돌아감
     * 
     * 
     * 애니메이션 종류
     *      Idle
     *      AttackReady     돌격 준비
     *      Attack          날라가는 모션
     *      AttackDelay     착지
     *      BackToIdle      되돌아감
     * 
     * 
     * 구현 기능
     *      Player 탐지
     *      Player-SSC 사이의 장애물(Wall, Bottom) 확인 후 없으면 발사
     *      발사할때, 발사 방향으로 착지지점 확인
     *      
     * 
     * 
     *
     *      
     * 
     * 
     * 
     */

        /* 변수설명
         * float missileSpeed       날라가는 속도
         * float delayTime          날라가기 종료부터 착지까지의 시간
         */
        [SerializeField]
        float missileSpeed = 35;
        [SerializeField]
        float missilePower = 5;

        [SerializeField]
        float flyTime = .5f;
        [SerializeField]
        float delayTime = .5f;

        [System.Serializable]
        class EnemyAnimation
        {
            public string idle = "Idle";
            public string attackReady = "AttackReady";
            public string backToIdle = "BackToIdle";
            
        }
        [System.Serializable]
        class MissileAnimation
        {
            public string missileEndIdle = "SpeedSlothMissile_End_Idle";
            public string missileEnd =  "SpeedSlothMissile_End";
            public string missileEnd2 = "SpeedSlothMissile_End2";
            public string missileEnd_Back = "SpeedSlothMissile_End_Back";
        }
        

        [SerializeField]
        GameObject missilePrefab;

        [SerializeField]
        EnemyAnimation enemyAnimation;
        [SerializeField]
        MissileAnimation missileAnimation;

        [SerializeField]
        EffectGenerateControl effects;
        [SerializeField]
        string hitEffectName = "Hit";

        Bounds bounds;

        Vector2 endPosition;

        
        public class SlothMissileBehavior : MonoBehaviour
        {
            internal EnemyBase owner;
            private void FixedUpdate()
            {
                if(owner.GetVisible() == false)
                {
                    Destroy(this.gameObject);
                }
            }
        }

        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            
            Vector2 dir = (usedControl.playerDir).normalized;
            WaitForSeconds wfs = new WaitForSeconds(Time.deltaTime);

            yield return usedControl.WaitAnimation(enemyAnimation.attackReady);
            usedControl.owner.SetColorAlpha(0);
            usedControl.owner.non_target = true;
            //Missile Routine Start
            GameObject currentMissile = Instantiate<GameObject>(missilePrefab);
            currentMissile.AddComponent<SlothMissileBehavior>().owner = usedControl.owner;
            bounds = usedControl.collider.bounds;
            Animator mAnim = currentMissile.GetComponent<Animator>();
            currentMissile.transform.position = bounds.center;
            BoxCollider2D mBox = currentMissile.GetComponent<BoxCollider2D>();
            Vector2 mPivot = mBox.bounds.center - currentMissile.transform.position;
            Vector2 size = mBox.bounds.size;

            //Bounds mBounds = missile.GetComponent<BoxCollider2D>().bounds;

            if (usedControl.sRenderer.flipX)
            {
                currentMissile.GetComponent<SpriteRenderer>().flipX = true;

            }
            currentMissile.transform.Rotate(new Vector3(0, 0, Vector2.SignedAngle(Vector2.right, dir * (usedControl.sRenderer.flipX ? -1 : 1))));
            Collider2D standCol = Physics2D.Raycast(usedControl.owner.transform.position, Vector2.down, 1, usedControl.owner.bottomLayer).collider;
            //---Missile Fly Routine Start
            for (float mTime = 0; mTime < flyTime; mTime += Time.fixedDeltaTime)
            {
                //mBounds = missile.GetComponent<BoxCollider2D>().bounds;
                RaycastHit2D mHit = Physics2D.BoxCast((Vector2)currentMissile.transform.position + mPivot, size, 0, dir, missileSpeed * Time.fixedDeltaTime, usedControl.owner.playerLayer | usedControl.owner.bottomLayer);
                if (mHit)
                {
                    if (mHit.collider == BladeJumper.bladeJumper.GetComponent<BoxCollider2D>())
                    {
                        if(effects!=null && effects.Contains(hitEffectName))
                        {
                            GameObject eff = Instantiate<GameObject>(effects.GetEffect(hitEffectName));
                            eff.transform.position = mHit.point;
                        }
                        BladeJumper.bladeJumper.Damaged(1, dir, Vector2.up * 6 + Vector2.right * Mathf.Sign(dir.x) * missilePower);
                    }
                    else
                    {
                        if ((standCol == mHit.collider))
                        {


                            currentMissile.transform.position = (Vector2)currentMissile.transform.position + dir * missileSpeed * Time.fixedDeltaTime;
                            //missile.transform.Translate(Vector2.right * missileSpeed * Time.fixedDeltaTime);
                            yield return wfs;
                            continue;
                        }
                    }

                    Debug.DrawLine((Vector2)currentMissile.transform.position + mPivot + Vector2.up * .1f, mHit.point, Color.green);
                    Debug.DrawLine((Vector2)currentMissile.transform.position + mPivot + Vector2.up * .2f, mHit.centroid, Color.red);
                    Debug.DrawLine((Vector2)currentMissile.transform.position + mPivot, mHit.centroid - mPivot, Color.blue);
                    currentMissile.transform.rotation = new Quaternion();
                    currentMissile.transform.position = (Vector2)currentMissile.transform.position + dir * (Vector2.Distance((Vector2)currentMissile.transform.position + mPivot, mHit.centroid) - 0.05f);
                    //missile.transform.Translate(dir * Vector2.Distance(mHit.centroid-mPivot, (Vector2)missile.transform.position + mPivot));
                    //missile.transform.position = mHit.centroid-mPivot ;
                    break;
                }
                else
                {
                    currentMissile.transform.position = (Vector2)currentMissile.transform.position + dir * missileSpeed * Time.fixedDeltaTime;
                    //missile.transform.Translate(Vector2.right * (sRenderer.flipX ? -1 : 1) * missileSpeed * Time.fixedDeltaTime);
                }

                yield return wfs;
            }
            //---Missile Fly Routine End

            //---Missile End Routine Start

            dir.x *= -1;
            yield return wfs;
            Vector2 vel = dir * missilePower / 1.5f + Vector2.up * 20;
            currentMissile.GetComponent<SpriteRenderer>().flipX = !currentMissile.GetComponent<SpriteRenderer>().flipX;
            mAnim.Play(missileAnimation.missileEnd);
            currentMissile.transform.rotation = new Quaternion();

            bool bottom = false;
            for (float mTime = 0; mTime < delayTime; mTime += Time.fixedDeltaTime)
            {
                //mBounds = missile.GetComponent<BoxCollider2D>().bounds;
                vel.y -= 60 * Time.fixedDeltaTime;
                if (bottom == false)
                {
                    RaycastHit2D hitY = Physics2D.BoxCast(mBox.bounds.center, mBox.bounds.size, 0, Vector2.up * vel.y, Mathf.Abs(vel.y) * Time.fixedDeltaTime, usedControl.owner.bottomLayer);
                    if (hitY)
                    {

                        //missile.transform.Translate(Vector2.up * Mathf.Sign(vel.y) * hitY.distance * Time.fixedDeltaTime);

                        vel.y = 0;
                        if (hitY.normal.y == 1)
                        {
                            break;
                        }
                    }
                    else
                    {
                        currentMissile.transform.Translate(Vector2.up * vel.y * Time.fixedDeltaTime);
                    }
                }
                //mBounds = missile.GetComponent<BoxCollider2D>().bounds;
                if (vel.x != 0)
                {
                    RaycastHit2D hitX = Physics2D.BoxCast(mBox.bounds.center, size, 0, Vector2.right * vel.x, Mathf.Abs(vel.x) * Time.fixedDeltaTime, usedControl.owner.bottomLayer);
                    if (hitX)
                    {

                        vel.x = 0;
                    }
                    else
                    {
                        currentMissile.transform.Translate(Vector2.right * vel.x * Time.fixedDeltaTime);

                    }
                }



                //missile.transform.Translate(vel* Time.fixedDeltaTime);
                yield return wfs;
            }
            //yield return new WaitForSeconds(.1f);
            RaycastHit2D endHit = Physics2D.BoxCast(mBox.bounds.center, mBox.bounds.size, 0, Vector2.down, 1f, usedControl.owner.bottomLayer);
            if (endHit)
            {
                currentMissile.GetComponent<SpriteRenderer>().flipX = !currentMissile.GetComponent<SpriteRenderer>().flipX;
                currentMissile.transform.position = endHit.point;
                mAnim.Play(missileAnimation.missileEnd2);
                yield return wfs;
                mAnim.Play(missileAnimation.missileEndIdle);
                yield return new WaitForSeconds(.2f);
                mAnim.Play(missileAnimation.missileEnd_Back);
                yield return new WaitForSeconds(.2f);
            }


            //---Missile End Routine End


            Destroy(currentMissile);
            yield return null;
            usedControl.owner.SetColorAlpha(1);
            usedControl.owner.non_target = false;
            yield return usedControl.WaitAnimation(enemyAnimation.backToIdle);
            //Missile Routine End

            usedControl.animator.Play(enemyAnimation.idle);

        }
    }
}
