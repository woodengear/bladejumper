using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Enemy.Pattern
{

    [CreateAssetMenu(fileName = "Move", menuName = "Enemy/EnemyPattern/Move")]
    public class MovePattern : EnemyPattern
    {
        [SerializeField]
        protected Vector2Position[] movePoint;

        [SerializeField]
        protected string moveAnimation = "Move";

        [SerializeField]
        GameObject moveEffect;

        protected int currentIndex = 0;

        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            yield return Move(usedControl);
        }

        protected IEnumerator Move(EnemyUsedControl usedControl)
        {
            yield return usedControl.WaitAnimation(moveAnimation, true);
            int index = Random.Range(0, movePoint.Length - 1);
            if (index < currentIndex)
            {
                usedControl.owner.transform.position = movePoint[index].position;
                currentIndex = index;
            }
            else
            {
                usedControl.owner.transform.position = movePoint[index + 1].position;
                currentIndex = index + 1;
            }
            usedControl.owner.EnemyFlip();
        }

        static internal IEnumerator Action(EnemyUsedControl usedControl, Vector2 position, GameObject moveEffect)
        {


            yield return null;
        }

    }
}