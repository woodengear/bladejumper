using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "LineSlash", menuName = "Enemy/EnemyPattern/LineSlash")]
    public class LineSlash : EnemyPattern
    {
        [SerializeField]
        LineRenderer lineR;
        [System.Serializable]
        class LineSlashAnimation
        {
            public string ready = "Slash_Ready";
            public string idle = "Slash_Idle";
            public string slash = "Slash_Action";
        }
        [SerializeField]
        LineSlashAnimation animaion;

        [ColorUsage(true,true)]
        [SerializeField]
        Color lineColor;
        [ColorUsage(true, true)]
        [SerializeField]
        Color lineEndColr;

        [SerializeField]
        float waitTime = 1;
        [SerializeField]
        float drawInterval = .5f;
        [SerializeField]
        float slashInterval = .3f;
        [SerializeField]
        int lineNumber = 1;

        [SerializeField]
        Vector2 power;
        [SerializeField]
        float damageTime= .1f;

        [SerializeField]
        EffectGenerateControl effects;
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {

            yield return WaitAnimation(usedControl, animaion.ready);



            List<(LineRenderer, Vector3[])> slashLines = new List<(LineRenderer, Vector3[])>();
            
            for (int i = 0; i < lineNumber; i++)
            {
                LineRenderer nlr = Instantiate<LineRenderer>(lineR);
                nlr.startColor = lineColor;
                nlr.endColor = lineColor;
                nlr.gameObject.AddComponent<PatternObject>().owner = usedControl.owner;
                List<Vector3> points = new List<Vector3>();

                Vector2 drawDir = new Vector2(Random.Range(-1f, 1), Random.Range(-1f, 1));
                Vector2 pos = usedControl.playerCenter + Vector2.right * Random.Range(-5, 5f); ;
                points.Add(pos - drawDir * 100);
                points.Add(pos + drawDir * 100);
                nlr.SetPositions(points.ToArray());

                slashLines.Add((nlr, points.ToArray()));
                yield return new WaitForSecondsRealtime(drawInterval);
            }
            

            yield return new WaitForSeconds(waitTime);



            Vector2 dir;

            //usedControl.owner.PlayAnimation(animaion.slash);



            usedControl.animator.updateMode = AnimatorUpdateMode.UnscaledTime;
            TimeManager.SetTimeScale(.01f);

            GameObject effect = Instantiate<GameObject>(effects.GetEffect("Parrying"));
            effect.transform.position = usedControl.center;
            effect.transform.localScale *= 2;
            yield return new WaitForSecondsRealtime(.2f);
            for (int i = 0; i < slashLines.Count; i++)
            {
                bool hitPlayer = false;
                yield return WaitAnimation(usedControl, animaion.slash, delegate ()
                {
                    if (hitPlayer==false)
                    {
                        slashLines[i].Item1.startColor = lineEndColr;
                        slashLines[i].Item1.endColor = lineEndColr;
                        slashLines[i].Item1.startWidth = .5f;
                        slashLines[i].Item1.endWidth = .5f;
                        RaycastHit2D hit = Physics2D.Linecast(slashLines[i].Item2[0], slashLines[i].Item2[1], 1 << BladeJumper.bladeJumper.gameObject.layer);
                        dir = slashLines[i].Item2[1] - slashLines[i].Item2[0];
                        if (hit)
                        {
                            BladeJumper.bladeJumper.Damaged(1, dir, new Vector2(Mathf.Sign(dir.x) * power.x, power.y));
                            GameObject effect = Instantiate<GameObject>(effects.GetEffect("Hit"));
                            effect.transform.position = hit.point;
                            effect.GetComponent<Animator>().updateMode = AnimatorUpdateMode.UnscaledTime;
                            hitPlayer = true;
                        }
                    }
                }, drawInterval,true);
                //yield return WaitAnimation(usedControl, animaion.slash);
                Destroy(slashLines[i].Item1.gameObject);
                //yield return new WaitForSeconds(slashInterval);
            }
            TimeManager.SetTimeScale(100f);
            usedControl.animator.updateMode = AnimatorUpdateMode.Normal;
        }
    }
}