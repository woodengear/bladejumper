using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "BoomerangOnly", menuName = "Enemy/EnemyPattern/Basic/BoomerangOnly")]
    public class BoomerangOnly : EnemyPattern
    {
        [SerializeField]
        bool returnWait = true;
        [SerializeField]
        BoomerangStatus boomerangStatus;
        // Start is called before the first frame update
        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            boomerangStatus.startPosition = usedControl.attackPivot.position;
            boomerangStatus.targetPosition = usedControl.playerCenter;
            BoomerangObject boomerang = Instantiate<BoomerangObject>(boomerangStatus.boomerang);
            boomerang.transform.position = usedControl.attackPivot.position;
            boomerang.StartBoomerang(boomerangStatus);
            if (returnWait)
            {
                yield return new WaitUntil(() => boomerang == null || boomerang.endBoomerang);
            }
            yield return null;
        }
    }
}