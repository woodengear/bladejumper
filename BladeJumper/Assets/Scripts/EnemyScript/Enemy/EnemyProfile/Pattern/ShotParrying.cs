using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{

    [CreateAssetMenu(fileName = "ShotParrying", menuName = "Enemy/EnemyPattern/ShotParrying")]
    public class ShotParrying : EnemyShotPattern
    {

        [SerializeField]
        float parryingDistance = 4;
        [System.Serializable]
        class EnemyAnimation
        {
            public string shot = "Attack";
            public string slash = "Slash_Action";
            public string slashReady = "Slash_Ready";
        }

        [SerializeField]
        EnemyAnimation enemyAnimation;

        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            yield return usedControl.WaitAnimation(enemyAnimation.shot);
            EnemyBullet bullet = Shot(usedControl);
            PlayAnimation(usedControl,enemyAnimation.slashReady);

            yield return new WaitUntil(() => (bullet == null||(bullet.reflectCount == 1&&Vector2.Distance(usedControl.center, (Vector2)bullet.transform.position) < parryingDistance)));

            if(bullet == null)
            {

            }
            else
            {
                bullet.Damaged(1, default, default, usedControl.owner.gameObject);
                bullet.SetSpeed(bulletStatus.bulletSpeed);
                yield return usedControl.WaitAnimation(enemyAnimation.slash);
                
            }
        }

    }
}