using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "Combo", menuName = "Enemy/EnemyPattern/Combo")]
    public class PatternCombo : EnemyPattern
    {
        [SerializeField]
        EnemyPattern[] patterns;
        // Start is called before the first frame update


        internal override IEnumerator Action(EnemyUsedControl usedControl)
        {
            for(int i = 0; i < patterns.Length; i++)
            {

                yield return patterns[i].Action(usedControl);
                
            }
            
        }

    }
}

