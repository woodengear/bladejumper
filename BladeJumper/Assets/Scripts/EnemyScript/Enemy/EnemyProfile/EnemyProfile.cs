using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enemy;

namespace Enemy.Pattern
{
    [CreateAssetMenu(fileName = "Profile", menuName = "Enemy/Profile")]
    public class EnemyProfile : ScriptableObject
    {
        [SerializeField]
        internal int hp = 1;
        [SerializeField]
        internal int interval = 3;
        [SerializeField]
        internal float aproachDistance = 1;


        [SerializeField]
        internal EnemyPattern[] patterns;

        [SerializeField]
        internal EnemyPattern[] approachPatterns;
        internal virtual IEnumerator Attack(EnemyUsedControl usedControl)
        {
            if(patterns!= null &&patterns.Length > 0)
                yield return patterns[Random.Range(0,patterns.Length)].Action(usedControl);
        }
        internal virtual IEnumerator ApproachAttack(EnemyUsedControl usedControl)
        {
            if (approachPatterns != null && approachPatterns.Length > 0)
                yield return approachPatterns[Random.Range(0, approachPatterns.Length)].Action(usedControl);
        }

    }
}