using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Enemy
{

    public class EnemyUsedControl
    {
        public SoundFXControl sound;
        public SpriteRenderer sRenderer;
        public Animator animator;
        public BoxCollider2D collider;
        public BoxCollider2D playerCollider;
        public Transform attackPivot;
        internal EnemyBase owner;
       
        /// <summary>
        /// enemyObject의 bounds.center
        /// </summary>
        public Vector2 center { get => collider.bounds.center; }
        public Vector2 playerCenter { get => playerCollider.bounds.center; }
        public Vector2 playerDir { get => playerCollider.bounds.center - collider.bounds.center; }
        public Vector2 attackDir { get => playerCollider.bounds.center - attackPivot.position; }
        public float playerDistance { get => playerDir.magnitude; }

        public Vector2 aimingDir = new Vector2();

        public IEnumerator WaitAnimation(string AnimName, bool oneShot = true)
        {
            yield return owner.WaitAnimation(AnimName, oneShot);
        }

        public Vector2 GetPlayerDir(Vector2 pivot)
        {
            return playerCenter - pivot;
        }
        public float GetPlayerDistance(Vector2 pivot)
        {
            return GetPlayerDir(pivot).magnitude;
        }

        internal void SetPlayerDirFix()
        {

        }
    }


}

