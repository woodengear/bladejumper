using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enemy.Pattern;
using Buff;

namespace Enemy
{
    public interface IEnemyObject
    {
        public Bounds GetBounds();
        public int GetHP();
        public bool GetVisible();
    }
    public enum EnemyAnimation {Idle, Hit, Death, Attack}

    public delegate IEnumerator EnemyBehaviour();

    [RequireComponent(typeof(BoxCollider2D))]
    public class EnemyBase : MonoBehaviour, IDamagedObject,IResetObject,IEnemyObject, IBuffUsed
    {
        /*
         * Enemy 들의 기본 코드
         * 
         * 기능
         *      지정 범위 안의 Player 탐지
         *      직선상에 존재하는지 탐지
         *      애니메이션 재생
         *      사망 처리
         *      동장에 해당하는 애니메이션 설정
         *      페이즈 설정
         *          입력된 페이즈 복사
         * 설정
         *      
         *
         * 
         */

        static GameObject detectGroup;
        
        

        [System.Serializable]
        internal struct EnemyStat
        {
            public int hp;
            public float attackInterval;
        }
        
        bool visible;
        internal bool animationEnd = false;
        protected WaitUntil animationWait;

        [Header("  Enemy Base")]
        [SerializeField]
        internal LayerMask playerLayer;
        [SerializeField]
        internal LayerMask bottomLayer;
        [SerializeField]
        bool infinityHP;
        [SerializeField]
        bool dataSave = false;
        [SerializeField]
        BoxCollider2D detectionArea;
        [SerializeField]
        Transform shotPosition;

        [SerializeField]
        protected EnemyProfile[] enemyProfile;

        [ReadOnly]
        [SerializeField]
        EnemyStat enemyStatus = new EnemyStat();

        [SerializeField]
        bool attackDelay = false;

        protected EnemyUsedControl baseObject = new EnemyUsedControl();
        public bool death = false;
        public bool action = false;
        public bool non_target = false;
        public int currentPhaseCount = 0;

        bool saveDeath = false;

        protected string enemyStatusType;

        internal bool attackSignal = false;

        [System.Serializable]
        struct BuffStatus
        {
            public BuffData[] attack;
            public BuffData[] hit;
            public BuffData[] death;
        }
        [SerializeField]
        BuffStatus buffSet;

        public void AttackSignal()
        {
            attackSignal = true;
        }


        //최종 hp
        int hpMax;
        int hp;

        private void OnBecameVisible()
        {
            visible = true;
            
        }
        protected virtual void OnBecameInvisible()
        {
            ResetStart();
            visible = false;
        }

        protected virtual void OnDrawGizmos()
        {
            if(detectionArea != null)
            {
                Gizmos.color = new Color(1,0,0,.2f);
                Gizmos.DrawCube(detectionArea.bounds.center, detectionArea.bounds.size);
            }
        }

        void SetEnemyStatus()
        {
            enemyStatus.hp = enemyProfile[currentPhaseCount].hp;
            enemyStatus.attackInterval = enemyProfile[currentPhaseCount].interval;
        }
        void SetEnemyStatus(int index)
        {
            if(enemyProfile != null&& index < enemyProfile.Length)
            {
                enemyStatus.hp = enemyProfile[index].hp;
                enemyStatus.attackInterval = enemyProfile[index].interval;
            }
        }
        internal void SetColorAlpha(float value)
        {
            Color color = baseObject.sRenderer.color;
            color.a = value;
            baseObject.sRenderer.color = color;
        }
        private void FixedUpdate()
        {
           
            if ((Dialog.DialogManager.instance != null && Dialog.DialogManager.instance.IsDialog() == false) && ImageCutscene.playing == false)
            {
                if (visible == true && death == false && action == false)
                {
                    if (enemyProfile[currentPhaseCount] != null && enemyProfile[currentPhaseCount].approachPatterns != null && enemyProfile[currentPhaseCount].approachPatterns.Length > 0 && ApproachDetect())
                    {
                        EnemyFlip();
                        StartCoroutine(BaseApproachAttack());
                    }
                    else if (DetectPlayer())
                    {
                        AttackAction();
                    }
                }
            }
        }
        // Start is called before the first frame update
        protected virtual void Start()
        {
            

            if(detectionArea != null)
            {
                if(detectGroup == null)
                {
                    detectGroup = new GameObject();
                    detectGroup.name = "DtectAreaGroup";
                }
                detectionArea.transform.parent = detectGroup.transform;
            }
            baseObject.sound = GetComponent<SoundFXControl>();
            baseObject.collider = GetComponent<BoxCollider2D>();
            baseObject.animator = GetComponent<Animator>();
            baseObject.sRenderer = GetComponent<SpriteRenderer>();
            baseObject.playerCollider = BladeJumper.bladeJumper.GetComponent<BoxCollider2D>();
            baseObject.owner = this;
            if(shotPosition == null)
            {
                baseObject.attackPivot = transform;
            }
            else
            {
                baseObject.attackPivot = shotPosition;
            }


            if (dataSave)
            {
                if (SaveManager.instance.GetDisposableUsed(gameObject.name.GetHashCode()))
                {
                    if (GetComponent<ResetControl>() != null)
                    {
                        Destroy(GetComponent<ResetControl>());
                    }
                    saveDeath = true;
                    death = true;
                    SetColorAlpha(0);
                    return;
                }
            }

            SetEnemyStatus();
            animationWait = new WaitUntil(() => animationEnd);


            for(int i = 0; i < enemyProfile.Length; i++)
            {
                hpMax += enemyProfile[i].hp;
            }
            hp = hpMax;

            if(infinityHP == false)
                EnemyHPBarManager.instance.CreateHPBar(this);

        }

        internal void PlayAnimation(string aNIM, bool oneShot = true)
        {

            string animStr = aNIM.ToString();
            if (currentPhaseCount > 0)
            {
                animStr += (currentPhaseCount + 1).ToString();
            }
            if (oneShot)
            {
                baseObject.animator.Play(animStr, -1, 0);
            }
            else
            {
                baseObject.animator.Play(animStr);
            }


        }
        internal IEnumerator WaitAnimation(string animationState, bool oneShot = false)
        {
            animationEnd = false;
            PlayAnimation(animationState,oneShot);
            yield return animationWait;
            
        }
        public void AnimationEnd()
        {
            animationEnd = true;
        }
        public void AnimationSwitch()
        {
            animationEnd = true;
        }

        internal void EnemyFlip()
        {
            if(baseObject.center.x < baseObject.playerCenter.x)
            {
                if(baseObject.sRenderer.flipX == false)
                {
                    return;
                }
            }
            else
            {
                if (baseObject.sRenderer.flipX == true)
                {
                    return;
                }
            }
            baseObject.sRenderer.flipX = !baseObject.sRenderer.flipX;
            for (int i = 0; i < transform.childCount; i++)
            {
                
                GameObject child = transform.GetChild(i).gameObject;
                if (detectionArea != null&&child == detectionArea.gameObject) continue;

                
                Vector2 childPos = child.transform.localPosition;
                childPos.x *= -1;
                child.transform.localPosition = childPos;
                if (child.GetComponent<SpriteRenderer>() != null)
                {
                    child.GetComponent<SpriteRenderer>().flipX = baseObject.sRenderer.flipX;
                }
                else
                {
                    child.transform.localScale = new Vector2(baseObject.sRenderer.flipX?-1:1,1);
                }
            }
        }
        
        bool ApproachDetect()
        {
            if (baseObject.playerDistance <= enemyProfile[currentPhaseCount].aproachDistance)
                return true;
            return false;
        }

        protected RaycastHit2D DetectPlayer()
        {
            

            if (detectionArea == null||detectionArea.bounds.Contains(baseObject.playerCenter))
            {
                RaycastHit2D hit = Physics2D.Raycast(baseObject.center,baseObject.playerDir,baseObject.playerDistance,playerLayer|bottomLayer);
                if (hit)
                {
                    if(hit.collider.gameObject != BladeJumper.bladeJumper.gameObject)
                    {
                        return new RaycastHit2D();
                    }
                    else
                    {
                        return hit;
                    }
                }
            }


            return new RaycastHit2D();
        }

        
        protected void AttackAction()
        {
            EnemyFlip();
            StartCoroutine(BaseAttackRoutine());
        }
        protected void DeathAction()
        {
            StartCoroutine (BaseDeathRoutine());
        }


        protected IEnumerator DeathRoutine()
        {
            yield return WaitAnimation("Death");
        }
        IEnumerator BaseApproachAttack()
        {
            action = true;
            
            yield return StartCoroutine(enemyProfile[currentPhaseCount].ApproachAttack(baseObject));
            PlayAnimation("Idle",false);
            action = false;
        }
        protected IEnumerator BaseAttackRoutine()
        {
            attackSignal = false;
            action = true;
            yield return StartCoroutine(enemyProfile[currentPhaseCount].Attack(baseObject));
            if (attackDelay)
                yield return WaitAnimation("Attack_Delay");

            PlayAnimation("Idle",false);
            yield return new WaitForSeconds(enemyStatus.attackInterval);
            action = false;
        }
        protected IEnumerator BaseDeathRoutine()
        {
            yield return StartCoroutine(DeathRoutine());
            SetColorAlpha(0);
            BuffAction(buffSet.death, killer);
        }

        public void PhaseChange()
        {
            StopAllCoroutines();

            if (currentPhaseCount +1 >= enemyProfile.Length)
            {
                death = true;
                if (dataSave)
                {
                    SaveManager.instance.SetDisposableUsed(this.gameObject.name.GetHashCode(), true);
                    saveDeath = true;
                }
                DeathAction();
            }
            else
            {
                StartCoroutine(PhaseChangeRoutine());
            }
            
        }

        IEnumerator PhaseChangeRoutine()
        {
            non_target = true;
            action = true;
            yield return WaitAnimation("Death");
            currentPhaseCount += 1;
            SetEnemyStatus();
            PlayAnimation("Idle");
            non_target = false;
            action = false;
        }

        public int GetHP()
        {
            if (non_target)
            {
                return -10;
            }
            return hp;
        }

        GameObject killer;

        public bool Damaged(int damage, Vector2 dir = default, Vector2 power = default, GameObject actor = null)
        {
            if (death == false&& non_target == false)
            {
                if(infinityHP == false)
                {
                    if (enemyStatus.hp > 0)
                        hp -= 1;
                    enemyStatus.hp -= 1;
                }
                
                if(enemyStatus.hp <= 0)
                {
                    killer = actor;
                    PhaseChange();
                }
                BuffAction(buffSet.hit, actor);
                return true;
            }
            return false;
        }

        void BuffAction(BuffData[] buffs, GameObject target)
        {
            if(buffs.Length > 0)
            {
                for (int i = 0; i < buffs.Length; i++)
                {
                    buffs[i].BuffActive(this.gameObject, target);
                }
            }
        }

        public void ResetStart()
        {
            if(saveDeath == false)
            {
                StopAllCoroutines();
                SetColorAlpha(1);
                SetEnemyStatus(0);
                hp = hpMax;
            }
        }
        public bool GetVisible()
        {
            return visible;
        }
        public virtual Bounds GetBounds()
        {
            return baseObject.collider.bounds;
        }

        public void AddBuff(BuffData buff)
        {
        }

        public void RemoveBuff(BuffData buff)
        {
           
        }

        public float GetTime(BuffData buff)
        {
            return 0;
        }

        public float AddTime(BuffData buff, float time)
        {
            return 0;
        }
        public GameObject GetTarget()
        {
            return this.gameObject;
        }
        public Dictionary<string, float> GetBuffs()
        {
            return null ;
        }
    }

}

