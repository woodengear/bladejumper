using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemy
{
    public class EnemyHPBarManager : MonoBehaviour
    {
        static internal EnemyHPBarManager instance;

        [SerializeField]
        EnemyHPBar hpBar;
        // Start is called before the first frame update

        

        private void Awake()
        {
            instance = this;
        }

        internal void CreateHPBar(IEnemyObject target)
        {
            EnemyHPBar ehp = Instantiate<EnemyHPBar>(hpBar,transform);
            ehp.SetHPBar(target);
        }
    }
}