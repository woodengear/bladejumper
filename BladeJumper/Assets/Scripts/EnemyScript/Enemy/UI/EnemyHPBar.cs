using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Enemy
{
    public class EnemyHPBar : MonoBehaviour
    {
        [SerializeField]
        GameObject layout;

        [SerializeField]
        GameObject hpImage;

        [SerializeField]
        Vector2 pivot;
        IEnemyObject target;

        int hpMax;
        int hp;

        float hpSize;

        List<GameObject> hpImages = new List<GameObject>();

        internal void SetHPBar(IEnemyObject target)
        {
            this.target = target;
            hpMax = target.GetHP();
            hp = hpMax;
            //hpSize = (1f - (1-hpMax)*0.02f)/hpMax;
            hpSize = (1f - (1 - 10) * 0.02f) / 10;
            Bounds eb = target.GetBounds();
            transform.position = new Vector2(eb.center.x, eb.max.y) + pivot;

            for (int i = 0; i < hpMax; i++)
            {
                GameObject ehp = Instantiate(hpImage, layout.transform);
                Vector2 scale = ehp.transform.localScale;
                scale.x = hpSize;
                ehp.transform.localScale = scale;
                hpImages.Add(ehp);
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (target.GetVisible())
            {
                if(target.GetHP() == -10)
                {
                    layout.SetActive(false);
                }
                else
                {
                    layout.SetActive(true);
                }
                if(hp != target.GetHP())
                {
                    hp = target.GetHP();
                    for(int i = 0; i < hpMax; i++)
                    {
                        hpImages[i].SetActive(i<hp);
                    }
                    
                }
                Bounds eb = target.GetBounds();
                transform.position = new Vector2(eb.center.x, eb.max.y) + pivot;
            }
        }
    }
}