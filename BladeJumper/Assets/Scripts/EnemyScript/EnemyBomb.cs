using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBomb : ThrownObject, IDamagedObject
{
    [SerializeField]
    GameObject areaEffect;

    [SerializeField]
    internal float radius = 2;

    [SerializeField]
    GameObject[] BombEffect;

    [SerializeField]
    internal Collider2D col;
    internal float bombTime = 2;

    internal void ActiveArea()
    {
        areaEffect.SetActive(true);
    }

    internal override void SetForce(Vector2 force)
    {
        areaEffect.transform.localScale = Vector2.one * radius*2;
        areaEffect.SetActive(false);

        base.SetForce(force);
        StartCoroutine(BombTimer());
    }

    protected override void ParryingAction()
    {
        Bomb();
    }
    IEnumerator BombTimer()
    {
        rb = GetComponent<Rigidbody2D>();
        int time = 0;
        while (time < 2 &&rb.velocity.magnitude > .3f)
        {
            time++;
            yield return new WaitForSeconds(1);
        }
        areaEffect.SetActive(true);
        GetComponent<AudioSource>().volume = SoundSetting.fxVolume;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(bombTime);
        Bomb();
    }

    void Bomb()
    {
        RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, radius, Vector2.zero, 0, playerMask);
        GetComponent<AudioSource>().pitch = 1;
        soundFXControl.PlaySound("Bomb");
        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i])
            {
                
                BladeJumper bj = hit[i].collider.GetComponent<BladeJumper>();
                if (bj)
                {
                    float dirX = bj.transform.position.x - transform.position.x;
                    bj.Damaged(1, hit[i].point, Vector2.up * 6 + Vector2.right * Mathf.Sign(dirX) * 15, this.gameObject);
                }


            }
        }
        

        for (int i = 0; i < BombEffect.Length; i++)
        {
            GameObject effect = Instantiate<GameObject>(BombEffect[i]);
            effect.transform.position = transform.position;
            effect.transform.localScale = effect.transform.localScale * (radius / 2);
        }



        Destroy(this.gameObject);
    }



    public override bool Damaged(int damage, Vector2 dir = default, Vector2 power = default, GameObject actor = null)
    {
        StopAllCoroutines();
        Bomb();
        return false;
    }

}
