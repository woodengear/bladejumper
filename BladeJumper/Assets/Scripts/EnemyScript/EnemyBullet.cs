using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;
using Buff;

public class EnemyBullet : MonoBehaviour,ITrailUse, IDamagedObject
{
    [SerializeField]
    LayerMask collsions;
    [SerializeField]
    LayerMask playerAttackMask;
    [SerializeField]
    int ownerLayer;
    [SerializeField]
    GameObject characterHitEffect;
    [SerializeField]
    GameObject wallHitEffect;
    [SerializeField]
    GameObject parryingEffect;

    [SerializeField]
    GameObject fireEffect;

    [SerializeField]
    GameObject trailEffect;



    TrailEffectControl trailEffectControl;

    GameObject owner;

    [SerializeField]
    internal BuffData[] buffs;

    float           speed;
    internal bool   infinity;
    Vector2         dir;
    bool            fire = false;
    bool            reflect = false;
    internal bool   reflectAble = true;
    float           power;
    internal int    reflectCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameObject fe= Instantiate<GameObject>(fireEffect);
        fe.transform.position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        
        SetRotate();
        if (fire)
        {
            MoveBullet();
            //DetectTarget();
        }
    }

    internal void Fire(float speed, Vector2 dir,float power,Enemy.EnemyBase owner)
    {
        if(trailEffect != null)
        {
            trailEffectControl = Instantiate<GameObject>(trailEffect, transform.position, transform.rotation).GetComponent<TrailEffectControl>();
            trailEffectControl.StartTrail(this.gameObject, this, .5f, 0);
            trailEffectControl.GetComponent<TrailRenderer>().AddPosition(transform.position);
        }
        
        
        ownerLayer = owner.gameObject.layer;
        this.speed = speed;
        this.dir = dir;
        this.power = power;
        this.owner = owner.gameObject;
        fire = true;
    }
    internal void Fire(float speed, Vector2 dir, float power, MonoBehaviour owner)
    {
        if (trailEffect != null)
        {
            trailEffectControl = Instantiate<GameObject>(trailEffect, transform.position, transform.rotation).GetComponent<TrailEffectControl>();
            trailEffectControl.StartTrail(this.gameObject, this, .5f, 0);
            trailEffectControl.GetComponent<TrailRenderer>().AddPosition(transform.position);
        }
        ownerLayer = owner.gameObject.layer;
        this.speed = speed;
        this.dir = dir;
        this.power = power;
        this.owner = owner.gameObject;
        fire = true;
    }

    void SetRotate()
    {
        transform.rotation = Quaternion.Euler(0,0,Vector2.SignedAngle(Vector2.right,dir));
    }

    /*
     * 1. 발사됨
     * 2. 충돌 검사
     * 
     */

    void MoveBullet()
    {
        
        if ((Dialog.DialogManager.instance != null && Dialog.DialogManager.instance.IsDialog()) || ImageCutscene.playing)
        {
            return;
        }
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir,(infinity?int.MaxValue:speed * Time.deltaTime), collsions);
        if (hit)
        {
            if(trailEffectControl != null)
                trailEffectControl.GetComponent<TrailRenderer>().AddPosition(hit.point);
            IDamagedObject damagedObject = hit.collider.GetComponent<IDamagedObject>();
            if(damagedObject != null)
            {
                damagedObject.Damaged(1,hit.point, Vector2.up * 6 + Vector2.right * Mathf.Sign(dir.x) * power,owner);
                if(buffs != null)
                {
                    for (int i = 0; i < buffs.Length; i++)
                    {
                        buffs[i].BuffActive(this.gameObject, hit.collider.gameObject);
                    }
                }
                
               

                transform.position = hit.point;
                GameObject effect = Instantiate<GameObject>(characterHitEffect, hit.point, transform.rotation);
                Destroy(this.gameObject);
            }
            else
            {
                if(hit.collider.gameObject.tag == "AttackArea")
                {
                    //Debug.Log("atarrParrying");
                    transform.position = hit.point + dir.normalized*.1f;
                }
                else
                {
                    fire = false;
                    GameObject effect = Instantiate<GameObject>(wallHitEffect, hit.point, transform.rotation);
                    Destroy(this.gameObject);
                }
            }
        }
        else
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        
    }

    
    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        
    }

    public bool EndTrail()
    {
        return false;
    }

    public Vector2 Position()
    {
        return transform.position;
    }

    

    public bool Damaged(int damage, Vector2 dir = default, Vector2 power = default, GameObject actor = null)
    {
        if (reflectAble&&owner != actor)
        {
            fire = true;
            speed = 50;
            //dir.x *= -1;
            //dir.y = Random.Range(0,1f);

            
            this.dir = (Vector2)(owner.GetComponent<BoxCollider2D>().bounds.center - transform.position).normalized;
            //dir *= -1;
            //transform.position = owner.transform.position;
            //trailEffectControl.transform.position = transform.position;
            GameObject effect = Instantiate<GameObject>(parryingEffect, transform.position, transform.rotation);

            SetRotate();
            owner = actor;
            collsions |= 1 << ownerLayer;
            collsions ^= 1 << 3;
            reflectCount += 1;
        }
        

        return false;
    }
    internal void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    int IDamagedObject.GetHP()
    {
        return 1;
    }
}
