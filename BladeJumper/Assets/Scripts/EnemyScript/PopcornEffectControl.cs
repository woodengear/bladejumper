using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopcornEffectControl : MonoBehaviour
{
    [SerializeField]
    float destroyTime = 5;

    [SerializeField]
    float power = 5;

    [SerializeField]
    float minRange;
    [SerializeField]
    float maxRange;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody2D[] rigidbody2Ds = GetComponentsInChildren<Rigidbody2D>();
        for(int i = 0; i < rigidbody2Ds.Length; i++)
        {
            rigidbody2Ds[i].AddForce(RotateVector(Vector2.right * power, i*360/rigidbody2Ds.Length),ForceMode2D.Impulse);
        }
        StartCoroutine(DestroyAction());
    }
    Vector2 RotateVector(Vector2 vec, float angle)
    {
        float rad = Mathf.Deg2Rad * angle;
        return new Vector2(vec.x * Mathf.Cos(rad) + vec.y * Mathf.Sin(rad), -Mathf.Sin(rad) * vec.x + Mathf.Cos(rad) * vec.y);
    }


    IEnumerator DestroyAction()
    {


        yield return new WaitForSeconds(destroyTime);
        SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer>();
        
        Color color = srs[0].color;
        while (color.a > 0.1f)
        {
            color.a -= .03f;
            for (int i = 0; i < srs.Length; i++)
            {
                
                
                srs[i].color = color;
                
            }
            yield return new WaitForSeconds(.1f);
        }
        Destroy(gameObject);
    }
}
