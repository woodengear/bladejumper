using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrownObject : MonoBehaviour, IDamagedObject
{
    [SerializeField]
    protected LayerMask playerMask;
    protected Rigidbody2D rb;
    Vector2 force;
    protected float power = 6;
    int hitCount = 0;

    protected SoundFXControl soundFXControl;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        soundFXControl = GetComponent<SoundFXControl>();
    }
    internal virtual void SetForce(Vector2 force)
    {
        
        rb = GetComponent<Rigidbody2D>();
        this.force = force;
        rb.AddForce(force, ForceMode2D.Impulse);
    }
    // Update is called once per frame
    protected virtual void FixedUpdate()
    {
        Debug.DrawRay(transform.position, rb.velocity * Time.deltaTime);
        if (hitCount == 0 && rb.velocity.magnitude * Time.deltaTime > .05f)
        {

            RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, rb.velocity, rb.velocity.magnitude * Time.deltaTime, playerMask);
            for (int i = 0; i < hit.Length; i++)
            {
                if (hit[i])
                {

                    BladeJumper bj = hit[i].collider.GetComponent<BladeJumper>();
                    if (bj)
                    {
                        hitCount++;
                        bj.Damaged(1, hit[i].point, Vector2.up * 6 + Vector2.right * Mathf.Sign(rb.velocity.normalized.x) * power, this.gameObject);

                        soundFXControl.PlaySound("Hit", 1, true);
                    }
                    else if (hit[i].collider.tag == "AttackArea")
                    {
                        StopAllCoroutines();
                        ParryingAction();
                    }
                }
            }
        }
    }

    protected virtual void ParryingAction()
    {

    }
    public virtual bool Damaged(int damage, Vector2 dir = default, Vector2 power = default, GameObject actor = null)
    {
        return false;
    }

    public virtual int GetHP()
    {
        return 1;
    }

    public void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
    public void OnBecameVisible()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        soundFXControl.PlaySound("Collision", .5f);

    }
}
