using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;
using UnityEngine.Events;

namespace Enemy
{


    [RequireComponent(typeof(CharacterCollider))]
    public class SwordLlamasterControl : MonoBehaviour, IDamagedObject,IEnemyObject
    {
        [SerializeField]
        protected LayerMask playerLayer;

        [SerializeField]
        protected LayerMask wallLayer;

        [SerializeField]
        protected Vector2Position[] movePoints;
        protected int currentPositionIndex = 0;

        [System.Serializable]
        protected struct BulletStatus
        {
            [SerializeField]
            public GameObject firePosition;
            [SerializeField]
            public EnemyBullet bulletPrefabs;
            public float bulletPower;
            public float bulletSpeed;
        }
        [SerializeField]
        protected BulletStatus bulletStatus;



        [SerializeField]
        ParticleSystem[] moveEffets;
        [SerializeField]
        ParticleSystem.MinMaxGradient[] moveEffectColors;


        protected bool coroutineTrigger = false;
        protected bool visible = false;
        bool corutineRunning = false;
        IEnumerator routine;


        [SerializeField]
        protected BoxCollider2D slashArea;



        protected SpriteRenderer sRenderer;

        [SerializeField]
        int hp = 5;
        int hpAll = 0;
        /*
         * 페이즈방식
         * 리스트 인덱스가 페이즈
         * 페이즈는 패턴 배열로 이루어짐
         */
        protected enum LlmasterPattern { SlashFast, MoveAction, ShotAction, MoveShotMove, ShotParrying };
        [System.Serializable]
        protected struct LlmasterPhase
        {
            public int hp;
            [SerializeField]
            internal LlmasterPattern[] approachPatterns;
            [SerializeField]
            internal LlmasterPattern[] patterns;
        }

        protected delegate IEnumerator EnemyPattern();
        protected List<List<EnemyPattern>> enemyPhase = new List<List<EnemyPattern>>();
        [SerializeField]
        protected LlmasterPhase[] phases;
        protected int currentPhase = 0;


        protected Animator animator;
        protected enum ANIM_STATE
        {
            Idle, Hit, Death,
            Slash, Slash_Ready, Slash_Idle, Slash_Action,
            Shot, Shot_Idle, Shot_Ready, Shot_Fire,
            Move, AfterEffect
        }

        public bool animationSwitch = false;

        protected BladeJumper player;
        protected SoundFXControl sfx;

        [SerializeField]
        bool death = false;
        bool non_damaged = false;
        private void OnDrawGizmosSelected()
        {
            if (movePoints.Length > 0)
            {
                Vector2 size = GetComponent<BoxCollider2D>().bounds.size;
                Vector2 offset = GetComponent<BoxCollider2D>().offset;
                Gizmos.color = Color.cyan;
                for (int i = 0; i < movePoints.Length; i++)
                {
                    Gizmos.DrawWireSphere(movePoints[i].position + offset, .3f);
                    Gizmos.DrawWireCube(movePoints[i].position + offset, size);
                }

            }
        }

        EnemyPattern PatternToIEnumerator(LlmasterPattern pattern)
        {
            switch (pattern)
            {
                case LlmasterPattern.SlashFast:
                    return SlashFast;
                case LlmasterPattern.MoveAction:
                    return MoveAction;
                case LlmasterPattern.ShotAction:
                    return ShotAction;
                case LlmasterPattern.MoveShotMove:
                    return MoveShotMove;
                case LlmasterPattern.ShotParrying:
                    return ShotParrying;
            }
            return null;
        }

        void ReferenceReset()
        {
            animator = GetComponent<Animator>();
            player = BladeJumper.bladeJumper;
            sRenderer = GetComponent<SpriteRenderer>();
            sfx = GetComponent<SoundFXControl>();
        }
        private void Awake()
        {
            hp = phases[0].hp;
            routine = PlayRoutine();
            ReferenceReset();
            death = SaveManager.instance.GetDisposableUsed(this.gameObject.name.GetHashCode());
            if (death == true)
            {
                sRenderer.color = new Color(0, 0, 0, 0);
                return;
            }
            for (int i = 0; i < moveEffets.Length; i++)
            {
                var col = moveEffets[i].colorOverLifetime;
                col.color = moveEffectColors[0];
                moveEffets[i].GetComponent<ParticleSystemRenderer>().sortingLayerName = "BE";
                //moveEffets[i].colorOverLifetime.color = moveEffectColors[i];
            }
            hpAll = 0;
            for (int i = 0; i < phases.Length; i++)
            {
                hpAll += phases[i].hp;
            }
        }
        private void Start()
        {
            EnemyHPBarManager.instance.CreateHPBar(this);
        }
        protected virtual void PhaseChange()
        {

            for (int i = 0; i < moveEffets.Length; i++)
            {
                var col = moveEffets[i].colorOverLifetime;
                col.color = moveEffectColors[1];
                moveEffets[i].GetComponent<ParticleSystemRenderer>().sortingLayerName = "FrontEffect_Unlighting";
            }
        }



        public void AnimationSwitch()
        {
            animationSwitch = true;
        }

        protected void PlayAnimation(ANIM_STATE aNIM, bool oneShot = false)
        {

            string animStr = aNIM.ToString();
            if (currentPhase > 0)
            {
                animStr += (currentPhase + 1).ToString();
            }
            if (oneShot)
            {
                animator.Play(animStr, -1, 0f);
            }
            else
            {
                animator.Play(animStr);
            }


        }
        //ActionStart
        private void OnBecameVisible()
        {
            death = SaveManager.instance.GetDisposableUsed(this.gameObject.name.GetHashCode());
            if (death == false)
            {
                visible = true;

                hpAll = 0;
                for(int i = 0; i < phases.Length; i++)
                {
                    hpAll += phases[i].hp;
                }

                //PlayAnimation(ANIM_STATE.Idle);
                //StartCoroutine(routine);
                StartCoroutine(routine);
            }
            else
            {
                StopAllCoroutines();
            }
        }

        //ActionEnd, Reset
        private void OnBecameInvisible()
        {
            if (death == false)
            {
                visible = false;
                StopAllCoroutines();
                corutineRunning = false;
                //PlayAnimation(ANIM_STATE.Idle);
                //StopAllCoroutines();
            }
            else
            {
                StopAllCoroutines();
                sRenderer.color = new Color(0, 0, 0, 0);
            }
        }


        protected void FlipCheck()
        {
            player = BladeJumper.bladeJumper;
            sRenderer = GetComponent<SpriteRenderer>();
            if (transform.position.x < player.transform.position.x && sRenderer.flipX == true)
            {
                bulletStatus.firePosition.transform.localPosition = new Vector2(-bulletStatus.firePosition.transform.localPosition.x, bulletStatus.firePosition.transform.localPosition.y);
                slashArea.transform.localScale = new Vector2(1, 1);
                sRenderer.flipX = false;
            }
            else if (transform.position.x > player.transform.position.x && sRenderer.flipX == false)
            {
                sRenderer.flipX = true;
                bulletStatus.firePosition.transform.localPosition = new Vector2(-bulletStatus.firePosition.transform.localPosition.x, bulletStatus.firePosition.transform.localPosition.y);
                slashArea.transform.localScale = new Vector2(-1, 1);
            }

        }

        /*
         * 이동
         * MovePoints 배열중 한곳으로 이동
         */
        protected void MovePoint()
        {
            int nextPos = 0;
            Vector2 offset = GetComponent<BoxCollider2D>().offset;
            if (currentPositionIndex == 0)
            {
                nextPos = Random.Range(1, movePoints.Length);
            }
            else if (currentPositionIndex == movePoints.Length - 1)
            {
                nextPos = Random.Range(0, movePoints.Length - 1);
            }
            else
            {
                nextPos = Random.Range(0, 2) == 1 ? Random.Range(currentPositionIndex + 1, movePoints.Length) : Random.Range(0, currentPositionIndex);
            }

            transform.position = movePoints[nextPos].position;
            currentPositionIndex = nextPos;
        }

        protected void MovePoint(int index)
        {
            Vector2 offset = GetComponent<BoxCollider2D>().offset;
            transform.position = movePoints[index].position;
            currentPositionIndex = index;

        }
        /*
         * 사격 가능한 다른 위치로 이동
         */
        protected void ShotPointMove()
        {
            int cIndex = currentPositionIndex;
            for (int i = 0; i < 20; i++)
            {
                MovePoint();
                if (ShotCheck() && currentPositionIndex != cIndex)
                {
                    break;
                }
            }
            sfx.PlaySound("Move");
        }
        /*
         * 권총사격
         * 
         */

        protected bool ShotCheck()
        {
            Vector2 dir = player.GetComponent<BoxCollider2D>().bounds.center - bulletStatus.firePosition.transform.position;
            RaycastHit2D hit = Physics2D.Raycast(bulletStatus.firePosition.transform.position, dir, dir.magnitude, wallLayer);
            FlipCheck();
            if (hit)
            {
                return false;
            }

            return true;
        }

        //근접이면 Slash로 전환
        protected bool SlashCheck()
        {
            return Vector2.Distance(player.transform.position, transform.position) < 2;
        }

        protected EnemyBullet Shot()
        {

            FlipCheck();
            EnemyBullet bullet = Instantiate<EnemyBullet>(bulletStatus.bulletPrefabs);
            bullet.transform.position = bulletStatus.firePosition.transform.position;
            bullet.Fire(bulletStatus.bulletSpeed, ((Vector2)player.GetComponent<BoxCollider2D>().bounds.center - (Vector2)bulletStatus.firePosition.transform.position).normalized, bulletStatus.bulletPower, this);

            return bullet;
        }

        protected void Slash(List<IDamagedObject> damagedObjects)
        {
            FlipCheck();
            if (slashArea.enabled)
            {
                List<Collider2D> targets = new List<Collider2D>();
                ContactFilter2D cf = new ContactFilter2D();
                cf.layerMask = ~0;
                cf.layerMask &= ~(1 << gameObject.layer);
                cf.useLayerMask = true;
                slashArea.OverlapCollider(cf, targets);

                if (targets.Count > 0)
                {
                    for (int i = 0; i < targets.Count; i++)
                    {
                        IDamagedObject target = targets[i].GetComponent<IDamagedObject>();
                        if (target != null && !damagedObjects.Contains(target))
                        {
                            damagedObjects.Add(target);
                            target.Damaged(1, default, Vector2.up * 6 + Vector2.right * (sRenderer.flipX ? -1 : 1) * bulletStatus.bulletPower, this.gameObject);
                        }
                    }
                }
            }
        }

        IEnumerator WaitAnimation(ANIM_STATE aNIM, bool oneShot = false)
        {

            animationSwitch = false;
            PlayAnimation(aNIM, oneShot);
            yield return new WaitUntil(() => animationSwitch);
            animationSwitch = false;
        }

        /*
         * 무브 액션
         * 잠시 대기했다가
         * 이동후
         * 후딜레이
         */
        IEnumerator MoveAction()
        {
            yield return WaitAnimation(ANIM_STATE.Move);
            sfx.PlaySound("Move");
            MovePoint();
            FlipCheck();
            PlayAnimation(ANIM_STATE.Idle);
            yield return new WaitForSeconds(.4f);
        }

        //제자리에서 사격
        IEnumerator ShotAction()
        {
            yield return StartCoroutine(WaitAnimation(ANIM_STATE.Shot, true));
            if (!SlashCheck())
            {

                Shot();
            }
            yield return WaitAnimation(ANIM_STATE.Shot);

        }

        IEnumerator Shot_3_fire()
        {
            FlipCheck();
            yield return WaitAnimation(ANIM_STATE.Shot_Ready);
            PlayAnimation(ANIM_STATE.Shot_Idle);
            yield return new WaitForSeconds(1);
            for (int i = 0; i < 3; i++)
            {
                Shot();
                yield return WaitAnimation(ANIM_STATE.Shot_Fire, true);
                yield return new WaitForSeconds(.1f);
            }

        }

        /*
         * [이동후 사격] 3회 반복
         */
        IEnumerator MoveShotMove()
        {
            yield return WaitAnimation(ANIM_STATE.Move);
            ShotPointMove();
            FlipCheck();
            for (int i = 0; i < 3; i++)
            {
                if (SlashCheck())
                {
                    yield return SlashAction();

                }
                else
                {
                    yield return StartCoroutine(WaitAnimation(ANIM_STATE.Shot, true));
                    if (ShotCheck())
                    {
                        Shot();
                    }
                }


                //yield return new WaitForSeconds(.4f);
                yield return WaitAnimation(ANIM_STATE.AfterEffect, true);
                ShotPointMove();
                FlipCheck();
            }
            PlayAnimation(ANIM_STATE.Idle);
        }
        IEnumerator MoveShotMove2()
        {
            FlipCheck();
            yield return WaitAnimation(ANIM_STATE.Move);
            MovePoint();
            yield return new WaitForSeconds(.1f);
            ShotPointMove();
            FlipCheck();
            for (int i = 0; i < 4; i++)
            {

                if (SlashCheck())
                {
                    yield return SlashAction();

                }
                else
                {
                    yield return StartCoroutine(WaitAnimation(ANIM_STATE.Shot, true));
                    if (ShotCheck())
                    {
                        Shot();
                    }
                }

                //yield return new WaitForSeconds(.4f);
                yield return WaitAnimation(ANIM_STATE.AfterEffect, true);
                MovePoint();
                yield return WaitAnimation(ANIM_STATE.AfterEffect, true);
                ShotPointMove();
                FlipCheck();
            }
            PlayAnimation(ANIM_STATE.Idle);
        }
        /*
         * 베기모션
         */
        IEnumerator SlashAction()
        {
            FlipCheck();
            yield return WaitAnimation(ANIM_STATE.Slash_Ready);
            PlayAnimation(ANIM_STATE.Slash_Action);
            List<IDamagedObject> targets = new List<IDamagedObject>();
            slashArea.gameObject.SetActive(true);
            while (slashArea.enabled)
            {
                Slash(targets);
                yield return null;
            }
            yield return WaitAnimation(ANIM_STATE.Slash_Action);
            slashArea.gameObject.SetActive(false);
            //yield return WaitAnimation(ANIM_STATE.Slash_Action);
            Debug.Log("Lamaster Slash Action");
        }

        IEnumerator SlashFast()
        {
            FlipCheck();
            PlayAnimation(ANIM_STATE.Slash_Action);
            List<IDamagedObject> targets = new List<IDamagedObject>();
            slashArea.gameObject.SetActive(true);
            while (slashArea.enabled)
            {
                Slash(targets);
                yield return null;
            }
            yield return WaitAnimation(ANIM_STATE.Slash_Action);
            slashArea.gameObject.SetActive(false);
            yield return new WaitForSeconds(.1f);
            //yield return WaitAnimation(ANIM_STATE.Slash_Action);
            Debug.Log("Lamaster Slash Action");
        }


        IEnumerator SlashReady()
        {
            FlipCheck();
            yield return WaitAnimation(ANIM_STATE.Slash_Ready);
            PlayAnimation(ANIM_STATE.Slash_Idle);
        }

        /*
         * 사격 후 패링 자세
         * 사격한 총알이 패링되어 돌아오면 패링
         */
        IEnumerator ShotParrying()
        {
            FlipCheck();
            yield return WaitAnimation(ANIM_STATE.Shot_Ready);
            EnemyBullet bullet = Shot();

            yield return WaitAnimation(ANIM_STATE.Shot_Fire);
            slashArea.gameObject.SetActive(true);
            //yield return SlashReady();
            PlayAnimation(ANIM_STATE.Slash_Ready);

            while (true)
            {
                if (bullet == null)
                {
                    break;
                }


                if (bullet.transform.position.x >= slashArea.bounds.min.x && bullet.transform.position.x <= slashArea.bounds.max.x
                    && bullet.transform.position.y >= slashArea.bounds.min.y && bullet.transform.position.y <= slashArea.bounds.max.y)
                {

                    bullet.Damaged(1, default, default, this.gameObject);
                    bullet.SetSpeed(bulletStatus.bulletSpeed);
                    yield return WaitAnimation(ANIM_STATE.Slash_Action);
                    break;
                }

                yield return null;
            }
            slashArea.gameObject.SetActive(false);

        }


        /*
         * 페이즈 변경
         * 
         */
        IEnumerator PhaseChangeAction()
        {
            non_damaged = true;
            yield return WaitAnimation(ANIM_STATE.Death);
            PhaseChange();

            yield return new WaitForSeconds(2);
            currentPhase += 1;
            non_damaged = false;
            yield return MoveShotMove2();
            yield return PlayRoutine();
        }

        IEnumerator Death()
        {
            yield return WaitAnimation(ANIM_STATE.Death);
            SaveManager.instance.SetDisposableUsed(gameObject.name.GetHashCode(), true);
            sRenderer.color = new Color(0, 0, 0, 0);
            death = true;
        }

        IEnumerator PlayRoutine()
        {
            ReferenceReset();
            corutineRunning = true;
            while (true)
            {
                if (visible)
                {
                    slashArea.gameObject.SetActive(false);
                    if (Vector2.Distance(player.transform.position, transform.position) < 2)
                    {
                        yield return PatternToIEnumerator(phases[currentPhase].approachPatterns[Random.Range(0, phases[currentPhase].approachPatterns.Length)])();
                    }
                    else
                    {
                        yield return PatternToIEnumerator(phases[currentPhase].patterns[Random.Range(0, phases[currentPhase].patterns.Length)])();
                    }
                }


                PlayAnimation(ANIM_STATE.Idle);
                yield return new WaitForSeconds(1);

            }
        }

        public bool Damaged(int damage, Vector2 dir = default, Vector2 power = default, GameObject actor = default)
        {
            if (non_damaged || death) return false;
            hp -= 1;
            hpAll -= 1;
            if (hp <= 0 && currentPhase + 1 < phases.Length)
            {

                hp = phases[currentPhase + 1].hp;
                StopAllCoroutines();
                StartCoroutine(PhaseChangeAction());
            }
            if (hp <= 0)
            {
                GetComponent<ResetControl>().resettable = false;
                //death = true;
                StopAllCoroutines();
                StartCoroutine(Death());
            }
            return !death;
        }

        public int GetHP()
        {
            if (death)
                return 0;
            else
            {
                return hpAll;
            }
                
        }

        public Bounds GetBounds()
        {
            return GetComponent<BoxCollider2D>().bounds;
        }

        public bool GetVisible()
        {
            return visible;
        }

        // Start is called before the first frame update


    }
}