using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerControl;

public class JumpingPlatform : MonoBehaviour
{
    [SerializeField]
    float jumpDistance = 5;

    [SerializeField]
    LayerMask playerLayer;

    [SerializeField]
    float maxSpeedX = 7;

    Vector2 startP;
    Vector2 endP;


    
    // Start is called before the first frame update
    void Start()
    {
        BoxCollider2D col = GetComponent<BoxCollider2D>();
        startP = new Vector2(col.bounds.min.x, col.bounds.max.y);
        endP = new Vector2(col.bounds.max.x, col.bounds.max.y);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        PlayerChack();
    }

    void PlayerChack()
    {
        RaycastHit2D hit = Physics2D.Linecast(startP, endP, playerLayer);

        if (hit)
        {
            Debug.Log(hit.point);
            CharacterCollider ccolider = hit.collider.GetComponent<CharacterCollider>();
            BladeJumper bj = ccolider.GetComponent<BladeJumper>();
            Vector2 velocity = ccolider.GetVelocity();
            float gravity = ccolider.GetGravity();
            float time = Mathf.Sqrt(jumpDistance * 2 / gravity);
            float newVel = gravity * time;
            if (newVel > velocity.y)
            {
                bj.GetComponent<AnimationControl>().lastSlash = false;
                if (Mathf.Abs(velocity.x) > maxSpeedX)
                {
                    
                    bj.ForceMove(Vector2.right * Mathf.Sign( velocity.x) * maxSpeedX + Vector2.up * (newVel - velocity.y),true);
                }
                else
                {
                    bj.ForceMove(Vector2.up * (newVel - velocity.y));
                }
                
                //ccolider.AddPower( Vector2.up * (newVel - velocity.y));

            }
            
            if(Mathf.Abs(velocity.x) < 2)
            {
                ccolider.AddPower(Vector2.right * (2 * Mathf.Sign(velocity.x) - velocity.x));

            }


        }
    }

}
