using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * 컷씬 기능 구현
 * 카메라 움직임 조절
 * 애니메이션 실행
 */
public struct CutSceneBehavior
{
    public enum Behavior {AnimationPlay, CameraMove}
    
    public Behavior behavior;
}

public class CutSceneControl : MonoBehaviour
{
    static public CutSceneControl instance;




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
